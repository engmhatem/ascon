@extends('emails.master')
@section('content')
        <tr>
            <td bgcolor="#ffffff" style="padding: 40px 40px 20px; text-align: center;">
                <h1 style="margin: 0; font-family: sans-serif; font-size: 24px; line-height: 27px; color: #333333; font-weight: normal;">{{ $title }}</h1>
            </td>
        </tr>

        <tr>
            <td bgcolor="#ffffff" style="padding: 0 20px 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
                <p style="margin: 0;">Career: {{ $msg->career->en_title }}</p>
                <p style="margin: 0;">Name: {{ $msg->name }}</p>
                <p style="margin: 0;">Email: {{ $msg->email }}</p>
                <p style="margin: 0;">Current Title: {{ $msg->current_title }}</p>
                <p style="margin: 0;">Current Salary: {{ $msg->current_salary }}</p>
                <p style="margin: 0;">Expected Join Date: {{ $msg->expected_join_date }}</p>
                <p style="margin: 0;">Current Location City: {{ $msg->current_location_city }}</p>
            </td>
        </tr>
@stop