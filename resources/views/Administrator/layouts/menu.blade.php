<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!-- END SIDEBAR TOGGLER BUTTON -->
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
            <li class="sidebar-search-wrapper">
                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                {!! Form::open(['method'=>'get','url'=>url(AD.'/admins'),'class'=>'sidebar-search']) !!}
                    <a href="javascript:;" class="remove">
                        <i class="icon-close"></i>
                    </a>
                    <div class="input-group has-error">
                        {!! Form::text('search',old('search'),['class'=>'form-control','placeholder'=>trans('admin.admins'),'required'=>'required']) !!}
                        <span class="input-group-btn">
                            <button type="submit" class="btn submit"><i class="icon-magnifier"></i></button>
                        </span>
                    </div>
                @if($errors->has('search'))
                    <span class="text-danger">{{ $errors->first('search') }}</span>
                @endif
                {!! Form::close() !!}
                <!-- END RESPONSIVE QUICK SEARCH FORM -->
            </li>
            {{--<li class="nav-item start {{ (Request::segment(2)=='home') ? 'active' :'' }}">--}}
                {{--<a href="javascript:;" class="nav-link nav-toggle">--}}
                    {{--<i class="icon-home"></i>--}}
                    {{--<span class="title">Dashboard</span>--}}
                    {{--<span class="selected"></span>--}}
                    {{--<span class="arrow"></span>--}}
                {{--</a>--}}
                {{--<ul class="sub-menu">--}}
                    {{--<li class="nav-item start {{ (Request::segment(2)=='home') ? 'active open' :'' }}">--}}
                        {{--<a href="{{ url(AD.'/home') }}" class="nav-link ">--}}
                            {{--<i class="icon-bar-chart"></i>--}}
                            {{--<span class="title">Dashboard 1</span>--}}
                            {{--<span class="badge badge-success">1</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            <li class="heading">
                <h3 class="uppercase">Dashboard</h3>
            </li>
            <!-- Start Admins Area -->
            <li class="nav-item start {{ (Request::segment(2)=='admins') ? 'active' :'' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-user-secret"></i>
                    <span class="title">{{ trans('admin.admins') }}</span>
                    <span class="selected"></span>
                    <span class="arrow {{ (Request::segment(2)=='admins') ? 'open' :'' }}"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start {{ (Request::segment(2)=='admins' && Request::segment(3)=='create') ? 'active' :'' }}">
                        <a href="{{ url(AD.'/admins/create') }}" class="nav-link ">
                            <i class="fa fa-plus-circle"></i>
                            <span class="title">{{ trans('admin.add_admin') }}</span>
                        </a>
                    </li>
                    <li class="nav-item start {{ (Request::is(AD.'/admins')) ? 'active' :'' }}">
                        <a href="{{ url(AD.'/admins') }}" class="nav-link ">
                            <i class="icon-user"></i>
                            <span class="title">{{ trans('admin.show_admin') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- End Admins Area -->

            <!-- Start News And Media Area -->
            <li class="nav-item start {{ (Request::segment(2)=='news-and-media') ? 'active' :'' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-newspaper-o"></i>
                    <span class="title">{{ trans('admin.news_and_media') }}</span>
                    <span class="selected"></span>
                    <span class="arrow {{ (Request::segment(2)=='news-and-media') ? 'open' :'' }}"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start {{ (Request::segment(2)=='news-and-media' && Request::segment(3)=='create') ? 'active' :'' }}">
                        <a href="{{ url(AD.'/news-and-media/create') }}" class="nav-link ">
                            <i class="fa fa-plus-circle"></i>
                            <span class="title">{{ trans('admin.add_news_and_media') }}</span>
                        </a>
                    </li>
                    <li class="nav-item start {{ (Request::is(AD.'/news-and-media')) ? 'active' :'' }}">
                        <a href="{{ url(AD.'/news-and-media') }}" class="nav-link ">
                            <i class="icon-eye"></i>
                            <span class="title">{{ trans('admin.show_news_and_media') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- End News And Media Area -->

            <!-- Start Sliders Area -->
            <li class="nav-item start {{ (Request::segment(2)=='sliders') ? 'active' :'' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-sliders"></i>
                    <span class="title">{{ trans('admin.sliders') }}</span>
                    <span class="selected"></span>
                    <span class="arrow {{ (Request::segment(2)=='sliders') ? 'open' :'' }}"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start {{ (Request::segment(2)=='sliders' && Request::segment(3)=='create') ? 'active' :'' }}">
                        <a href="{{ url(AD.'/sliders/create') }}" class="nav-link ">
                            <i class="fa fa-plus-circle"></i>
                            <span class="title">{{ trans('admin.add_slider') }}</span>
                        </a>
                    </li>
                    <li class="nav-item start {{ (Request::is(AD.'/sliders')) ? 'active' :'' }}">
                        <a href="{{ url(AD.'/sliders') }}" class="nav-link ">
                            <i class="icon-eye"></i>
                            <span class="title">{{ trans('admin.show_sliders') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- End Sliders Area -->

            <!-- Start Partner Area -->
            <li class="nav-item start {{ (Request::segment(2)=='partner') ? 'active' :'' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">{{ trans('admin.partner') }}</span>
                    <span class="selected"></span>
                    <span class="arrow {{ (Request::segment(2)=='partner') ? 'open' :'' }}"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start {{ (Request::segment(2)=='partner' && Request::segment(3)=='create') ? 'active' :'' }}">
                        <a href="{{ url(AD.'/partner/create') }}" class="nav-link ">
                            <i class="fa fa-plus-circle"></i>
                            <span class="title">{{ trans('admin.add_partner') }}</span>
                        </a>
                    </li>
                    <li class="nav-item start {{ (Request::is(AD.'/partner')) ? 'active' :'' }}">
                        <a href="{{ url(AD.'/partner') }}" class="nav-link ">
                            <i class="icon-eye"></i>
                            <span class="title">{{ trans('admin.show_partner') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- End Partner Area -->

            <!-- Start Career Area -->
            <li class="nav-item start {{ (Request::segment(2)=='career') ? 'active' :'' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-newspaper-o"></i>
                    <span class="title">{{ trans('admin.career') }}</span>
                    <span class="selected"></span>
                    <span class="arrow {{ (Request::segment(2)=='career') ? 'open' :'' }}"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start {{ (Request::segment(2)=='career' && Request::segment(3)=='create') ? 'active' :'' }}">
                        <a href="{{ url(AD.'/career/create') }}" class="nav-link ">
                            <i class="fa fa-plus-circle"></i>
                            <span class="title">{{ trans('admin.add_career') }}</span>
                        </a>
                    </li>
                    <li class="nav-item start {{ (Request::is(AD.'/career')) ? 'active' :'' }}">
                        <a href="{{ url(AD.'/career') }}" class="nav-link ">
                            <i class="icon-eye"></i>
                            <span class="title">{{ trans('admin.show_career') }}</span>
                        </a>
                    </li>
                    <li class="nav-item start {{ (Request::is(AD.'/show-careers-cv')) ? 'active' :'' }}">
                        <a href="{{ url(AD.'/show-careers-cv') }}" class="nav-link ">
                            <i class="icon-eye"></i>
                            <span class="title">{{ trans('admin.show_career_cv') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- End Career Area -->

            <!-- Start Settings Area -->
            <li class="nav-item start {{ (Request::segment(2)=='settings') ? 'active' :'' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-bar-chart-o"></i>
                    <span class="title">{{ trans('admin.settings') }}</span>
                    <span class="selected"></span>
                    <span class="arrow {{ (Request::segment(2)=='settings') ? 'open' :'' }}"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start {{ (Request::segment(2)=='settings' && Request::segment(3)=='create') ? 'active' :'' }}">
                        <a href="{{ url(AD.'/settings/create') }}" class="nav-link ">
                            <i class="fa fa-plus-circle"></i>
                            <span class="title">{{ trans('admin.add_analytics') }}</span>
                        </a>
                    </li>

                    <li class="nav-item start {{ (Request::is(AD.'/settings')) ? 'active' :'' }}">
                        <a href="{{ url(AD.'/settings') }}" class="nav-link ">
                            <i class="icon-eye"></i>
                            <span class="title">{{ trans('admin.show_analytics') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- End Settings Area -->

            <!-- Start Reports Area -->
            <li class="nav-item start {{ (Request::segment(2)=='customers') ? 'active' :'' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-newspaper-o"></i>
                    <span class="title">{{ trans('admin.reports') }}</span>
                    <span class="selected"></span>
                    <span class="arrow {{ (Request::segment(2)=='customers') ? 'open' :'' }}"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start {{ (Request::segment(3)=='registered-sectors') ? 'active' :'' }}">
                        <a href="{{ url(AD.'/customers/registered-sectors') }}" class="nav-link ">
                            <i class="fa fa-users"></i>
                            <span class="title">{{ trans('admin.registered_sectors') }}</span>
                        </a>
                    </li>

                    <li class="nav-item start {{ (Request::segment(3)=='registered-products') ? 'active' :'' }}">
                        <a href="{{ url(AD.'/customers/registered-products') }}" class="nav-link ">
                            <i class="fa fa-users"></i>
                            <span class="title">{{ trans('admin.registered_products') }}</span>
                        </a>
                    </li>

                    <li class="nav-item start {{ (Request::segment(3)=='contact-us') ? 'active' :'' }}">
                        <a href="{{ url(AD.'/customers/contact-us') }}" class="nav-link ">
                            <i class="fa fa-users"></i>
                            <span class="title">{{ trans('admin.contact_us') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- End Reports Area -->

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>