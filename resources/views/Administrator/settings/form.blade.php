@extends(ADL.'.master')
@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet light portlet-fit portlet-form bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">{{ Route::currentRouteName() }}</span>
                </div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN FORM-->
                @if(!empty($form_data))
			        {!! Form::model($form_data,['files'=>true,'method'=>'PATCH','url'=>'/'.AD.'/settings/'.$form_data->id, 'id'=>'form_sample_3', 'class'=>'form-horizontal']) !!}
			        @else
			          {!! Form::open(['files'=>true,'method'=>'POST','id'=>'form_sample_3','route'=>'settings.store', 'class'=>'form-horizontal']) !!}
			    @endif

                    <div class="form-body">        
                        <div class="form-group">
                        	<label for="title" class="col-md-3 control-label">
                        		{{ trans('admin.title') }} <span class="required"> * </span>
                            </label>

                            <div class="col-md-6">
                            	<div class="input-group">
                            		<span class="input-group-addon">
	                                    <i class="fa fa-language"></i>
	                                </span>
					            	{!! Form::text('title',old('title'), array('id'=>'title', 'class'=>'form-control','required'=>'required','placeholder'=>trans('admin.title'))) !!}
                            	</div>
                            	@if($errors->has('title'))
						          <span class="help-block text-danger">{{ $errors->first('title') }}</span>
						        @endif
					        </div>
                        </div>

                        <div class="form-group">
                            <label for="location" class="col-md-3 control-label">
                                {{ trans('admin.location') }} <span class="required">Location in Page*</span>
                            </label>
                            <div class="col-md-6">
                                <div class="mt-radio-inline">
                                    <label class="mt-radio">
                                        {!! Form::radio('location', '1','true',array('class'=>'location')) !!}
                                        {{ trans('admin.header') }}
                                        <span></span>
                                    </label>
                                    <label class="mt-radio">
                                        {!! Form::radio('location', '2','true',array('class'=>'location')) !!}
                                        {{ trans('admin.footer') }}
                                        <span></span>
                                    </label>
                                    <label class="mt-radio">
                                        {!! Form::radio('location', '0','',array('class'=>'location')) !!}
                                        {{ trans('admin.both') }}
                                        <span></span>
                                    </label>
                                </div>
                                @if($errors->has('location'))
                                    <span class="help-block text-danger">{{ $errors->first('location') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status" class="col-md-3 control-label">
                                {{ trans('admin.status') }} <span class="required">If On will appear on website*</span>
                            </label>
							<div class="col-md-6">
								<div class="mt-radio-inline">
								  <label class="mt-radio">
									{!! Form::radio('status', '1','true',array('class'=>'status')) !!}
									{{ trans('admin.on') }}
									<span></span>
								  </label>
								  <label class="mt-radio">
									{!! Form::radio('status', '0','',array('class'=>'status')) !!}
									{{ trans('admin.off') }}
									<span></span>
								  </label>
								</div>
								@if($errors->has('status'))
								  <span class="help-block text-danger">{{ $errors->first('status') }}</span>
								@endif
							</div>
                        </div>

                        <div class="form-group">
                            <label for="script" class="col-md-3 control-label">
                                {{ trans('admin.script') }} <span class="required"> * </span>
                            </label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-code"></i>
                                    </span>
                                    {!! Form::textarea('script',old('script'), array('id'=>'script', 'class'=>'form-control','required'=>'required','placeholder'=>trans('admin.script'))) !!}
                                </div>
                                @if($errors->has('script'))
                                    <span class="help-block text-danger">{{ $errors->first('script') }}</span>
                                @endif
                            </div>
                        </div>

                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                            	{!! Form::submit($submit_button, array('class'=>'btn green')) !!}
                                <a href="{{ url(AD.'/settings') }}" class="btn red">{{ trans('admin.cancel') }}</a>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                <!-- END FORM-->
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
</div>

@stop