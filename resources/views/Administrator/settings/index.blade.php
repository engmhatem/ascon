@extends(ADL.'.master')
@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="btn-group">
                    <a href="{{ url(AD.'/settings/create') }}" class="btn sbold green"> Add New <i class="fa fa-plus"></i></a>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
            	@if(!empty($all) && count($all)>0)
	                <table class="table table-striped table-bordered table-hover" id="sample_1">
	                    <thead>
	                    	<tr>
					            <th>
					            	<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
		                                    <input type="checkbox" name="check_all" id="check_all" class="checkboxes"/>
		                                    <span></span>Select All &nbsp;
		                            </label>

					              	<button type="button" class="btn btn-danger delete_all_selected" data-toggle="modal" data-target="#myModalSelected" id="delete_all_selected" disabled="disabled">Delete All Selected</button>
					            </th>
				          	</tr>
	                        <tr>
	                            <th>ID</th>
					          	<th>{{ trans('admin.title') }}</th>
					          	<th>{{ trans('admin.location') }}</th>
					          	<th>{{ trans('admin.status') }}</th>
					          	<th>{{ trans('admin.action') }}</th>
	                        </tr>
	                    </thead>
	                    <tfoot>
                            <tr>
	                            <th>ID</th>
								<th>{{ trans('admin.title') }}</th>
								<th>{{ trans('admin.location') }}</th>
								<th>{{ trans('admin.status') }}</th>
								<th>{{ trans('admin.action') }}</th>
	                        </tr>
	                    </tfoot>
	                    <tbody>
	                        @foreach($all as $single)
						        <tr id="user_{{ $single->id }}" @if($single->status == 0)class="danger" @endif>
						            <td>
						            	<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
		                                    <input type="checkbox" name="perm[]" class="All_Checkbox checkboxes" value="{{ $single->id }}" />
		                                    <span></span>{{ $single->id }}
		                                </label>
						            </td>
						            <td>{{ $single->title }}</td>
						            <td>{{ config('ascon.location.'.$single->location) }}</td>
						            <td>
						                @if($single->status == 1)
						                	<span class="label label-sm label-success label-mini">{{ trans('admin.on') }}</span>
						                  @else
						                  <span class="label label-sm label-danger label-mini">{{ trans('admin.off') }}</span>
						                @endif
						            </td>
						              
						            <td>
						            	<a href="{{url(AD.'/settings/'.$single->id.'/edit')}}" class="btn btn-outline btn-circle btn-sm purple">
                                        	<i class="fa fa-edit"></i>
                                        </a>

										<button type="button" class="btn btn-outline btn-circle red btn-sm black delete_button" data-id="{{ $single->id }}" data-toggle="modal" data-target="#myModal"><i class="fa fa-trash-o"></i></button>
						            </td>
						        </tr>
					        @endforeach
	                    </tbody>
	                </table>
                @else
			    	<p class="text-warning">{{ trans('admin.empty_data') }}</p>
			    @endif
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<!-- Modal to delete single item -->
<div class="modal fade" id="myModal" role="dialog">
<div class="modal-dialog modal-sm">
  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title">{{trans('admin.confirm_delete')}}</h4>
    </div>
    <div class="modal-body">
      {{ Form::open(['route' => ['sliders.destroy', '1'],'method'=>'DELETE','role'=>'form', 'class'=>'form-horizontal', 'id'=>'Delete_form']) }}
      {!! Form::submit(trans('admin.delete'), array('class'=>'btn btn-danger')) !!}
      <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{trans('admin.cancel')}}</button>
    {{ Form::close() }}
    </div>
  </div>
</div>
</div>
@stop