@extends(ADL.'.master')
@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <!-- <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Buttons &nbsp;</span>
                </div> -->
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
            	@if(!empty($all) && count($all)>0)
	                <table class="table table-striped table-bordered table-hover" id="sample_1">
	                    <thead>
	                        <tr>
								<th>{{ trans('admin.name') }}</th>
					          	<th>{{ trans('admin.country') }}</th>
					          	<th>{{ trans('admin.phone') }}</th>
								<th>{{ trans('admin.message') }}</th>
								<th>{{ trans('admin.created_at') }}</th>
	                        </tr>
	                    </thead>
	                    <tfoot>
                            <tr>
								<th>{{ trans('admin.name') }}</th>
								<th>{{ trans('admin.country') }}</th>
								<th>{{ trans('admin.phone') }}</th>
								<th>{{ trans('admin.message') }}</th>
								<th>{{ trans('admin.created_at') }}</th>
	                        </tr>
	                    </tfoot>
	                    <tbody>
	                        @foreach($all as $single)
						        <tr id="user_{{ $single->id }}">
						            {{--<td>--}}
						            	{{--<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">--}}
		                                    {{--<input type="checkbox" name="perm[]" class="All_Checkbox checkboxes" value="{{ $single->id }}" />--}}
		                                    {{--<span></span>{{ $single->name }}--}}
		                                {{--</label>--}}
						            {{--</td>--}}
						            <td>{{ $single->first_name }} {{ $single->last_name }}</td>
						            <td>{{ $single->country }}</td>
						            <td>{{ $single->phone }}</td>
						            <td>{{ $single->message }}</td>
						            <td>{{ $single->created_at }}</td>
						        </tr>
					        @endforeach
	                    </tbody>
	                </table>
                @else
			    	<p class="text-warning">{{ trans('admin.empty_data') }}</p>
			    @endif
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
@stop