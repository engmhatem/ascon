@extends(ADL.'.master')
@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
            	@if(!empty($all) && count($all)>0)
	                <table class="table table-striped table-bordered table-hover" id="sample_1">
	                    <thead>
	                        <tr>
								<th>{{ trans('admin.product') }}</th>
								<th>{{ trans('admin.name') }}</th>
					          	<th>{{ trans('admin.email') }}</th>
					          	<th>{{ trans('admin.phone') }}</th>
								<th>{{ trans('admin.message') }}</th>
								<th>{{ trans('admin.created_at') }}</th>
	                        </tr>
	                    </thead>
	                    <tfoot>
                            <tr>
								<th>{{ trans('admin.product') }}</th>
								<th>{{ trans('admin.name') }}</th>
								<th>{{ trans('admin.email') }}</th>
								<th>{{ trans('admin.phone') }}</th>
								<th>{{ trans('admin.message') }}</th>
								<th>{{ trans('admin.created_at') }}</th>
	                        </tr>
	                    </tfoot>
	                    <tbody>
	                        @foreach($all as $single)
						        <tr id="user_{{ $single->id }}">
						            <td>{{ config('ascon.products.'.$single->product_id) }}</td>
						            <td>{{ $single->name }}</td>
						            <td>{{ $single->email }}</td>
						            <td>{{ $single->phone }}</td>
						            <td>{{ $single->message }}</td>
						            <td>{{ $single->created_at }}</td>
						        </tr>
					        @endforeach
	                    </tbody>
	                </table>
                @else
			    	<p class="text-warning">{{ trans('admin.empty_data') }}</p>
			    @endif
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
@stop