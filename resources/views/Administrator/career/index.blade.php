@extends(ADL.'.master')
@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <!-- <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Buttons &nbsp;</span>
                </div> -->
                <div class="btn-group">
                    <a href="{{ url(AD.'/career/create') }}" class="btn sbold green"> Add New <i class="fa fa-plus"></i></a>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
            	@if(!empty($all) && count($all)>0)
	                <table class="table table-striped table-bordered table-hover" id="sample_1">
	                    <thead>
	                    	<tr>
					            <th>
					            	<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
		                                    <input type="checkbox" name="check_all" id="check_all" class="checkboxes"/>
		                                    <span></span>Select All &nbsp;
		                            </label>

					              	<button type="button" class="btn btn-danger delete_all_selected" data-toggle="modal" data-target="#myModalSelected" id="delete_all_selected" disabled="disabled">Delete All Selected</button>
					            </th>
				          	</tr>
	                        <tr>
					          	<th>{{ trans('admin.ar_title') }}</th>
					          	<th>{{ trans('admin.en_title') }}</th>
					          	<th>{{ trans('admin.image') }}</th>
								<th>{{ trans('admin.publish_date') }}</th>
								<th>{{ trans('admin.close_date') }}</th>
					          	<th>{{ trans('admin.action') }}</th>
	                        </tr>
	                    </thead>
	                    <tfoot>
                            <tr>
					          	<th>{{ trans('admin.ar_title') }}</th>
					          	<th>{{ trans('admin.en_title') }}</th>
					          	<th>{{ trans('admin.image') }}</th>
					          	<th>{{ trans('admin.publish_date') }}</th>
								<th>{{ trans('admin.close_date') }}</th>
								<th>{{ trans('admin.action') }}</th>
	                        </tr>
	                    </tfoot>
	                    <tbody>
	                        @foreach($all as $single)
						        <tr id="user_{{ $single->id }}">
						            <td>
						            	<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
		                                    <input type="checkbox" name="perm[]" class="All_Checkbox checkboxes" value="{{ $single->id }}" />
		                                    <span></span>{{ $single->ar_title }}
		                                </label>
						            </td>
						            <td>{{ $single->en_title }}</td>
									<td>
										@if($single->image)
											<a href="{{ $single->image }}" data-lightbox="image-1" data-title="{{ $single->ar_title }}" class="text-success">
												Show <i class="fa fa-image"></i>
											</a>
										@else
											<span class="text-danger">No Image</span>
										@endif
									</td>
						            <td>{{ $single->publish_date }}</td>
						            <td>{{ $single->close_date }}</td>

						            <td>
						            	<a href="{{url('Administrator/career/'.$single->id.'/edit')}}" class="btn btn-outline btn-circle btn-sm purple">
                                        	<i class="fa fa-edit"></i>
                                        </a>
										<button type="button" class="btn btn-outline btn-circle red btn-sm black delete_button" data-id="{{ $single->id }}" data-toggle="modal" data-target="#myModal"><i class="fa fa-trash-o"></i></button>
									</td>
						        </tr>
					        @endforeach
	                    </tbody>
	                </table>
                @else
			    	<p class="text-warning">{{ trans('admin.empty_data') }}</p>
			    @endif
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
@stop