@extends(ADL.'.master')
@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <!-- <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Buttons &nbsp;</span>
                </div> -->
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
            	@if(!empty($all) && count($all)>0)
	                <table class="table table-striped table-bordered table-hover" id="sample_1">
	                    <thead>
	                    	{{--<tr>--}}
					            {{--<th>--}}
					            	{{--<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">--}}
		                                    {{--<input type="checkbox" name="check_all" id="check_all" class="checkboxes"/>--}}
		                                    {{--<span></span>Select All &nbsp;--}}
		                            {{--</label>--}}

					              	{{--<button type="button" class="btn btn-danger delete_all_selected" data-toggle="modal" data-target="#myModalSelected" id="delete_all_selected" disabled="disabled">Delete All Selected</button>--}}
					            {{--</th>--}}
				          	{{--</tr>--}}
	                        <tr>
					          	<th>{{ trans('admin.career') }}</th>
					          	<th>{{ trans('admin.name') }}</th>
					          	<th>{{ trans('admin.email') }}</th>
					          	<th>{{ trans('admin.current_title') }}</th>
								<th>{{ trans('admin.current_salary') }}</th>
								<th>{{ trans('admin.expected_join_date') }}</th>
								<th>{{ trans('admin.current_location_city') }}</th>
								<th>{{ trans('admin.CV_file') }}</th>
{{--					          	<th>{{ trans('admin.action') }}</th>--}}
	                        </tr>
	                    </thead>
	                    <tfoot>
                            <tr>
								<th>{{ trans('admin.career') }}</th>
								<th>{{ trans('admin.name') }}</th>
								<th>{{ trans('admin.email') }}</th>
								<th>{{ trans('admin.current_title') }}</th>
								<th>{{ trans('admin.current_salary') }}</th>
								<th>{{ trans('admin.expected_join_date') }}</th>
								<th>{{ trans('admin.current_location_city') }}</th>
								<th>{{ trans('admin.CV_file') }}</th>
{{--								<th>{{ trans('admin.action') }}</th>--}}
	                        </tr>
	                    </tfoot>
	                    <tbody>
	                        @foreach($all as $single)
						        <tr id="user_{{ $single->id }}">
						            <td>
						            	<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
		                                    {{--<input type="checkbox" name="perm[]" class="All_Checkbox checkboxes" value="{{ $single->id }}" />--}}
		                                    <span></span>{{ $single->career->en_title }}
		                                </label>
						            </td>
						            <td>{{ $single->name }}</td>
						            <td>{{ $single->email }}</td>
						            <td>{{ $single->current_title }}</td>
						            <td>{{ $single->current_salary }}</td>
						            <td>{{ $single->expected_join_date }}</td>
						            <td>{{ $single->current_location_city }}</td>
									<td>
										@if($single->CV_file)
											<a href="{{ $single->CV_file }}" class="text-success">
												Show <i class="fa fa-pdf"></i>
											</a>
										@else
											<span class="text-danger">No CV</span>
										@endif
									</td>

						            {{--<td>--}}
										{{--<button type="button" class="btn btn-outline btn-circle red btn-sm black delete_button" data-id="{{ $single->id }}" data-toggle="modal" data-target="#myModal"><i class="fa fa-trash-o"></i></button>--}}
									{{--</td>--}}
						        </tr>
					        @endforeach
	                    </tbody>
	                </table>
                @else
			    	<p class="text-warning">{{ trans('admin.empty_data') }}</p>
			    @endif
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
@stop