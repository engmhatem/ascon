@extends(ADL.'.master')
@section('content')

	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN VALIDATION STATES-->
			<div class="portlet light portlet-fit portlet-form bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-settings font-dark"></i>
						<span class="caption-subject font-dark sbold uppercase">{{ Route::currentRouteName() }}</span>
					</div>
					@if(!empty($form_data))
						<div class="col-xs-6">
							@if(!empty($form_data->image))
								<img src="{{ $form_data->image }}" class="img-thumbnail" style="max-height: 120px !important;">
							@endif
						</div>
					@endif
				</div>
				<div class="portlet-body">
					<!-- BEGIN FORM-->
					@if(!empty($form_data))
						{!! Form::model($form_data,['files'=>true,'method'=>'PATCH','url'=>'/'.AD.'/career/'.$form_data->id, 'id'=>'form_sample_3', 'class'=>'form-horizontal']) !!}
					@else
						{!! Form::open(['files'=>true,'method'=>'POST','id'=>'form_sample_3','route'=>'career.store', 'class'=>'form-horizontal']) !!}
					@endif

					<div class="form-body">
						<div class="form-group">
							<label for="ar_title" class="col-md-3 control-label">
								{{ trans('admin.ar_title') }} <span class="required"> * </span>
							</label>

							<div class="col-md-6">
								<div class="input-group">
                            		<span class="input-group-addon">
	                                    <i class="fa fa-file-text"></i>
	                                </span>
									{!! Form::text('ar_title',old('ar_title'), array('id'=>'ar_title', 'class'=>'form-control','required'=>'required','placeholder'=>trans('admin.ar_title'))) !!}
								</div>
								@if($errors->has('ar_title'))
									<span class="help-block text-danger">{{ $errors->first('ar_title') }}</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="en_title" class="col-md-3 control-label">
								{{ trans('admin.en_title') }} <span class="required"> * </span>
							</label>
							<div class="col-md-6">
								<div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-file-text"></i>
                                    </span>
									{!! Form::text('en_title',old('en_title'), array('id'=>'en_title', 'class'=>'form-control','required'=>'required','placeholder'=>trans('admin.en_title'))) !!}
								</div>
								@if($errors->has('en_title'))
									<span class="help-block text-danger">{{ $errors->first('en_title') }}</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="image" class="col-md-3 control-label">
								{{ trans('admin.image') }}
								@if($required)<span class="required"> * </span>@endif
							</label>
							<div class="col-md-6">
								<div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-image"></i>
                                    </span>
									{!! Form::file('image',array('id'=>'image', 'class'=>'form-control',$required,'placeholder'=>trans('admin.image'))) !!}
								</div>
								<div class="margin-bottom-25">
									<span class="note note-warning">Dimensions width min=<strong>1200</strong> max=<strong>1500</strong> & height min=<strong>300</strong> max=<strong>500</strong></span>
								</div>
								@if($errors->has('image'))
									<span class="help-block text-danger">{{ $errors->first('image') }}</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="publish_date" class="col-md-3 control-label">
								{{ trans('admin.publish_date') }} <span class="required"> * </span>
							</label>
							<div class="col-md-6">
								<div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
									{!! Form::text('publish_date',old('publish_date'), array('id'=>'publish_date', 'class'=>'form-control date_time_picker','required'=>'required','placeholder'=>trans('admin.publish_date'))) !!}
								</div>
								@if($errors->has('publish_date'))
									<span class="help-block text-danger">{{ $errors->first('publish_date') }}</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="close_date" class="col-md-3 control-label">
								{{ trans('admin.close_date') }} <span class="required"> * </span>
							</label>
							<div class="col-md-6">
								<div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
									{!! Form::text('close_date',old('close_date'), array('id'=>'close_date', 'class'=>'form-control date_time_picker','required'=>'required','placeholder'=>trans('admin.close_date'))) !!}
								</div>
								@if($errors->has('close_date'))
									<span class="help-block text-danger">{{ $errors->first('close_date') }}</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="position_code" class="col-md-3 control-label">
								{{ trans('admin.position_code') }} <span class="required">* </span>
							</label>
							<div class="col-md-6">
								<div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-code"></i>
                                    </span>
									{!! Form::text('position_code',old('position_code'), array('id'=>'position_code', 'class'=>'form-control','required'=>'required','placeholder'=>trans('admin.position_code'))) !!}
								</div>
								@if($errors->has('position_code'))
									<span class="help-block text-danger">{{ $errors->first('position_code') }}</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="ar_location" class="col-md-3 control-label">
								{{ trans('admin.ar_location') }} <span class="required">* </span>
							</label>
							<div class="col-md-6">
								<div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-location-arrow"></i>
                                    </span>
									{!! Form::text('ar_location',old('ar_location'), array('id'=>'ar_location', 'class'=>'form-control','required'=>'required','placeholder'=>trans('admin.ar_location'))) !!}
								</div>
								@if($errors->has('ar_location'))
									<span class="help-block text-danger">{{ $errors->first('ar_location') }}</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="en_location" class="col-md-3 control-label">
								{{ trans('admin.en_location') }} <span class="required">* </span>
							</label>
							<div class="col-md-6">
								<div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-location-arrow"></i>
                                    </span>
									{!! Form::text('en_location',old('en_location'), array('id'=>'en_location', 'class'=>'form-control','required'=>'required','placeholder'=>trans('admin.en_location'))) !!}
								</div>
								@if($errors->has('en_location'))
									<span class="help-block text-danger">{{ $errors->first('en_location') }}</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="ar_description" class="col-md-3 control-label">
								{{ trans('admin.ar_description') }} <span class="required"> * </span>
							</label>
							<div class="col-md-6">
								<div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-pencil"></i>
                                    </span>
									{!! Form::textarea('ar_description',old('ar_description'), array('id'=>'ar_description', 'class'=>'form-control','required'=>'required','placeholder'=>trans('admin.ar_description'))) !!}
								</div>
								@if($errors->has('ar_description'))
									<span class="help-block text-danger">{{ $errors->first('ar_description') }}</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="en_description" class="col-md-3 control-label">
								{{ trans('admin.en_description') }} <span class="required"> * </span>
							</label>
							<div class="col-md-6">
								<div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-pencil"></i>
                                    </span>
									{!! Form::textarea('en_description',old('en_description'), array('id'=>'en_description', 'class'=>'form-control','required'=>'required','placeholder'=>trans('admin.en_description'))) !!}
								</div>
								@if($errors->has('en_description'))
									<span class="help-block text-danger">{{ $errors->first('en_description') }}</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="ar_special_requirements" class="col-md-3 control-label">
								{{ trans('admin.ar_special_requirements') }} <span class="required"> * </span>
							</label>
							<div class="col-md-6">
								<div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-pencil"></i>
                                    </span>
									{!! Form::textarea('ar_special_requirements',old('ar_special_requirements'), array('id'=>'ar_special_requirements', 'class'=>'form-control','required'=>'required','placeholder'=>trans('admin.ar_special_requirements'))) !!}
								</div>
								@if($errors->has('ar_special_requirements'))
									<span class="help-block text-danger">{{ $errors->first('ar_special_requirements') }}</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="en_special_requirements" class="col-md-3 control-label">
								{{ trans('admin.en_special_requirements') }} <span class="required"> * </span>
							</label>
							<div class="col-md-6">
								<div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-pencil"></i>
                                    </span>
									{!! Form::textarea('en_special_requirements',old('en_special_requirements'), array('id'=>'en_special_requirements', 'class'=>'form-control','required'=>'required','placeholder'=>trans('admin.en_special_requirements'))) !!}
								</div>
								@if($errors->has('en_special_requirements'))
									<span class="help-block text-danger">{{ $errors->first('en_special_requirements') }}</span>
								@endif
							</div>
						</div>

					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								{!! Form::submit($submit_button, array('class'=>'btn green')) !!}
								<a href="{{ url(AD.'/career') }}" class="btn red">{{ trans('admin.cancel') }}</a>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
				<!-- END FORM-->
				</div>
				<!-- END VALIDATION STATES-->
			</div>
		</div>
	</div>

@stop