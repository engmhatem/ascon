@extends(ADL.'.master')
@section('content')

	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN VALIDATION STATES-->
			<div class="portlet light portlet-fit portlet-form bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-settings font-dark"></i>
						<span class="caption-subject font-dark sbold uppercase">{{ Route::currentRouteName() }}</span>
					</div>
                    @if(!empty($form_data))
                        <div class="col-xs-6">
                            @if(!empty($form_data->image))
                                <img src="{{ $form_data->image }}" class="img-thumbnail" style="max-height: 120px !important;">
                            @endif
                        </div>
                    @endif
				</div>
				<div class="portlet-body">
					<!-- BEGIN FORM-->
					@if(!empty($form_data))
						{!! Form::model($form_data,['method'=>'PATCH','files'=>true,'url'=>'/'.AD.'/partner/'.$form_data->id, 'id'=>'form_sample_3', 'class'=>'form-horizontal']) !!}
					@else
						{!! Form::open(['method'=>'POST','files'=>true,'id'=>'form_sample_3','route'=>'partner.store', 'class'=>'form-horizontal']) !!}
					@endif

					<div class="form-body">
						<div class="form-group">
							<label for="ar_title" class="col-md-3 control-label">
								{{ trans('admin.ar_title') }} <span class="required"> * </span>
							</label>

							<div class="col-md-6">
								<div class="input-group">
                            		<span class="input-group-addon">
	                                    <i class="fa fa-file-text"></i>
	                                </span>
									{!! Form::text('ar_title',old('ar_title'), array('id'=>'ar_title', 'class'=>'form-control','required'=>'required','placeholder'=>trans('admin.ar_title'))) !!}
								</div>
								@if($errors->has('ar_title'))
									<span class="help-block text-danger">{{ $errors->first('ar_title') }}</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="en_title" class="col-md-3 control-label">
								{{ trans('admin.en_title') }} <span class="required"> * </span>
							</label>
							<div class="col-md-6">
								<div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-file-text"></i>
                                    </span>
									{!! Form::text('en_title',old('en_title'), array('id'=>'en_title', 'class'=>'form-control','required'=>'required','placeholder'=>trans('admin.en_title'))) !!}
								</div>
								@if($errors->has('en_title'))
									<span class="help-block text-danger">{{ $errors->first('en_title') }}</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="link" class="col-md-3 control-label">
								{{ trans('admin.link') }} <span class="required"> * </span>
							</label>
							<div class="col-md-6">
								<div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-link"></i>
                                    </span>
									{!! Form::text('link',old('link'), array('id'=>'link', 'class'=>'form-control','required'=>'required','placeholder'=>trans('admin.link'))) !!}
								</div>
								@if($errors->has('link'))
									<span class="help-block text-danger">{{ $errors->first('link') }}</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="status" class="col-md-3 control-label">
								{{ trans('admin.status') }} <span class="required">If On will appear on website*</span>
							</label>
							<div class="col-md-6">
								<div class="mt-radio-inline">
									<label class="mt-radio">
										{!! Form::radio('status', '1','true',array('class'=>'status')) !!}
										{{ trans('admin.on') }}
										<span></span>
									</label>
									<label class="mt-radio">
										{!! Form::radio('status', '0','',array('class'=>'status')) !!}
										{{ trans('admin.off') }}
										<span></span>
									</label>
								</div>
								@if($errors->has('status'))
									<span class="help-block text-danger">{{ $errors->first('status') }}</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="image" class="col-md-3 control-label">
								{{ trans('admin.image') }}
								@if($required)<span class="required"> * </span>@endif
							</label>
							<div class="col-md-6">
								<div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-image"></i>
                                    </span>
									{!! Form::file('image',array('id'=>'image', 'class'=>'form-control',$required,'placeholder'=>trans('admin.image'))) !!}
								</div>
								<div class="margin-bottom-25">
									<span class="note note-warning">Dimensions width <strong>160</strong> & height <strong>80</strong></span>
								</div>
								@if($errors->has('image'))
									<span class="help-block text-danger">{{ $errors->first('image') }}</span>
								@endif
							</div>
						</div>

					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								{!! Form::submit($submit_button, array('class'=>'btn green')) !!}
								<a href="{{ url(AD.'/partner') }}" class="btn red">{{ trans('admin.cancel') }}</a>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
				<!-- END FORM-->
				</div>
				<!-- END VALIDATION STATES-->
			</div>
		</div>
	</div>

@stop