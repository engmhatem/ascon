@extends(FEL.'.master')

@section('content')
    <section class="about-bg">
        <h1>{{ trans('main.about_us') }}</h1>
    </section>
@if(session()->get('lang') =='en')
    <!--Start Single Page-->
    <div class="single_page">
        <!--Start About Us Page Section-->
        <section class="aboutus_page-section aboutus_page-section-left">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-5 aboutus_page-img" data-src="{{ asset('public/Frontend/images/about-page4.png') }}">
                    </div>

                    <div class="col-sm-12 col-md-7 paragraph">
                        <h2>About Us </h2>
                        <p>
                            Arab Computer Systems (ASCON) was established since 1990 to design and implement computer and financial software application systems. our twenty-eight years of experience have given us the hard ground to study and implement the requirements of the Gulf and Saudi business market in particular, (UNIX, Linux, and windows)
                            <br/>
                            Ascon has always been using the latest and best technologies, so it chose to work using Oracle databases with all its accessories and requirements
                            Ascon adopted in its applications the flexibility, simplicity and the use with the intensification of accurate and rapid reports, which play a key role for business owners to make the right decisions in the right time.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <!--Start About Us Page Section-->

        <!--Start About Us Page Section-->
        <section class="aboutus_page-section aboutus_page-section-right">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-7 paragraph">
                        <h2>Targeted Market</h2>
                        <p>
                            Ascon Targets the market of Saudi Arabia,  the Gulf States, Egypt, and some African and European countries by responding to and covering all their software complications.
                            <br/>
                            Ascon also targets certain sectors of the market, medium and large companies.
                        </p>
                    </div>
                    <div class="col-sm-12 col-md-5 aboutus_page-img" data-src="{{ asset('public/Frontend/images/about-page2.png') }}">
                    </div>
                </div>
            </div>
        </section>
        <!--Start About Us Page Section-->

        <!--Start About Us Page Section-->
        <section class="aboutus_page-section aboutus_page-section-left">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-5 aboutus_page-img" data-src="{{ asset('public/Frontend/images/about-page1.png') }}">
                    </div>
                    <div class="col-sm-12 col-md-7 paragraph">
                        <h2>Ascon Activity</h2>
                        <p>
                            - First software industry:
                            <br/>
                            Ascon manufactures various types and sizes of software packages in line with the needs of           the Arab markets and the latest technologies available, resulting in the acquisition of multiple packages of software to serve many sectors.
                            <br/>
                            - Second Consulting:
                            <br/>
                            It includes information and database insurance consulting, consulting for corporate restructuring, and applied consulting for Ascon systems in accordance with international standards and in accordance with the conditions and environment of the installations and their markets.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <!--Start About Us Page Section-->

        <!--Start About Us Page Section-->
        <section class="aboutus_page-section aboutus_page-section-right">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-7 paragraph">
                        <h2>Ascon Advantages</h2>
                        <p>
                            - All systems are available in both Arabic and English and web-enabled.
                            <br/>
                            - Ascon systems combine the financial and administrative aspects at the same time, helping the senior management for taking the right decisions.
                            <br/>
                            - Ascon's systems are flexible and simple to access screens, navigate between the system screens and guide users to alert messages.
                            <br/>
                            - Automatic tests to verify data accuracy while alerting users if any errors occur.
                            <br/>
                            - A secure system to manage users' access to the systems.
                            <br/>
                            - Secure data via backup system.
                            <br/>
                            - Testing and checking the data immediately to ensure no repetition.
                        </p>
                    </div>
                    <div class="col-sm-12 col-md-5 aboutus_page-img" data-src="{{ asset('public/Frontend/images/about-page3.png') }}">
                    </div>
                </div>
            </div>
        </section>
        <!--Start About Us Page Section-->
    </div>
    <!--End Single Page-->
    @else
    <!--Start Single Page-->
    <div class="single_page">
        <!--Start About Us Page Section-->
        <section class="aboutus_page-section aboutus_page-section-left">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-5 aboutus_page-img" data-src="{{ asset('public/Frontend/images/about-page4.png') }}">
                    </div>
                    <div class="col-sm-12 col-md-7 paragraph">
                        <h2>عن الشركة </h2>
                        <p>
                            تاسست شركة العرب لانظمة الكمبيوتر (اسكون) منذ العام 1990 لتصميم وتنفيذ انظمة تطبيقات برمجيات الحاسب الالى المالية والادارية وقد تجاوزت خبرتنا الثمانية وعشرون عاماً ومنحتنا الارض الصلبة لدراسة وتنفيذ متطلبات سوق الاعمال الخليجى والسعودى على وجه الخصوص من البرمجيات التى تعمل تحت مظلة انظمة التشغيل المختلفة <bdi>(UNIX, Linux , and windows)</bdi>
                            <br> وقد اخذت شركة اسكون على عاتقها استخدام احدث وافضل التقنيات ولهذا فقد اختارت العمل باستخدام قواعد البيانات اوراكل بكل ملحقاتها ومتطلباتها
                            <br> اعتمدت اسكون فى تطبيقاتها على المرونة والبساطة وسهولة الاستخدام مع تكثيف التقارير الدقيقة والسريعة والتى تلعب دوراً اساسياً لدى اصحاب الاعمال لاتخاذ القرارات المناسبة بالوقت المناسب .
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <!--Start About Us Page Section-->

        <!--Start About Us Page Section-->
        <section class="aboutus_page-section aboutus_page-section-right">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-7 paragraph">
                        <h2>السوق المستهدف </h2>
                        <p>
                            تخدم شركة اسكون سوق المملكة العربية السعودية ودول الخليج ومصر وبعض الدول الافريقية والاوربية من خلال الاستجابة وتغطية جميع احتياجاتها من الحلول البرمجية.
                            <br>
                            كما تستهدف شركة اسكون قطاعات معينة من السوق وهى قطاعات الشركات المتوسطة والكبيرة.
                        </p>
                    </div>
                    <div class="col-sm-12 col-md-5 aboutus_page-img" data-src="{{ asset('public/Frontend/images/about-page2.png') }}">
                    </div>
                </div>
            </div>
        </section>
        <!--Start About Us Page Section-->

        <!--Start About Us Page Section-->
        <section class="aboutus_page-section aboutus_page-section-left">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-5 aboutus_page-img" data-src="{{ asset('public/Frontend/images/about-page1.png') }}">
                    </div>
                    <div class="col-sm-12 col-md-7 paragraph">
                        <h2>نشاط اسكون</h2>
                        <p>أولا صناعة البرمجيات: <br/> تقوم اسكون بصناعة أنواع واحجام متعددة من حزم البرامج بما يتوافق مع احتياجات الأسواق العربية وبأحدث تقنيات متوفرة، ونتج عن ذلك امتلاك اسكون لحزم متعددة من البرمجيات تخدم العديد من القطاعات. </p>
                        <p>ثانيا الاستشارات: <br/> وتشمل استشارات تامين المعلومات وقواعد البيانات، واستشارات لإعادة هيكلة الشركات، والاستشارات التطبيقية لأنظمة اسكون وفق معايير عالمية وبما يتفق مع ظروف وبيئة المنشآت وأسواق تواجدها. </p>
                    </div>
                </div>
            </div>
        </section>
        <!--Start About Us Page Section-->

        <!--Start About Us Page Section-->
        <section class="aboutus_page-section aboutus_page-section-right">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-7 paragraph">
                        <h2>مزايا اسكون</h2>
                        <p>
                            تتميز كافة الانظمة بالعمل باللغتين العربية والانجليزية وبخاصية web-enabled .
                            <br>
                            تتميز انظمة اسكون بالجمع بين الجانبين المالى والادارى فى نفس الوقت مما يساعد الادارة العليا باتخاذ القرارات المناسبة بالوقت المناسب .
                            <br>
                            تتميز انظمة اسكون بالمرونة والبساطة فى الدخول للشاشات والتنقل بين شاشات النظام وارشاد المستخدمين بالرسائل التنبيهية .
                            <br>
                            الاختبارات التلقائية للتحقق من دقة البيانات مع تنبية المستخدمين حال حدوث اى اخطاء .
                            <br>
                            نظام لادارة صلاحيات المستخدمين لتأمين الدخول للانظمة
                            <br>
                            تامين البيانات عن طريق نظام النسخ الاحتياطى
                            <br>
                            الاختبار والتدقيق الفورى للبيانات لضمان عدم التكرار
                            <br>
                        </p>
                    </div>
                    <div class="col-sm-12 col-md-5 aboutus_page-img" data-src="{{ asset('public/Frontend/images/about-page3.png') }}">
                    </div>
                </div>
            </div>
        </section>
        <!--Start About Us Page Section-->
    </div>
    <!--End Single Page-->
@endif
@stop