@extends(FEL.'.master')

@section('content')
    <section class="about-bg">
        <h1>{{ trans('main.contact_us') }}</h1>
    </section>

    <section class="contact_us">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-5 contact_us-details ">
                    <div class="contact_us-branch">
                        <h3><b>{{ trans('main.saudi_arabia') }} - {{ trans('main.Riyadh') }}</b></h3>
                        <ul>
                            <li>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <span><a href="tel:+96614620694"><bdi>+ 966-1-4620694</bdi></a></span>
                                <span><a href="tel:+96614612677"><bdi>+ 966-1-4612677</bdi></a></span>
                            </li>

                            <li>
                                <i class="fa fa-fax" aria-hidden="true"></i>
                                <span><a href="tel:+96614664643"><bdi>+ 966-1-4664643</bdi></a></span>

                            </li>

                            <li>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span>{{ trans('main.address_riyadh') }}</span>
                            </li>

                            <li>
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <span><bdi><a href="mailto:ascon@ascon-me.com">ascon@ascon-me.com</a></bdi></span>
                            </li>
                        </ul>
                    </div>
                    <div class="contact_us-branch">
                        <h3><b>Egypt – Alexandria</b></h3>
                        <ul>
                            <li>
                                <i class="fa fa-fax" aria-hidden="true"></i>
                                <span><a href="tel:+2035772210"> <bdi>+203 577 2210</bdi></a></span>
                                <span><a href="tel:+2035749982"><bdi>+203 574 9982</bdi></a></span>
                            </li>

                            <li>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <span><a href="tel:201099922774"><bdi>+201099922774</bdi></a></span>

                            </li>

                            <li>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span><a href="https://goo.gl/maps/7CedLERvcRt" target="_blank">{{ trans('main.address_alex') }}</a></span>
                            </li>
                        </ul>
                    </div>

                    <div class="contact_us-branch">
                        <h3><b>Kenya – Nairobi</b></h3>
                        <ul>
                            <li>
                                <i class="fa fa-fax" aria-hidden="true"></i>
                                <span><a href="tel:+254704080808"> <bdi>+254 704080808</bdi></a></span>
                                <span><a href="tel:+254704191919"><bdi>+254 704191919</bdi></a></span>
                            </li>

                            <li>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span><bdi>FCB Mihrab Ground Floor No:B Postal</bdi></span>
                                <span><bdi>Address:P.O.BOX 76444-00508</bdi></span>
                            </li>
                        </ul>
                    </div>

                    <div class="contact_us-branch">
                        <h3><b>Turkey – Istanbul</b></h3>
                        <ul>
                            <li>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span><bdi>Ağaoğlu my office 212 – Kat 2 – ofis 24</bdi></span>
                                <span><bdi>Istanbul – Mahmutbey mahallesi</bdi></span>
                                <span><bdi>Address:P.O.BOX 76444-00508</bdi></span>
                            </li>

                            <li>
                                <i class="fa fa-fax" aria-hidden="true"></i>
                                <span><bdi>Posta kodu : 34218</bdi></span>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-12 col-lg-7">
                    <div class="form-card">
                        @if(session()->has('contact_success'))
                            <div class="alert alert-success">
                                {{ session()->get('contact_success') }}
                            </div>
                        @endif
                        <h3>{{ trans('main.contact_us') }}</h3>

                        {{ Form::open(['url'=>'contact-form']) }}

                            <div class="form-group col-sm-12 col-md-6">
                                {{ Form::text('first_name',old('first_name'),['class'=>'form-control','placeholder'=>trans('main.first_name'),'required'=>'required']) }}
                                {!! Form::hidden('lang',session()->get('lang')) !!}
                                @if($errors->has('first_name'))
                                    <p class="text-danger">{{ $errors->first('first_name') }}</p>
                                @endif
                            </div>

                            <div class="form-group col-sm-12 col-md-6">
                                {{ Form::text('last_name',old('last_name'),['class'=>'form-control','placeholder'=>trans('main.last_name'),'required'=>'required']) }}
                                @if($errors->has('last_name'))
                                    <p class="text-danger">{{ $errors->first('last_name') }}</p>
                                @endif
                            </div>

                            <div class="form-group col-sm-12 col-md-6">
                                {{ Form::text('phone',old('phone'),['class'=>'form-control','placeholder'=>trans('main.phone_no'),'required'=>'required']) }}
                                @if($errors->has('phone'))
                                    <p class="text-danger">{{ $errors->first('phone') }}</p>
                                @endif
                            </div>

                            <div class="form-group col-sm-12 col-md-6">
                                {{ Form::text('country',old('country'),['class'=>'form-control','placeholder'=>trans('main.country'),'required'=>'required']) }}
                                @if($errors->has('country'))
                                    <p class="text-danger">{{ $errors->first('country') }}</p>
                                @endif
                            </div>

                            <div class="form-group col-lg-12">
                                {{ Form::textarea('message',old('message'),['rows'=>5,'class'=>'form-control','placeholder'=>trans('main.message_txt'),'required'=>'required']) }}
                                @if($errors->has('message'))
                                    <p class="text-danger">{{ $errors->first('message') }}</p>
                                @endif
                            </div>
                            <button class="button">{{ trans('main.submit') }}</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop