@extends(FEL.'.master')

@section('content')

    <section class="about-bg">
        <h1>{{ trans('main.news') }}</h1>
    </section>

    {{--News and Media--}}
    @if(count($news))
        <section class="news">
            <div class="container">
                <div class="row">
                    @foreach($news as $new)
                        <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="news_block">
                            <aside class="news_img">
                                <img src="{{ $new->image }}" alt="{{ $new->{session()->get('lang').'_title'} }}"/>
                            </aside>
                            <aside class="news_description">
                                <div class="news-title"><span class="news-name">{{ str_limit($new->{session()->get('lang').'_title'},80) }}</span>
                                </div>
                                <span class="date"><i class="fa fa-calendar"></i> {{ $new->start_date->toFormattedDateString() }}</span>
                                <p>{!! str_limit($new->{session()->get('lang').'_description'},100) !!}</p>
                                <a href="{{ url(session()->get('lang').'/news/'.$new->id.'/'.str_replace(' ','-',$new->{session()->get('lang').'_title'})) }}" class="button">{{ trans('main.read_more')}}</a>
                            </aside>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-xs-12">{{ $news->links() }}</div>
                </div>
            </div>
        </section>
    @endif
@stop