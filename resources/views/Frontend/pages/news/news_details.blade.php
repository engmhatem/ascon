@extends(FEL.'.master')

@section('content')

    <section class="about-bg">
        <h1>{{ $news_and_media->{session()->get('lang').'_title'} }}</h1>
    </section>

    {{--News and Media--}}
    @if(!empty($news_and_media))
        <div class="news_details">
            <div class="container">
                <aside class="news_details-img"> <img src="{{ $news_and_media->image }}" alt="{{ $news_and_media->{session()->get('lang').'_title'} }}"> </aside>
                <div class="news_details-description">
                    <div class="news_details-title">
                        <aside>
                            <h2>{{ $news_and_media->{session()->get('lang').'_title'} }}</h2> <span class="date"><i class="fa fa-calendar"></i> {{ $news_and_media->start_date->toFormattedDateString() }}</span> </aside>
                        <aside class=" show_popup-js"> <span><i class="fa fa-video-camera" aria-hidden="true"></i></span>
                        </aside>
                    </div>

                    <!-- Start Poup-->
                    @if(!empty($news_and_media->video_link))
                        <div class="popup_services">
                            <div class="popup_services-content"> <span class="popup_services-close"><i class="fa fa-times" aria-hidden="true"></i> </span>
                                <div class="news_details-video">
                                    <iframe width="100%" src="https://www.youtube.com/embed/{{ $news_and_media->video_link }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                                </div>
                            </div>
                        </div>
                    @endif
                    <!-- End Poup-->
                </div>
                <div class="row">
                    <div class="col-sm-12 col-lg-8">
                        <p class="news_details-paragraph">{!! $news_and_media->{session()->get('lang').'_description'} !!}</p>
                    </div>
                    @if(count($news))
                        <div class="col-sm-12 col-lg-4">
                            <div class="related_articles">
                                <h4 class="related_articles-title">{{ trans('main.related_articles') }}</h4>
                                <ul>
                                    @foreach($news as $single)
                                        <li>) }}"
                                            <a href="{{ url(session()->get('lang').'/news/'.$single->id.'/'.str_replace(' ','-',$single->{session()->get('lang').'_title'})) }}" class="related_articles-linke">
                                                <h2>{{ $single->{session()->get('lang').'_title'} }}</h2> <span class="date"><i class="fa fa-calendar"></i> {{ $single->start_date->toFormattedDateString() }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    @endif
@stop