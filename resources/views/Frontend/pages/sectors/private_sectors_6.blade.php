@extends(FEL.'.master')

@section('content')
    @if(session()->get('lang') == 'ar')
        <section class="about-bg">
            <h1> مجموعة أنظمة قطاع التجميل والتفصيل</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector6.jpg') }}" alt="مجموعة أنظمة قطاع التجميل والتفصيل">
                            </div>
                            <h3> التجميل</h3>
                            <ul class="sector_block-list">
                                <li>قاعدة بيانات سرية للعملاء</li>
                                <li> تسجيل وحجز مواعيد للعملاء</li>
                                <li> امكانية اختيار موظفة تادية الخدمة</li>
                                <li> امكانية اختيار خدمة واحدة او اكثر من خدمة</li>
                                <li> امكانية منح العملاء عضويات تتمتع بنسب خصم مختلفة لكل عضوية</li>
                                <li>امكانية عمل عروض ترويجية للعملاء </li>
                                <li> امكانية عرض منتجات خاصة بالتجميل وبيعها للعملاء</li>
                                <li> تقارير مفصلة عن الخدمات المقدمة من كل موظفة خلال فترة</li>
                                <li> تقارير عن مبيعات الموظفات من المنتجات والخدمات المقدمة للعملاء</li>
                                <li> امكانية تقييم اداء الموظفات مع العملاء</li>
                                <li> امكانية احتساب عمولات للموظفات بناءا على خدماتهم المقدمة للعملاء ومدى كفاءتهم بتادية الخدمات</li>

                            </ul>
                            <h3> التفصيل</h3>
                            <ul class="sector_block-list">
                                <li>امكانية بيع ملابس مجهزة سابقاً للعملاء </li>
                                <li> امكانية الاتفاق مع العملاء على تنفيذ ملابس معينة بمقاسات معينة وتصاميم معينة والتسليم بمواعيد محددة</li>
                                <li> بيان بالموظفين وفقاً لتخصصاتهم وامكانية تقدير انجازات كل موظف خلال فترة</li>
                                <li> امكانية الحصول على دفعات مقدمة من العملاء بناءا على الاتفاقية معهم </li>
                                <li> امكانية استخراج كشف حساب للعملاء اجمالى وتفصيلى على كل اتفاقية</li>
                                <li>متابعة جدول مواعيد الموظفين مع العملاء </li>
                                <li> جدولة مواعيد البروفات للعملاء</li>
                                <li> حجز مواعيد للعملاء بالتنسيق مع الموظفين </li>
                                <li> بيان بالمنتجات الجاهزة للتسليم وامكانية ارسال رسائل قصيرة للعملاء على الجوالات بجاهزية طلباتهم للتسليم </li>
                                <li> امكانية عمل عضويات للعملاء</li>
                                <li> تقارير مفصلة عن الخدمات المقدمة من كل موظفة خلال فترة </li>
                                <li> امكانية احتساب عمولات للموظفات بناءا على خدماتهم المقدمة للعملاء ومدى كفاءتهم بتادية الخدمات</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">جميع القطاعات</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">مجموعة أنظمة قطاع المقاولات</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">مجموعة أنظمة قطاع الصيانة والتشغيل </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}"> مجموعة أنظمة قطاع الاستثمار العقارى وادارة الاملاك </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}"> مجموعة أنظمة قطاع الصيدليات ومستودعات الادوية </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}">مجموعة أنظمة قطاع تجارة التجزئة ونقاط البيع </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}"> مجموعة أنظمة قطاع المدارس والمعاهد التعليمية </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/8') }}"> مجموعة أنظمة قطاع التصنيع </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}"> مجموعة أنظمة قطاع تأجير السيارات </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}"> مجموعة أنظمة قطاع النقليات </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}"> مجموعة أنظمة قطاع مكافحة الحشرات </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}"> مجموعة أنظمة قطاع الاعاشة </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}"> مجموعة أنظمة قطاع الجمعيات الخيرية </a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @else
        <section class="about-bg">
            <h1>Beauty and Customization sector Systems Group</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector6.jpg') }}" alt="Beauty and Customization sector Systems Group">
                            </div>
                            <h3>Cosmetology</h3>
                            <ul class="sector_block-list">
                                <li>A confidential customer database.</li>
                                <li>Register and book appointments for customers.</li>
                                <li>The possibility of choosing an employee of performing the service.</li>
                                <li>The possibility of choosing one or more of service.</li>
                                <li>The possibility of granting customers membership with different discount rates for each membership.</li>
                                <li>Possibility of a promotional offers for customers.</li>
                                <li>The possibility of displaying beauty products and selling them to customers.</li>
                                <li>Detailed reports on the services provided by each employee during the period.</li>
                                <li>Reports on employee sales of products and services provided to customers.</li>
                                <li>The possibility of evaluating the performance of employees with customers.</li>
                                <li>The possibility of calculating the commissions for employees based on their services provided to customers and the extent of their competence to perform services.</li>
                            </ul>
                            <h3>Customization</h3>
                            <ul class="sector_block-list">
                                <li>The possibility of selling clothes already equipped for customers.</li>
                                <li>The possibility of agreeing with customers to implement certain clothes of certain sizes and designs and delivery of specific dates.</li>
                                <li>Statement of staff according to their specialization and the possibility of estimating the achievements of each employee during the period.</li>
                                <li>The possibility of obtaining advance payments from customers based on the agreement with them.</li>
                                <li>The possibility of extracting a statement of account for customers is a total and detailed on each agreement.</li>
                                <li>Follow up the schedule of employees with customers.</li>
                                <li>Scheduling rehearsal dates for customers.</li>
                                <li>Schedule appointments for clients in coordination with staff.</li>
                                <li>Statement of ready-made products for delivery and the possibility of sending SMS messages to customers on the mobile phones of their applications for delivery readiness.</li>
                                <li>The possibility of making memberships to the customers.</li>
                                <li>Detailed reports on the services provided by each employee during the period.</li>
                                <li>The possibility of calculating the commissions for employees based on their services provided to customers and the extent of their competence to perform services.</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">Private Sectors</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">Construction Sector Systems Group</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">Maintenance and Operating Systems sector group </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}">Group of Real Estate Investment Sector Systems and Property Management</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}">Pharmacies and Pharmaceutical Warehouses Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}">Retail and POS Systems Sector Group</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}">Schools and educational institutes sector Systems Group </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/8') }}">Manufacturing Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}">Group Of Car Rental Sector Systems</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}">Transport Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}">Pest Control Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}">Subsistence Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}">Charitable Sector Sector Regulations</a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @endif
    @include(FE.'.pages.sectors.sector_form')
@stop