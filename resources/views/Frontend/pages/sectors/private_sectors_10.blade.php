@extends(FEL.'.master')

@section('content')
    @if(session()->get('lang') == 'ar')
        <section class="about-bg">
            <h1> مجموعة أنظمة قطاع النقليات</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector10.jpg') }}" alt=" مجموعة أنظمة قطاع النقليات">
                            </div>
                            <ul class="sector_block-list">
                                <li> متابعة بيانات الشاحنات والسائقين وانواع الحمولات</li>
                                <li>متابعة جدول الاتجاهات والمسافات بين المدن واسعار الشحن </li>
                                <li> مصاريف الرحلات الاساسية وفقاً لجدول الاتجاهات والمسافات </li>
                                <li> ادخال طلبات شحن ومتابعة تحركات الشاحنات واماكن التوقف</li>
                                <li>متابعة الرحلات المغادرة والقادمة لكل فرع </li>
                                <li> الشحن من والى الاتجاهات المختلفة ومتابعة تاشيرات السائقين فى حالة الشحن الدولى</li>
                                <li>اصدار الفواتير وبوالص الشحن للعملاء وتسجيل الاستحقاقات </li>
                                <li> مركز ربحية لكل شاحنة ولكل سائق </li>
                                <li> متابعة حالات الشاحنات ( فى الخدمة / متوقفة / معطلة / بالورشة/ فى رحلة /فى انتظار وصولها لفرع)</li>
                                <li>تقارير عن اماكن تواجد الشاحنات وجدول التحركات </li>
                                <li> تقارير عن ايرادات ومصروفات كل شاحنة</li>
                                <li>تقارير عن موقف الشاحنات والسائقين (الصيانة الدورية / انتهاء المستندات / انواع الحمولات / المخالفات / الرخص / التاشيرات) </li>
                                <li> تقارير عن ايرادات ومصروفات كل رحلة</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">جميع القطاعات</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">مجموعة أنظمة قطاع المقاولات</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">مجموعة أنظمة قطاع الصيانة والتشغيل </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}"> مجموعة أنظمة قطاع الاستثمار العقارى وادارة الاملاك </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}"> مجموعة أنظمة قطاع الصيدليات ومستودعات الادوية </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}">مجموعة أنظمة قطاع تجارة التجزئة ونقاط البيع </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/6') }}">مجموعة أنظمة قطاع التجميل والتفصيل</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}"> مجموعة أنظمة قطاع المدارس والمعاهد التعليمية </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/8') }}">مجموعة أنظمة قطاع التصنيع </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}"> مجموعة أنظمة قطاع تأجير السيارات </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}"> مجموعة أنظمة قطاع مكافحة الحشرات </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}"> مجموعة أنظمة قطاع الاعاشة </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}"> مجموعة أنظمة قطاع الجمعيات الخيرية </a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @else
        <section class="about-bg">
            <h1>Transport Sector Systems Group</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector10.jpg') }}" alt="Transport Sector Systems Group">
                            </div>
                            <ul class="sector_block-list">
                                <li>Follow-up with the trucks, drivers and load types.</li>
                                <li>Follow-up of the table of directions and distances between cities and freight prices.</li>
                                <li>Basic travel expenses according to the schedule of directions and distances.</li>
                                <li>Enter shipping orders and follow up with truck movements and stopping places.</li>
                                <li>Follow up with the departing and arriving flights for each branch.</li>
                                <li>Shipping to and from different directions and following-up with the drivers about their visas in case of international shipping.</li>
                                <li>Issuing invoices and bills of lading to customers and record benefits.</li>
                                <li>Profit center for each truck and per driver.</li>
                                <li>Follow-up cases of trucks (in service / parked / Off / workshop / on a trip / waiting for the arrival of the branch).</li>
                                <li>Reports on the whereabouts of trucks and schedule moves.</li>
                                <li>Reports on revenue and expenses of each truck.</li>
                                <li>Reports on trucks and drivers position (periodic maintenance / documents / types of cargo / irregularities / licenses / visas).</li>
                                <li>Reports on revenue and expenses of each trip.</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">Private Sectors</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">Construction Sector Systems Group</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">Maintenance and Operating Systems sector group </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}">Group of Real Estate Investment Sector Systems and Property Management</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}">Pharmacies and Pharmaceutical Warehouses Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}">Retail and POS Systems Sector Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/6') }}">Beauty and Customization sector Systems Group</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}">Schools and educational institutes sector Systems Group </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/8') }}">Manufacturing Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}">Group Of Car Rental Sector Systems</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}">Pest Control Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}">Subsistence Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}">Charitable Sector Sector Regulations</a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @endif
    @include(FE.'.pages.sectors.sector_form')
@stop