@extends(FEL.'.master')

@section('content')
    @if(session()->get('lang') == 'ar')
        <section class="about-bg">
            <h1> مجموعة أنظمة قطاع المقاولات</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector1.jpg') }}" alt="مجموعة أنظمة قطاع المقاولات">
                            </div>
                            <h3>ادارة المناقصات والمشاريع</h3>

                            <ul class="sector_block-list">
                                <li>سجل بيانات تاريخية للمناقصات وبنودها </li>
                                <li> بيانات عناصر التكلفة لبنود المناقصات ومن ثم تحديد السعر الذى سيتم تقديمه بالمناقصة</li>
                                <li>قياس ربحية عروض المناقصات قبل تقديمها لاتخاذ قرار دخول المناقصة من عدمه </li>
                                <li>امكانية تحويل المناقصة بعد الترسية الى مشروع واحد او عدة مشاريع </li>
                                <li> بيانات عقود المشاريع </li>
                                <li> اثبات الدفعات المقدمة</li>
                                <li>موازنات تقديرية للمشاريع </li>
                                <li> إسناد بنود الأعمال لمقاول باطن او عدة مقاولين</li>
                                <li>امكانية تحويل بنود الاعمال من مقاول باطن لاخر </li>
                                <li>مقارنة عناصر التكلفة الفعلية بالموازنة وبيان الانحرافات على مستوى بنود الأعمال والاجمالى </li>
                                <li> مؤشرات الأداء وفقاً لعناصر التكلفة ( العمالة – المواد – المعدات – مقاولي الباطن – التكاليف الأخرى )</li>
                                <li> رفع المستخلصات للجهات الاستشارية ثم الجهات المالكة بعد اعتمادها </li>
                                <li> متابعة دورة تحصيل المستخلصات بالجهات الحكومية</li>
                                <li> تحصيل القيمة الصافية للمستخلصات بعد استقطاع الغرامات ونسب الدفعة المقدمة</li>
                                <li>بيان منفصل بموقف كل من (الدفعة المقدمة / المبالغ المحتجزة / الغرامات وانواعها/المستخلص ) لكل مشروع ولكل المشاريع </li>
                                <li>متابعة مستخلصات مقاولى الباطن </li>
                                <li>قياس معدلات الاداء من الناحية المالية لكل مشروع </li>
                                <li>متابعة التغيرات فى قيمة ومدة العقود </li>
                                <li> متابعة فترات التوقف لكل مشروع او لكل المشاريع</li>
                                <li> تقارير تحليلية لمصروفات وايرادات كل مشروع </li>
                            </ul>
                            <h3>ادارة الحركة</h3>

                            <ul class="sector_block-list">
                                <li> موازنة تقديرية لاحتياجات كل مشروع من المعدات والسيارات خلال مدة تنفيذه </li>
                                <li> احصائيات تفصيلية لادارة الحركة عن المعدات المتوفرة لديها وحالتها واماكن تواجدها</li>
                                <li>ادارة ورش الصيانة ومتابعة دخول المعدات والسيارات للورش وخروجها عن طريق كروت التشغيل . </li>
                                <li>كل مشروع يقوم بارسال طلب توفير معدة يتم معالجته من خلال ادارة الحركة (نقل معدة من مشروع اخر – المعدة متوفرة لدى ادارة الحركة – استئجار المعدة من الغير – شراء المعدة ) </li>
                                <li> تحميل المشاريع بقيمة ايجارية للمعدات </li>
                                <li> تكلفة تفصيلية لكل معدة ورقابة على قطع الغيار والمحروقات </li>
                                <li> ادارة النقليات بين المشاريع والمستودعات عن طريق اسطول الشاحنات بادارة الحركة</li>
                                <li> متابعة بوالص التامين للمعدات والسيارات بكل تفاصيلها</li>
                                <li>متابعة الحوادث ونسب التحمل ومطالبات التامين </li>
                                <li>متابعة تنقلات المعدات والاصول بين المشاريع </li>
                                <li>امكانية الربط مع انظمة التتبع </li>
                                <li> متابعة اهلاكات الاصول الثابتة والاضافات والاستبعادات</li>
                            </ul>
                            <h3>العمالة والمواد</h3>
                            <ul class="sector_block-list">
                                <li> متابعة العمالة وتواجدها بالمشاريع</li>
                                <li>متابعة تنقلات العمالة وتحميل كل مشروع بنصيبه من تكلفة العمالة </li>
                                <li> اجور / خارج دوام / تصفيات / مكافآت / استقطاعات / اجازات / سلف / غيابات ..... يتم مراقبتهم على مستوى العامل والمشروع والشركة </li>
                                <li> امكانية الربط مع ماكينات الحضور والانصراف مباشرة </li>
                                <li> تقارير بالانحرافات لاستهلاك العمالة الفعلى عن التقديرى على مستوى كل مشروع </li>
                                <li>دورة كاملة للمشتريات بدءاً من طلب المواد ثم طلب الشراء ثم عروض الاسعار ثم امر الشراء وصولاً لاستلام المواد بالمستودعات او المواقع ثم اصدار فاتورة الشراء </li>
                                <li>متابعة المشتريات لكل مشروع وربطها مع الموازنة التقديرية للمواد بالمشروع </li>
                                <li> تقارير بالانحرافات لاستهلاك المواد الفعلى عن التقديرى على مستوى كل مشروع بل وكل بند من بنود اعمال المشروع</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">Private Sectors</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}"> مجموعة أنظمة قطاع الصيانة والتشغيل </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}"> مجموعة أنظمة قطاع الاستثمار العقارى وادارة الاملاك </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}"> مجموعة أنظمة قطاع الصيدليات ومستودعات الادوية </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}"> مجموعة أنظمة قطاع تجارة التجزئة ونقاط البيع </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/6') }}"> مجموعة أنظمة قطاع التجميل والتفصيل </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}"> مجموعة أنظمة قطاع المدارس والمعاهد التعليمية </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/8') }}"> مجموعة أنظمة قطاع التصنيع </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}"> مجموعة أنظمة قطاع تأجير السيارات </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}"> مجموعة أنظمة قطاع النقليات </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}"> مجموعة أنظمة قطاع مكافحة الحشرات </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}"> مجموعة أنظمة قطاع الاعاشة </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}"> مجموعة أنظمة قطاع الجمعيات الخيرية </a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @else
        <section class="about-bg">
            <h1>Construction Sector Systems Group</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector1.jpg') }}" alt="Construction Sector Systems Group">
                            </div>
                            <h3>Tenders and Project Management</h3>
                            <ul class="sector_block-list">
                                <li>Historical data for tenders and its items record.</li>
                                <li>The cost elements of the tender items and then the price to be presented in the tender.</li>
                                <li>Measure the profitability of tenders before submitting them to make a decision whether or not to enter the tender.</li>
                                <li>The possibility of converting the tender after the award to one project or several projects.</li>
                                <li>Project contract data.</li>
                                <li>Proof of advance payments.</li>
                                <li>Discretionary budgets for projects.</li>
                                <li>Assigning the work items to a subcontractor or several contractors.</li>
                                <li>The possibility of converting business items from one subcontractor to another.</li>
                                <li>Comparison of the actual cost elements of the budget and the indication of the deviations at the level of the items of work and the total.</li>
                                <li>Performance indicators according to cost elements (labor - materials - equipment - subcontractors - other costs).</li>
                                <li>Raise the abstracts in order to get advices from specialized authorities and then the owners after their adoption.</li>
                                <li>Follow up the collection of abstracts in governmental bodies.</li>
                                <li>The collection of the net value of the extracts after deducting fines and payment rates.</li>
                                <li>Separate statement of the status of each of the (advance payment / retention / fines and types / abstracts) for each project and for all projects.</li>
                                <li>Follow-up sub-contractors extracts.</li>
                                <li>Measuring performance rates of financial terms for each project.</li>
                                <li>Follow-up changes in the value and duration of contracts.</li>
                                <li>Follow-up periods for each project or for all projects.</li>
                                <li>Analytical reports of expenses and revenues of each project.</li>
                            </ul>
                            <h3>Traffic Management</h3>

                            <ul class="sector_block-list">
                                <li>Estimated budget for the needs of each project of equipment and vehicles during the period of implementation.</li>
                                <li>Detailed traffic management statistics on available equipment, condition and location.</li>
                                <li>Management of maintenance workshops and follow-up entry of equipment and vehicles for workshops and exit through the operating cards.</li>
                                <li>Each project sends a request for the provision of equipment to be processed through the management of the movement (transfer from another project - the availability in the traffic management - the renting from others - the purchases).</li>
                                <li>Load projects with rental value of equipment.</li>
                                <li>Detailed cost for each equipment and control of spare parts and fuel.</li>
                                <li>Management of transportation between projects and warehouses by truck fleet management movement.</li>
                                <li>Follow-up insurance policies for equipment and cars in all its details.</li>
                                <li>Accident follow-up, tolerance ratios and insurance claims.</li>
                                <li>Follow-up to the movement of equipment and assets between projects.</li>
                                <li>The possibility of linking with tracking systems.</li>
                                <li>Follow-up depreciation of fixed assets, additions and exclusions.</li>
                            </ul>
                            <h3>Employment and Materials</h3>
                            <ul class="sector_block-list">
                                <li>Follow-up with the labor and the employers settlement projects.</li>
                                <li>Follow-up to the movement of labor and loading each project share of the cost of labor.</li>
                                <li>Wages / out time / qualifying / bonuses / deductions / vacations / advances / absences ..... are monitored on the level of the working project and the company.</li>
                                <li>The possibility of connecting with machines of attendance and departure directly.</li>
                                <li>Reports of deviations of actual labor consumption are estimated at the level of each project.</li>
                                <li>A complete cycle of procurement for projects ranging from application materials and then purchase order and then quotations and purchase order to receive access materials or warehousing sites and then issuing the purchase invoice.</li>
                                <li>Follow-up procurement for each project and linking it with the estimated budget of the project materials.</li>
                                <li>Reports of deviations for the consumption of the actual material for the estimated at the level of each project, but the project and each item of business items.</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">Private Sectors</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">Maintenance and Operating Systems sector group </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}">Group of Real Estate Investment Sector Systems and Property Management</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}">Pharmacies and Pharmaceutical Warehouses Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}">Retail and POS Systems Sector Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/6') }}">Beauty and Customization sector Systems Group</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}">Schools and educational institutes sector Systems Group </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/8') }}">Manufacturing Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}">Group Of Car Rental Sector Systems</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}">Transport Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}">Pest Control Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}">Subsistence Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}">Charitable Sector Sector Regulations</a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @endif
    @include(FE.'.pages.sectors.sector_form')
@stop