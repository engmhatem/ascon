@extends(FEL.'.master')

@section('content')
    @if(session()->get('lang') == 'ar')
        <section class="about-bg">
            <h1> مجموعة أنظمة قطاع الاعاشة</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector12.jpg') }}" alt=" مجموعة أنظمة قطاع الاعاشة">
                            </div>
                            <ul class="sector_block-list">
                                <li>امكانية تعريف انواع الخدمات التى يتم تقديمها للعملاء </li>
                                <li> سهولة التعاقد مع العملاء على تقديم الخدمات من خلال النظام</li>
                                <li> مرونة تحديد طريقة وفترات السداد لقيمة العقد(مقدم/مؤخر—شهرى/ربع سنوى/نصف سنوى/سنوى)</li>
                                <li> سهولة استخراج فواتير العملاء الدورية آلياً من النظام</li>
                                <li>سهولة التعديل على العقود مع الاحتفاظ بالبيانات التاريخية السابقة </li>
                                <li>متابعة المندوبين ومبيعاتهم وتحصيلاتهم من العملاء </li>
                                <li>مرونة فى تحديد سياسات العمولات للمندوبين </li>
                                <li> تحديد جدول الوجبات الاسبوعى واليومى الذى يتم توريده للعميل </li>
                                <li>تحديد المواقع التى يتم تقديم الخدمات للعميل فيها </li>
                                <li> امكانية تعريف مكونات كل وجبة من المواد الخام لمتابعة المشتريات والصرف من المستودعات لتجهيز الوجبات</li>
                                <li>امكانية متابعة الانحراف بين الفعلى والتقديرى فيما يتم صرفه من مواد خام لاعداد الوجبات </li>
                                <li> تحديد الكميات المطلوبة للشراء بناءاً على الكميات المطلوبة بالعقود ومقارنتها مع المخزون الحالى </li>
                                <li>الربط الكامل مع نظام ضريبة القيمة المضافة </li>
                                <li> الربط الكامل مع نظام الحسابات العامة والعملاء وترحيل القيود آلياً مع ضبط المقدم والمستحق من الايرادات</li>
                                <li> الربط الكامل مع نظامى المشتريات والمستودعات</li>
                                <li> تقارير تفصيلية عن ايرادات العقود </li>
                                <li>تقارير تفصيلية عن المبيعات للعملاء والمندوبين </li>
                                <li> تقارير عن المتحصلات والمتاخرات من العقود </li>
                                <li> تقرير مجمع للفواتير الشهرية </li>
                                <li> تقارير عن مبيعات المندوبين وتحصيلاتهم ونسبة تحقيقهم للمستهدف والعمولة المستحقة</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">جميع القطاعات</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">مجموعة أنظمة قطاع المقاولات</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">مجموعة أنظمة قطاع الصيانة والتشغيل </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}"> مجموعة أنظمة قطاع الاستثمار العقارى وادارة الاملاك </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}"> مجموعة أنظمة قطاع الصيدليات ومستودعات الادوية </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}">مجموعة أنظمة قطاع تجارة التجزئة ونقاط البيع </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/6') }}">مجموعة أنظمة قطاع التجميل والتفصيل</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}"> مجموعة أنظمة قطاع المدارس والمعاهد التعليمية </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/8') }}">مجموعة أنظمة قطاع التصنيع </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}"> مجموعة أنظمة قطاع تأجير السيارات </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/10') }}"> مجموعة أنظمة قطاع النقليات </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/11') }}"> مجموعة أنظمة قطاع مكافحة الحشرات </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}"> مجموعة أنظمة قطاع الجمعيات الخيرية </a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @else
        <section class="about-bg">
            <h1>Subsistence Sector Systems Group</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector12.jpg') }}" alt="Subsistence Sector Systems Group">
                            </div>
                            <ul class="sector_block-list">
                                <li>The possibility of defining the services offered to the customers.</li>
                                <li>The ease of contracting with customers to provide the best services through the system.*</li>
                                <li>Eligibility for determining the methods and periods of payment for the contract value (front / back / monthly / quarterly / half yearly / yearly).</li>
                                <li>Easy to automatically extract customer invoices automatically from the system.</li>
                                <li>Easy to modify contracts while retaining historical data.</li>
                                <li>Follow-up of delegates, sales and collections of customers.</li>
                                <li>Flexibility in determining commission policies for delegates.</li>
                                <li>Determining the weekly and daily meal schedule to be provided to the customer.</li>
                                <li>Identify the locations where the services are provided to the customer.</li>
                                <li>The possibility of defining the ingredients of each meal of raw materials to follow up purchases and drainage from the warehouses for processing meals.</li>
                                <li>The possibility of defining the ingredients of each meal of raw materials to follow up purchases and drainage from the warehouses for processing meals.</li>
                                <li>The possibility of following the deviation between the actual and the estimate of the amount spent from raw materials for the preparation of meals.</li>
                                <li>Determining quantities required to purchase based on quantities required by contracts and comparing them with current inventory.</li>
                                <li>Full connectivity with the VAT system.</li>
                                <li>Full connectivity with the system of general accounts and customers and automatically transfer entries with the adjustment of the provider and the receivable from the revenue.</li>
                                <li>Full connectivity with both procurement and warehouse systems.</li>
                                <li>Detailed reports on contract revenues.</li>
                                <li>Detailed sales reports for clients and delegates.</li>
                                <li>Reports on receipts and savings from contracts.</li>
                                <li>Compiled monthly invoicing report.</li>
                                <li>Sales reports from the sales representatives, reporting what they have collected and the goal achievement percentage and the commission required.</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">Private Sectors</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">Construction Sector Systems Group</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">Maintenance and Operating Systems sector group </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}">Group of Real Estate Investment Sector Systems and Property Management</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}">Pharmacies and Pharmaceutical Warehouses Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}">Retail and POS Systems Sector Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/6') }}">Beauty and Customization sector Systems Group</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}">Schools and educational institutes sector Systems Group </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/8') }}">Manufacturing Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}">Group Of Car Rental Sector Systems</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}">Transport Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}">Pest Control Systems Group</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}">Charitable Sector Sector Regulations</a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @endif
    @include(FE.'.pages.sectors.sector_form')
@stop