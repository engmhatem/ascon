@extends(FEL.'.master')

@section('content')
    @if(session()->get('lang') == 'ar')
        <section class="about-bg">
            <h1> مجموعة أنظمة قطاع الجمعيات الخيرية</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector13.jpg') }}" alt=" مجموعة أنظمة قطاع الاعاشة">
                            </div>
                            <ul class="sector_block-list">
                                <li> سهولة الهيكلة الادارية لكافة الفروع والادارات </li>
                                <li>ربط الهياكل الادارية مع مراكز التكلفة المالية </li>
                                <li> امكانية تسجيل المجالات الخيرية المختلفة التى يتم العمل بها (الصحى-الاجتماعى – المساجد – الدعوى- التعليمى - الاعلامى)</li>
                                <li>امكانية تسجيل المنح والمبادرات </li>
                                <li>امكانية عمل خطة استراتيجية للمؤسسة الخيرية وربط انواع مصاريفها وفروعها المنح والمبادرات بالخطة مع امكانية تحديد قيمة كل نوع مصروف على مستوى الفرع والمبادرة </li>
                                <li> امكانية تسجيل المشاريع المختلفة مع امكانية تحميل كافة مستندات المشروع على النظام</li>
                                <li> امكانية المقارنة بين المصروفات المقدرة وفقاً للخطة الاستراتيجية والمصروفات الفعلية مع تحديد الانحرافات</li>
                                <li> اثبات التحويل بنظام المنح فعلياً عند استلام الجهات الخيرية للمبالغ المحولة </li>
                                <li>امكانية تفعيل طلبات الصرف وحركات الصرف الفعلى وربطها مع النظام المالى للمؤسسة</li>
                                <li>امكانية ارسال رسائل قصيرة او ايميلات من النظام الى الجهات الخيرية الممنوحة عند الايداع واستلام مستندات المشروع </li>
                                <li>امكانية ربط نظام المشتريات مع نظام المنح والتخطيط لتوفير كافة متطلبات المشاريع الخيرية</li>

                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">جميع القطاعات</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">مجموعة أنظمة قطاع المقاولات</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">مجموعة أنظمة قطاع الصيانة والتشغيل </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}"> مجموعة أنظمة قطاع الاستثمار العقارى وادارة الاملاك </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}"> مجموعة أنظمة قطاع الصيدليات ومستودعات الادوية </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}">مجموعة أنظمة قطاع تجارة التجزئة ونقاط البيع </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/6') }}">مجموعة أنظمة قطاع التجميل والتفصيل</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}"> مجموعة أنظمة قطاع المدارس والمعاهد التعليمية </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/8') }}">مجموعة أنظمة قطاع التصنيع </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}"> مجموعة أنظمة قطاع تأجير السيارات </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/10') }}"> مجموعة أنظمة قطاع النقليات </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/11') }}"> مجموعة أنظمة قطاع مكافحة الحشرات </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/12') }}">مجموعة أنظمة قطاع الاعاشة</a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @else
        <section class="about-bg">
            <h1>Charitable Sector Sector Regulations</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector13.jpg') }}" alt="Charitable Sector Sector Regulations">
                            </div>
                            <ul class="sector_block-list">
                                <li>Ease of administrative structure in all branches and departments.</li>
                                <li>Linking administrative structures with financial cost centers.</li>
                                <li>Possibility of registration of the various charitable fields that are working (health - social - mosques - lawsuit - educational - media).</li>
                                <li>The possibility of registering grants and initiatives.</li>
                                <li>Forming a strategic plan for charity organizations and linking the types of expenses and grant branches and initiatives of the plan with the possibility of determining the value of each type of expense at the level of the branch and initiative.</li>
                                <li>The possibility of registering various projects with the possibility of loading all the project documents on the system.</li>
                                <li>The possibility of comparing between the estimated expenditures according to the strategic plan and the actual expenses with the identification of deviations.</li>
                                <li>Proof of transfer by the grant system effectively when the charity receives the transferred amounts.</li>
                                <li>The possibility of activating exchange orders and actual exchange movements and linking them with the financial system of the institution.</li>
                                <li>The possibility of sending SMS messages or emails from the system to the charity granted at the filing and receipt of project documents.</li>
                                <li>The possibility of linking the procurement system with the grant system and planning to provide all the requirements of charitable projects.</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">Private Sectors</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">Construction Sector Systems Group</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">Maintenance and Operating Systems sector group </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}">Group of Real Estate Investment Sector Systems and Property Management</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}">Pharmacies and Pharmaceutical Warehouses Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}">Retail and POS Systems Sector Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/6') }}">Beauty and Customization sector Systems Group</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}">Schools and educational institutes sector Systems Group </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/8') }}">Manufacturing Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}">Group Of Car Rental Sector Systems</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}">Transport Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}">Pest Control Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}">Subsistence Sector Systems Group</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @endif
    @include(FE.'.pages.sectors.sector_form')
@stop