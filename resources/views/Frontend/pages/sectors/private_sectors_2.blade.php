@extends(FEL.'.master')

@section('content')
    @if(session()->get('lang') == 'ar')
        <section class="about-bg">
            <h1>مجموعة أنظمة قطاع الصيانة والتشغيل</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector2.jpg') }}" alt="مجموعة أنظمة قطاع الصيانة والتشغيل">
                            </div>
                            <h3> ادارة المناقصات والمشاريع</h3>
                            <ul class="sector_block-list">
                                <li>سجل بيانات تاريخية للمناقصات وبنودها</li>
                                <li> بيانات عناصر التكلفة لبنود المناقصات ومن ثم تحديد السعر الذى سيتم تقديمه بالمناقصة </li>
                                <li> قياس ربحية عروض المناقصات قبل تقديمها لاتخاذ قرار دخول المناقصة من عدمه</li>
                                <li>امكانية تحويل المناقصة بعد الترسية الى مشروع واحد او عدة مشاريع </li>
                                <li> بيانات عقود المشاريع</li>
                                <li> اثبات الدفعات المقدمة</li>
                                <li> موازنات تقديرية للمشاريع</li>
                                <li> رفع المستخلصات الدورية للجهات المالكة بعد اعتمادها </li>
                                <li> متابعة دورة تحصيل المستخلصات بالجهات الحكومية</li>
                                <li> تحصيل القيمة الصافية للمستخلصات بعد استقطاع الغرامات ونسب الدفعة المقدمة </li>
                                <li>بيان منفصل بموقف كل من (الدفعة المقدمة / المبالغ المحتجزة / الغرامات وانواعها/المستخلص ) لكل مشروع ولكل المشاريع </li>
                                <li> قياس معدلات الاداء من الناحية المالية لكل مشروع </li>
                                <li> متابعة التغيرات فى قيمة ومدة العقود</li>
                                <li> متابعة فترات التوقف لكل مشروع او لكل المشاريع</li>
                                <li> تقارير تحليلية لمصروفات وايرادات كل مشروع</li>
                            </ul>
                            <h3> العمالة والمواد</h3>
                            <ul class="sector_block-list">
                                <li> متابعة العمالة وتسكينها بالمشاريع</li>
                                <li>متابعة تنقلات العمالة وتحميل كل مشروع بنصيبه من تكلفة العمالة </li>
                                <li> اجور / خارج دوام / تصفيات / مكافآت / استقطاعات / اجازات / سلف / غيابات ..... يتم مراقبتهم على مستوى العامل والمشروع والشركة</li>
                                <li>امكانية الربط مع ماكينات الحضور والانصراف مباشرة </li>
                                <li>تقارير بالانحرافات لاستهلاك العمالة الفعلى عن التقديرى على مستوى كل مشروع </li>
                                <li>دورة كاملة للمشتريات الخاصة بالمشاريع بدءاً من طلب المواد ثم طلب الشراء ثم عروض الاسعار ثم امر الشراء وصولاً لاستلام المواد بالمستودعات او المواقع ثم اصدار فاتورة الشراء </li>
                                <li> متابعة المشتريات لكل مشروع وربطها مع الموازنة التقديرية للمواد بالمشروع </li>
                                <li>متابعة المواد التى يتم صرفها لكل مشروع </li>
                                <li> تقارير بالانحرافات لاستهلاك المواد الفعلى عن التقديرى على مستوى كل مشروع</li>
                            </ul>


                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">جميع القطاعات</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">مجموعة أنظمة قطاع المقاولات</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}"> مجموعة أنظمة قطاع الاستثمار العقارى وادارة الاملاك </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}"> مجموعة أنظمة قطاع الصيدليات ومستودعات الادوية </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}"> مجموعة أنظمة قطاع تجارة التجزئة ونقاط البيع </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/6') }}"> مجموعة أنظمة قطاع التجميل والتفصيل </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}"> مجموعة أنظمة قطاع المدارس والمعاهد التعليمية </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/8') }}"> مجموعة أنظمة قطاع التصنيع </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}"> مجموعة أنظمة قطاع تأجير السيارات </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}"> مجموعة أنظمة قطاع النقليات </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}"> مجموعة أنظمة قطاع مكافحة الحشرات </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}"> مجموعة أنظمة قطاع الاعاشة </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}"> مجموعة أنظمة قطاع الجمعيات الخيرية </a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @else
        <section class="about-bg">
            <h1>Maintenance and Operating Systems sector group</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector2.jpg') }}" alt="Maintenance and Operating Systems sector group">
                            </div>
                            <h3>Tenders and Project Management</h3>
                            <ul class="sector_block-list">
                                <li>Historical data for tenders and its items record.</li>
                                <li>Cost elements of the terms of bidding data and then determine the price, which will be presented in the tender.</li>
                                <li>Measure the profitability of tenders before submitting them to make a decision whether or not to enter the tender.</li>
                                <li>The possibility of converting the tender after the award to one project or several projects</li>
                                <li>Project contracts data.</li>
                                <li>Proof of advance payments.</li>
                                <li>Discretionary budgets for projects.</li>
                                <li>Raising the periodic extracts of the owners after their adoption.</li>
                                <li>Follow up the collection of abstracts in governmental bodies.</li>
                                <li>The collection of the net value of the extracts after deducting fines and payment rates.</li>
                                <li>A separate statement, the position of each of the (payment / amounts withheld / fines and types / Abstract) for each project and each project.</li>
                                <li>Measuring performance rates of financial terms for each project.</li>
                                <li>Follow up changes in the value and duration of contracts.</li>
                                <li>Follow-up periods for each project or for all projects.</li>
                                <li>Analytical reports of expenses and revenues of each project.</li>
                            </ul>
                            <h3>Employment and materials</h3>
                            <ul class="sector_block-list">
                                <li>Follow-up with the labor and the employers settlement projects.</li>
                                <li>Follow-up to the movement of labor and loading each project share of the cost of labor.</li>
                                <li>Wages / out time / qualifying / bonuses / deductions / vacations / advances / absences ..... are monitored on the level of the working project and the company.</li>
                                <li>The possibility of connecting with machines of attendance and departure directly.</li>
                                <li>Reports of deviations of actual labor consumption are estimated at the level of each project.</li>
                                <li>A complete cycle of procurement for projects ranging from application materials and then purchase order and then quotations and purchase order to receive access materials or warehousing sites and then issuing the purchase invoice.</li>
                                <li>Follow-up purchases for each project and linked with the estimated budget of the project materials.</li>
                                <li>Follow-up materials that are drained for each project.</li>
                            </ul>


                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">Private Sectors</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">Construction Sector Systems Group</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}">Group of Real Estate Investment Sector Systems and Property Management</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}">Pharmacies and Pharmaceutical Warehouses Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}">Retail and POS Systems Sector Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/6') }}">Beauty and Customization sector Systems Group</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}">Schools and educational institutes sector Systems Group </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/8') }}">Manufacturing Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}">Group Of Car Rental Sector Systems</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}">Transport Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}">Pest Control Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}">Subsistence Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}">Charitable Sector Sector Regulations</a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @endif
    @include(FE.'.pages.sectors.sector_form')
@stop