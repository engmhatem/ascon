@extends(FEL.'.master')

@section('content')

    <section class="about-bg">
        <h1>{{ trans('main.private_sectors') }}</h1>
    </section>

    @if(session()->get('lang') == 'ar')
        <section class="news">
            <div class="container">
                <div class="row">
                    <!--Start Block=1= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector1.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name"> مجموعة أنظمة قطاع المقاولات</h2>
                                <ul class="sector_block-list">
                                    <li> سجل بيانات تاريخية للمناقصات وبنودها </li>
                                    <li> بيانات عناصر التكلفة لبنود المناقصات ومن ثم تحديد السعر الذى سيتم تقديمه بالمناقصة</li>
                                    <li> قياس ربحية عروض المناقصات قبل تقديمها لاتخاذ قرار دخول المناقصة من عدمه </li>
                                    <li> امكانية تحويل المناقصة بعد الترسية الى مشروع واحد او عدة مشاريع </li>
                                    <li> بيانات عقود المشاريع</li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/1') }}" class="button">قراءة المزيد</a>
                            </div>
                        </div>
                    </div>

                    <!--End Block=1= -->

                    <!--Start Block=2= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector2.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name"> مجموعة أنظمة قطاع الصيانة والتشغيل</h2>

                                <ul class="sector_block-list">
                                    <li> امكانية تحويل المناقصة بعد الترسية الى مشروع واحد او عدة مشاريع </li>
                                    <li> امكانية تحويل المناقصة بعد الترسية الى مشروع واحد او عدة مشاريع </li>
                                    <li> موازنات تقديرية للمشاريع </li>
                                    <li> امكانية تحويل المناقصة بعد الترسية الى مشروع واحد او عدة مشاريع* رفع المستخلصات الدورية للجهات المالكة بعد اعتمادها </li>
                                    {{--<li> تقارير تحليلية لمصروفات وايرادات كل مشروع </li>--}}
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/2') }}" class="button">قراءة المزيد</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=2= -->



                    <!--Start Block=3= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector3.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">مجموعة أنظمة قطاع الاستثمار العقارى وادارة الاملاك</h2>
                                <ul class="sector_block-list">
                                    <li> تسجيل بيانات الاراضى </li>
                                    <li> انشاء مساهمات جديدة على الاراضى</li>
                                    <li> طرح المساهمات للاكتتاب </li>
                                    <li> متابعة عروض البيع والشراء وتداول الاسهم </li>
                                    <li> تقارير تحليلية لمصروفات وايرادات كل مشروع تقسيم وبيع اراضى المساهمات </li>
                                    <li> تقارير تحليلية لمصروفات وايرادات كل مشروع تقسيم وبيع اراضى المساهمات* اعداد شجرة العقارات والمو </li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/3') }}" class="button">قراءة المزيد</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=3= -->



                    <!--Start Block=4= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector4.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">مجموعة أنظمة قطاع الصيدليات ومستودعات الادوية</h2>
                                <ul class="sector_block-list">
                                    <li> مرونة كاملة فى تصنيف مجموعات الاصناف بشكل شجرى </li>
                                    <li> تعريف الاصناف وفقاً لمجموعاتها </li>
                                    <li> وحدات قياس متعددة لكل صنف وامكانية التعامل داخل النظام باى منها </li>
                                    <li> امكانية ادراج الاصناف البديلة للصنف وارفاق صورة واقعية له </li>
                                    <li> تقارير تحليلية لمصروفات وايرادات* تحديد حد الطلب والحد الادنى والاقصى لكل صنف على مستوى الصنف او المستودع او نقطة البيع </li>
                                    <li> تقارير تحليلية لمصروفات وايرادات كل مشروع تقسيم وبيع اراضى المساهمات اعداد شجرة العقارات والمواقع والمناطق سهولة التعامل مع الشحنات المختلفة لنفس الصنف وتواريخ الصلاحية المختلفة ايضا لنفس الصنف </li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/4') }}" class="button">قراءة المزيد</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=4= -->



                    <!--Start Block=5= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector5.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">مجموعة أنظمة قطاع تجارة التجزئة ونقاط البيع </h2>
                                <ul class="sector_block-list">
                                    <li>عطور - ساعات – ملابس – مواد غذائية – مطاعم – مواد بناء</li>
                                    <li> سياسات تسعير للاصناف وخصومات وعروض ترويجية </li>
                                    <li> نظام الولاء للعملاء واحتساب النقاط والمكافآت وطباعة بطاقات للعملاء </li>
                                    <li> إمكانية تعليق الفواتير </li>
                                    <li> اوقات دوام خاصة لكل نقطة بيع </li>
                                    <li> تقارير تحليلية لمصروفات وايرادات* تحديد حد الطلب والحد الادنى والاقصى لكل صنف على مستوى الصنف او سهولة السداد وفقاً لطرق السداد المختلفة لدى العملاء </li>
                                    <li> تقارير تحليلية لمصروفات وايرادات كل مشروع تقسيم وبيع اراضى المساهمات اعداد شجرة العقارات والمواقع والمناطق سهولة التعامل مع الشحنات المختلفة لنفس الصنف وتواريخ الصلاحية المختلفة ايضا لنفس الصنف </li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/5') }}" class="button">قراءة المزيد</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=5= -->

                    <!--Start Block=6= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector6.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">مجموعة أنظمة قطاع التجميل والتفصيل </h2>
                                <ul class="sector_block-list">
                                    <li> امكانية عرض منتجات خاصة بالتجميل وبيعها للعملاء </li>
                                    <li> تقارير مفصلة عن الخدمات المقدمة من كل موظفة خلال فترة </li>
                                    <li> تقارير عن مبيعات الموظفات من المنتجات والخدمات المقدمة للعملاء </li>
                                    <li> امكانية تقييم اداء الموظفات مع العملاء </li>
                                    <li> امكانية احتساب عمولات للموظفات بناءا على خدماتهم المقدمة للعملاء ومدى كفاءتهم بتادية الخدمات </li>
                                    <li> امكانية بيع ملابس مجهزة سابقاً للعملاء </li>
                                    <li> امكانية الاتفاق مع العملاء على تنفيذ ملابس معينة بمقاسات معينة وتصاميم معينة والتسليم بمواعيد محددة</li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/6') }}" class="button">قراءة المزيد</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=6= -->

                    <!--Start Block=7= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector7.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">مجموعة أنظمة قطاع المدارس والمعاهد التعليمية </h2>
                                <ul class="sector_block-list">
                                    <li> امكانية العمل كمدرسة او معهد منفصلين اومجموعة قابضة ويتبعها مجموعة مدارس </li>
                                    <li> تحديد الاقسام والسنوات الدراسية وتقسيم الفصول التابعة لكل سنة دراسية
                                    </li>
                                    <li> تحديد انواع المناهج الدراسية
                                    </li>
                                    <li>تحديد مصروفات السنوات الدراسية ونسب الخصومات على الابناء الاشقاء </li>
                                    <li>تحديد انواع الحافلات وقيمة مصروفات الحافلات وخصم الاخوة </li>
                                    <li> اسهولات اجراءات التسجيل والقبول للدارسين الجدد والمنتقلين بين الصفوف</li>
                                    <li> تسجيل مواعيد الاختبارات ونتائج الاختبارات للدارسين بجميع المراحل الدراسية</li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/7') }}" class="button">قراءة المزيد</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=7= -->

                    <!--Start Block=8= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector8.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">مجموعة أنظمة قطاع التصنيع </h2>
                                <ul class="sector_block-list">
                                    <li> امكانية الاطلاع على التكلفة التقديرية لامر الانتاج بناءاً على التكلفة المعيارية التى تم تعريفها مع المنتجات </li>
                                    <li> امكانية تحويل امر الانتاج لامر تشغيل او عدة اوامر تشغيل</li>
                                    <li> امكانية انشاء طلبات صرف للمواد الخام الخاصة باوامر التشغيل آلياً من النظام</li>
                                    <li>امكانية متابعة المنتجات داخل الاقسام والمراحل الانتاجية </li>
                                    <li>متابعة اوامر التشغيل من حيث وقت البداية والنهاية وما تم صرفه من مواد خام </li>
                                    <li> امكانية مرور المنتجات على مرحلة اختبارات الجودة قبل تحويلها للانتاج التام</li>
                                    <li> الرقابة على المستودعات تحت التشغيل</li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/8') }}" class="button">قراءة المزيد</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=8= -->

                    <!--Start Block=9= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector9.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">مجموعة أنظمة قطاع تأجير السيارات </h2>
                                <ul class="sector_block-list">
                                    <li> سهولة ادخال بيانات اسطول السيارات وفقاً للموديلات والانواع والالوان والفئات </li>
                                    <li> امكانية تقسيم اسطول السيارات الى فئات مختلفة (سيارات كبيرة – متوسطة – صغيرة – سيارات فارهة ..)</li>
                                    <li>امكانية تحديد سياسة التسعير للايجار اليومى والشهرى </li>
                                    <li>امكانية اعداد سياسة للخصومات والعروض </li>
                                    <li> امكانية تقسيم العملاء لفئات وعمل كروت عضويات للعملاء وفقاً للفئات المتاحة مع امكانية انواع مختلفة من الخصومات والمميزات للعملاء حسب فئة العضوية </li>
                                    <li> امكانية فحص السيارة ظاهرياً عند التسليم للعميل وطباعة نموذج الفحص من النظام </li>
                                    <li> سهولة استبدال السيارة فى حال وجود اى مشاكل بالسيارة المؤجرة </li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/9') }}" class="button">قراءة المزيد</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=9= -->


                    <!--Start Block=10= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector10.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">مجموعة أنظمة قطاع النقليات </h2>
                                <ul class="sector_block-list">
                                    <li>متابعة بيانات الشاحنات والسائقين وانواع الحمولات </li>
                                    <li>متابعة جدول الاتجاهات والمسافات بين المدن واسعار الشحن </li>
                                    <li> مصاريف الرحلات الاساسية وفقاً لجدول الاتجاهات والمسافات</li>
                                    <li> ادخال طلبات شحن ومتابعة تحركات الشاحنات واماكن التوقف </li>
                                    <li> متابعة الرحلات المغادرة والقادمة لكل فرع</li>
                                    <li>الشحن من والى الاتجاهات المختلفة ومتابعة تاشيرات السائقين فى حالة الشحن الدولى </li>
                                    <li> اصدار الفواتير وبوالص الشحن للعملاء وتسجيل الاستحقاقات</li>
                                    <li> مركز ربحية لكل شاحنة ولكل سائق </li>
                                    <li>متابعة حالات الشاحنات ( فى الخدمة / متوقفة / معطلة / بالورشة/ فى رحلة /فى انتظار وصولها لفرع)</li>

                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/10') }}" class="button">قراءة المزيد</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=10= -->

                    <!--Start Block=11= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector11.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">مجموعة أنظمة قطاع مكافحة الحشرات </h2>
                                <ul class="sector_block-list">
                                    <li>امكانية تعريف انواع الخدمات التى يتم تقديمها للعملاء </li>
                                    <li>سهولة التعاقد مع العملاء على تقديم الخدمات من خلال النظام </li>
                                    <li>مرونة تحديد طريقة وفترات السداد لقيمة العقد(مقدم/مؤخر—شهرى/ربع سنوى/نصف سنوى/سنوى) </li>
                                    <li> سهولة استخراج فواتير العملاء الدورية آلياً من النظام </li>
                                    <li> سهولة التعديل على العقود مع الاحتفاظ بالبيانات التاريخية السابقة </li>
                                    <li> متابعة المندوبين ومبيعاتهم وتحصيلاتهم من العملاء</li>
                                    <li> مرونة فى تحديد سياسات العمولات للمندوبين</li>
                                    <li>الربط الكامل مع نظام ضريبة القيمة المضافة </li>

                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/11') }}" class="button">قراءة المزيد</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=11= -->

                    <!--Start Block=12= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector12.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name"> مجموعة أنظمة قطاع الاعاشة</h2>
                                <ul class="sector_block-list">
                                    <li>امكانية تعريف انواع الخدمات التى يتم تقديمها للعملاء</li>
                                    <li>سهولة التعاقد مع العملاء على تقديم الخدمات من خلال النظام </li>
                                    <li> مرونة تحديد طريقة وفترات السداد لقيمة العقد(مقدم/مؤخر—شهرى/ربع سنوى/نصف سنوى/سنوى)</li>
                                    <li> سهولة استخراج فواتير العملاء الدورية آلياً من النظام </li>
                                    <li> سهولة التعديل على العقود مع الاحتفاظ بالبيانات التاريخية السابقة </li>
                                    <li> متابعة المندوبين ومبيعاتهم وتحصيلاتهم من العملاء</li>
                                    <li> مرونة فى تحديد سياسات العمولات للمندوبين </li>
                                    <li>تحديد جدول الوجبات الاسبوعى واليومى الذى يتم توريده للعميل </li>

                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/12') }}" class="button">قراءة المزيد</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=12= -->

                    <!--Start Block=13= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector13.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name"> مجموعة أنظمة قطاع الجمعيات الخيرية</h2>
                                <ul class="sector_block-list">
                                    <li> سهولة الهيكلة الادارية لكافة الفروع والادارات </li>
                                    <li> ربط الهياكل الادارية مع مراكز التكلفة المالية </li>
                                    <li>امكانية تسجيل المجالات الخيرية المختلفة التى يتم العمل بها (الصحى-الاجتماعى – المساجد – الدعوى- التعليمى - الاعلامى) </li>
                                    <li> امكانية تسجيل المنح والمبادرات </li>
                                    <li> امكانية عمل خطة استراتيجية للمؤسسة الخيرية وربط انواع مصاريفها وفروعها المنح والمبادرات بالخطة مع امكانية تحديد قيمة كل نوع مصروف على مستوى الفرع والمبادرة</li>
                                    <li> امكانية تسجيل المشاريع المختلفة مع امكانية تحميل كافة مستندات المشروع على النظام</li>
                                    <li>امكانية المقارنة بين المصروفات المقدرة وفقاً للخطة الاستراتيجية والمصروفات الفعلية مع تحديد الانحرافات </li>
                                    <li> اثبات التحويل بنظام المنح فعلياً عند استلام الجهات الخيرية للمبالغ المحولة </li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/13') }}" class="button">قراءة المزيد</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=13= -->

                </div>
            </div>
        </section>
    @else
        <section class="news">
            <div class="container">
                <div class="row">
                    <!--Start Block=1= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector1.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">Construction Sector Systems Group</h2>
                                <ul class="sector_block-list">
                                    <li>Historical data for tenders and its items record.</li>
                                    <li>The cost elements of the tender items and then the price to be presented in the tender.</li>
                                    <li>Measure the profitability of tenders before submitting them to make a decision whether or not to enter the tender.</li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/1') }}" class="button">Read More</a>
                            </div>
                        </div>
                    </div>

                    <!--End Block=1= -->

                    <!--Start Block=2= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector2.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">Maintenance and Operating Systems sector group</h2>

                                <ul class="sector_block-list">
                                    <li>Historical data for tenders and its items record.</li>
                                    <li>Cost elements of the terms of bidding data and then determine the price, which will be presented in the tender.</li>
                                    <li>Measure the profitability of tenders before submitting them to make a decision whether or not to enter the tender.</li>
                                    <li>The possibility of converting the tender after the award to one project or several projects</li>
                                    <li>Project contracts data.</li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/2') }}" class="button">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=2= -->



                    <!--Start Block=3= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector3.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">Group of Real Estate Investment Sector Systems and Property Management</h2>
                                <ul class="sector_block-list">
                                    <li>Land registration data.</li>
                                    <li>The creation of new contributions to the land.</li>
                                    <li>Put forward contributions to the IPO(initial public offering).</li>
                                    <li>Closure of the IPO(initial public offering).</li>
                                    <li>Follow-up buying and selling and trading stock offerings.</li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/3') }}" class="button">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=3= -->

                    <!--Start Block=4= -->
                    <div class="col-xm-12 col-sm-6 col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector4.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">Pharmacies and Pharmaceutical warehouses sector Systems Group</h2>
                                <ul class="sector_block-list">
                                    <li>Complete flexibility in categorizing groups of varieties.</li>
                                    <li>Define the items according to their groups.</li>
                                    <li>Multiple units of measurement for each item and the possibility of dealing within the system.</li>
                                    <li>The possibility of including alternative varieties of the item and attach a realistic picture of it.</li>
                                    <li>Determine the demand limit and the minimum and maximum limit for each item at the level of the class, warehouse or point of sale.</li>
                                    <li>Easy handling with different shipments of the same class and the dates of the different validity also the same class.</li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/4') }}" class="button">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=4= -->



                    <!--Start Block=5= -->
                    <div class="col-xm-12 col-sm-6 col-md-4">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector5.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">Retail and POS systems sector group</h2>
                                <ul class="sector_block-list">
                                    <li>Complete flexibility in categorizing groups of varieties.</li>
                                    <li>Define the items according to their groups.</li>
                                    <li>Multiple measurement units for each class and the possibility of dealing within the system in any of them.</li>
                                    <li>The possibility of alternative items included for the class and attach a realistic picture of him.</li>
                                    <li>Determining the demand limit and the minimum and maximum for each class on the level of product or warehouse or point of sale.</li>
                                    <li>Easy handling with different shipments of the same class and the dates of the different validity also the same class.</li>
                                    <li>Easy to deal with colors, sizes, seasons and varieties of models.</li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/5') }}" class="button">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=5= -->

                    <!--Start Block=6= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector6.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">Beauty and Customization sector Systems Group</h2>
                                <ul class="sector_block-list">
                                    <li>A confidential customer database.</li>
                                    <li>Register and book appointments for customers.</li>
                                    <li>The possibility of choosing an employee of performing the service.</li>
                                    <li>The possibility of choosing one or more of service.</li>
                                    <li>The possibility of granting customers membership with different discount rates for each membership.</li>
                                    <li>Possibility of a promotional offers for customers.</li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/6') }}" class="button">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=6= -->

                    <!--Start Block=7= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector7.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">Schools and educational institutes sector Systems Group </h2>
                                <ul class="sector_block-list">
                                    <li>Possibility to work as a separate school or institute and a holding group followed by a group of schools.</li>
                                    <li>Determination of classes and academic years and the division of classes for each academic year.</li>
                                    <li>Determining the types of curriculum.</li>
                                    <li>Determine the school years expenses and discounts on rates of brothers, sons.</li>
                                    <li>Determining the types of buses, the value of bus expenses and the discount of brothers.</li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/7') }}" class="button">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=7= -->

                    <!--Start Block=8= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector8.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">Manufacturing Sector Systems Group</h2>
                                <ul class="sector_block-list">
                                    <li>Ease of planning for production during the period In the case of continuous production system can determine the absorptive force and productivity of the plant during a given period and is based upon the annual production plan and then the production plan semi-annual and quarterly access to weekly and daily plan.</li>
                                    <li>In the case of production on the basis of orders can start from customer orders and then convert them to orders Antegmn through the system.</li>
                                    <li>Ease of product identification and identification of BOM for each product as well as identification of the stages of the product during the operation and the requirements of each stage of employment, machinery and raw materials.</li>
                                    <li>Easy to define the production stages of production lines in the factory.</li>
                                    <li>Easy to define the machines available in the factory and the rate of depreciation per hour for each machine as well as production capacity.</li>
                                    <li>Definition of the existing workers in the factory and the possibility of linking workers with certain machines.</li>
                                    <li>Definition of work schedules and vortices (shift).</li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/8') }}" class="button">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=8= -->

                    <!--Start Block=9= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector9.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">Group Of Car Rental Sector Systems</h2>
                                <ul class="sector_block-list">
                                    <li>Group car rental sector systems ease the introduction of the fleet of cars according to data models and types and colors and categories.</li>
                                    <li>The possibility of dividing the fleet of cars into different categories (large cars - medium - small - luxury cars ..).</li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/9') }}" class="button">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=9= -->


                    <!--Start Block=10= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector10.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">Transport Sector Systems Group </h2>
                                <ul class="sector_block-list">
                                    <li>Follow-up with the trucks, drivers and load types.</li>
                                    <li>Follow-up of the table of directions and distances between cities and freight prices.</li>
                                    <li>Basic travel expenses according to the schedule of directions and distances.</li>
                                    <li>Enter shipping orders and follow up with truck movements and stopping places.</li>
                                    <li>Follow up with the departing and arriving flights for each branch.</li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/10') }}" class="button">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=10= -->

                    <!--Start Block=11= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector11.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">Pest Control Systems Group</h2>
                                <ul class="sector_block-list">
                                    <li>The possibility of defining the types of services offered to customers.</li>
                                    <li>Ease of contracting with customers to provide services through the system.</li>
                                    <li>Eligibility for determining the method and periods of payment for the contract value (front / back / monthly / quarterly / half yearly / yearly).</li>
                                    <li>Easy to automatically extract customer invoices automatically from the system.</li>
                                    <li>Easy to modify contracts while retaining the previous data.</li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/11') }}" class="button">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=11= -->

                    <!--Start Block=12= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector12.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">Subsistence Sector Systems Group</h2>
                                <ul class="sector_block-list">
                                    <li>The possibility of defining the services offered to the customers.</li>
                                    <li>The ease of contracting with customers to provide the best services through the system.*</li>
                                    <li>Eligibility for determining the methods and periods of payment for the contract value (front / back / monthly / quarterly / half yearly / yearly).</li>
                                    <li>Easy to automatically extract customer invoices automatically from the system.</li>
                                    <li>Easy to modify contracts while retaining historical data.</li>
                                    <li>Follow-up of delegates, sales and collections of customers.</li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/12') }}" class="button">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=12= -->

                    <!--Start Block=13= -->
                    <div class="col-xm-12 col-sm-6  col-md-4 ">
                        <div class="sector_block">
                            <div>
                                <img src="{{ asset('public/Frontend/images/sector13.jpg') }}" alt="{{ trans('main.site_name') }}" />
                            </div>
                            <div>
                                <h2 class="news-name">Charitable Sector Sector Regulations</h2>
                                <ul class="sector_block-list">
                                    <li>Ease of administrative structure in all branches and departments.</li>
                                    <li>Linking administrative structures with financial cost centers.</li>
                                    <li>Possibility of registration of the various charitable fields that are working (health - social - mosques - lawsuit - educational - media).</li>
                                </ul>
                                <a href="{{ url(session()->get('lang').'/private-sectors/13') }}" class="button">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!--End Block=13= -->

                </div>
            </div>
        </section>
    @endif
@stop