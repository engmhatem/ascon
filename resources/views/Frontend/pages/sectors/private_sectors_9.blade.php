@extends(FEL.'.master')

@section('content')
    @if(session()->get('lang') == 'ar')
        <section class="about-bg">
            <h1> مجموعة أنظمة قطاع تأجير السيارات</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector9.jpg') }}" alt="مجموعة أنظمة قطاع تأجير السيارات">
                            </div>
                            <ul class="sector_block-list">
                                <li> سهولة ادخال بيانات اسطول السيارات وفقاً للموديلات والانواع والالوان والفئات</li>
                                <li> امكانية تقسيم اسطول السيارات الى فئات مختلفة (سيارات كبيرة – متوسطة – صغيرة – سيارات فارهة ..)</li>
                                <li> امكانية تحديد سياسة التسعير للايجار اليومى والشهرى</li>
                                <li>امكانية اعداد سياسة للخصومات والعروض </li>
                                <li>امكانية تقسيم العملاء لفئات وعمل كروت عضويات للعملاء وفقاً للفئات المتاحة مع امكانية انواع مختلفة من الخصومات والمميزات للعملاء حسب فئة العضوية </li>
                                <li>امكانية فحص السيارة ظاهرياً عند التسليم للعميل وطباعة نموذج الفحص من النظام </li>
                                <li> تسجيل عقود الايجار على النظام وتحديد القيمة الايجارية وعدد الكيلومترات المسموحة وسعر الكيلومترات الزائدة ومدة عقد الايجاروتسجيل قراءة عداد الكيلومترات وقت خروج السيارة وتاريخ تسليم العميل للسيارة واستلام دفعة مقدمة من العميل </li>
                                <li> استلام السيارة من العميل وعمل الفحص الظاهرى للسيارة وتسجيله بالنظام وفى حالة وجود اية تلفيات يتم تحميلها على العميل حسب السياسة المتفق عليها وتسجيل قراءة العداد وتسجيلها بالنظام فيقوم بحساب الكيلومترات الزائدة ومن ثم اعداد فاتورة للعميل تشمل المستحقات عليها ويخصم منها ما تم سداده سابقاً ويتم اقفال العقد مع سداد العميل للمستحقات عليه </li>
                                <li>فى حال وجود عدة فروع للشركة هناك مرونة بالنظام لان يتم تسليم السيارة واقفال العقد باى فرع من فروع الشركة </li>
                                <li> امكانية التعامل مع الايجار طويل الاجل للشركات وتسجيل بيانات موظفى الشركات والمستخدمين الفعليين للسيارات</li>
                                <li> وفى حالة الانتاج على اساس الطلبيات يمكن البدء من طلبيات العملاء ومن ثم تحويلها لاوامر انتاجمن خلال النظام مع الفوترة الشهرية للشركة واثبات المستحق عليها بالنظام </li>
                                <li> سهولة استبدال السيارة فى حال وجود اى مشاكل بالسيارة المؤجرة</li>
                                <li> امكانية دخول السيارة للورشة للصيانة الدورية او الطارئة مع استبعادها من المتاح للتاجير خلال فترة صيانتها </li>
                                <li> امكانية نقل السيارات بين الفروع</li>
                                <li> تقارير احصائية عن كل سيارة عقود ايجارها وقيمة الايرادات وكذلك المصروفات عليها مما يتيح معرفة جدوى الابقاء عليها او بيعها باقرب وقت لتفادى الخسائر</li>
                                <li> بيان بالسيارات المتاحة للتاجير وموديلاتها وفئاتها والسيارات المتوقفة بالصيانة او المستبعدة </li>
                                <li> بيان بالمستحقات على العقود والعملاء</li>
                                <li> طباعة عقود الايجار والفواتير </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">جميع القطاعات</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">مجموعة أنظمة قطاع المقاولات</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">مجموعة أنظمة قطاع الصيانة والتشغيل </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}"> مجموعة أنظمة قطاع الاستثمار العقارى وادارة الاملاك </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}"> مجموعة أنظمة قطاع الصيدليات ومستودعات الادوية </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}">مجموعة أنظمة قطاع تجارة التجزئة ونقاط البيع </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/6') }}">مجموعة أنظمة قطاع التجميل والتفصيل</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}"> مجموعة أنظمة قطاع المدارس والمعاهد التعليمية </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/8') }}">مجموعة أنظمة قطاع التصنيع </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}"> مجموعة أنظمة قطاع النقليات </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}"> مجموعة أنظمة قطاع مكافحة الحشرات </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}"> مجموعة أنظمة قطاع الاعاشة </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}"> مجموعة أنظمة قطاع الجمعيات الخيرية </a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @else
        <section class="about-bg">
            <h1>Group Of Car Rental Sector Systems</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector9.jpg') }}" alt="مجموعة أنظمة قطاع تأجير السيارات">
                            </div>
                            <ul class="sector_block-list">
                                <li>Group car rental sector systems ease the introduction of the fleet of cars according to data models and types and colors and categories.</li>
                                <li>The possibility of dividing the fleet of cars into different categories (large cars - medium - small - luxury cars ..).</li>
                                <li>The possibility of determining pricing policy for daily and monthly rent.</li>
                                <li>The possibility of preparing a policy for discounts and offers.</li>
                                <li>The possibility of dividing customers into categories and making membership cards for customers according to the available categories with the possibility of different types of discounts and benefits to customers according to the membership category.</li>
                                <li>The possibility of checking the vehicle on the delivery of the customer and print the examination form of the system.</li>
                                <li>Registration of rent contracts on the system, determining the rental value, the permitted number of kilometers, the excess mileage, the duration of the rental contract, the reading of the mileage meter at the time of the departure of the car, the date of delivery of the customer to the vehicle and the receipt of a payment from the customer.</li>
                                <li>The car is received from the client and the work of the visual inspection of the vehicle and its registration system. In case there is any damage to be loaded on the client as per the agreed policy and record the meter reading and recording system who shall calculate the extra kilometers and then prepare an invoice for the customer include dues and deducting what has been paid previously and is closing the contract with payment of customer receivables on it.</li>
                                <li>In case there are several branches of the company there is flexibility in the system because the delivery of the car and the closing of the contract in any branch of the company.</li>
                                <li>Possibility of dealing with long term rental of companies and recording data of employees of companies and actual users of cars.</li>
                                <li>In the case of production on the basis of orders can start from customer orders and then convert them to production orders through the system with the monthly billing of the company and prove the system.</li>
                                <li>Easy replacement of the car in the event of any problems with the other rented car.</li>
                                <li>The possibility of entering the car workshop for regular maintenance or emergency with their exclusion from available for rent during the period of maintenance.</li>
                                <li>The possibility of car transfer between branches.</li>
                                <li>Statistical reports for each car to lease contracts and the value of revenues and expenses as well as allowing them to know the feasibility of keeping them or sell them as soon as to avoid losses.</li>
                                <li>Statement of cars available for rent and models and categories and parked cars, maintenance or excluded.</li>
                                <li>Statement of receivables on contracts and customers.</li>
                                <li>Print rent invoices contracts.</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">Private Sectors</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">Construction Sector Systems Group</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">Maintenance and Operating Systems sector group </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}">Group of Real Estate Investment Sector Systems and Property Management</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}">Pharmacies and Pharmaceutical Warehouses Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}">Retail and POS Systems Sector Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/6') }}">Beauty and Customization sector Systems Group</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}">Schools and educational institutes sector Systems Group </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/8') }}">Manufacturing Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}">Transport Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}">Pest Control Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}">Subsistence Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}">Charitable Sector Sector Regulations</a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @endif
    @include(FE.'.pages.sectors.sector_form')
@stop