@extends(FEL.'.master')

@section('content')
    @if(session()->get('lang') == 'ar')
        <section class="about-bg">
            <h1> مجموعة أنظمة قطاع التصنيع</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector8.jpg') }}" alt="مجموعة أنظمة قطاع التصنيع">
                            </div>
                            <ul class="sector_block-list">
                                <li>
                                    سهولة التخطيط للانتاج خلال فترة ففى حالة الانتاج المستمر يمكن النظام من تحديد القوة الاستيعابية والانتاجية للمصنع خلال فترة معينة وبناءا عليه يتم وضع خطة الانتاج السنوية ومن ثم خطة الانتاج النصف سنوية والربع سنوية وصولاً الى الخطة الاسبوعية واليومية </li>
                                <li> وفى حالة الانتاج على اساس الطلبيات يمكن البدء من طلبيات العملاء ومن ثم تحويلها لاوامر انتاجمن خلال النظام</li>
                                <li> سهولة تعريف المنتجات وتحديد ال BOM لكل منتج وكذلك تحديد المراحل التى يمر عليها المنتج اثناء التشغيل ومتطلبات كل مرحلة من العمالة والماكينات والمواد الخام</li>
                                <li> سهولة تعريف المراحل الانتاجية لخطوط الانتاج الموجودة بالمصنع </li>
                                <li>سهولة تعريف الماكينات المتاحة بالمصنع ومعدل اهلاك الساعة لكل ماكينة وكذلك السعة الانتاجية</li>
                                <li>تعريف العمالة الموجودة بالمصنع وامكانية ربط عمال مع ماكينات معينة </li>
                                <li>تعريف مواعيد العمل والدوامات (الورديات) </li>
                                <li> امكانية تقسيم المستودعات المرتبطة بالمصنع الى مستودعات المواد الخام ومستودعات الانتاج تحت التشغيل ومستودعات الانتاج التام</li>
                                <li> يبدا التشغيل اعتبارا من اعداد امر الانتاج سواء بناءاً على خطة انتاج او عن طلبية عميل او امر انتاج مباشر</li>
                                <li> امكانية الاطلاع على التكلفة التقديرية لامر الانتاج بناءاً على التكلفة المعيارية التى تم تعريفها مع المنتجات </li>
                                <li> امكانية تحويل امر الانتاج لامر تشغيل او عدة اوامر تشغيل</li>
                                <li> امكانية انشاء طلبات صرف للمواد الخام الخاصة باوامر التشغيل آلياً من النظام </li>
                                <li>امكانية متابعة المنتجات داخل الاقسام والمراحل الانتاجية </li>
                                <li> متابعة اوامر التشغيل من حيث وقت البداية والنهاية وما تم صرفه من مواد خام </li>
                                <li> امكانية مرور المنتجات على مرحلة اختبارات الجودة قبل تحويلها للانتاج التام</li>
                                <li>الرقابة على المستودعات تحت التشغيل </li>
                                <li>سهولة قياس الانحرافات بين التكلفة المعيارية والتكلفة الفعلية لاوامر الانتاج </li>
                                <li>امكانية تحميل التكاليف غير المباشرة على اوامر الانتاج على اساس نسب يتم تحديدها مسبقاً بناءاً على الدراسات السابقة </li>
                                <li> امكانية اتمام اوامر التشغيل وتحويل المنتجات لمستودع الانتاج التام من خلال شاشات مخصصة لذلك بالنظام</li>
                                <li> الانتاج
                                </li>
                                <li>بناءاً على خطة الانتاج يمكن للنظام تحديد المتطلبات من المواد الخام ومقارنتها مع الارصدة المتواجدة بالفعل بالمستودعات مع امكانية انشاء طلب شراء آلى فى حالة عدم كفاية الارصدة الحالية لمتطلبات </li>
                                <li>تقارير لمتابعة اوامر التشغيل – موقف اوامر الانتاج </li>
                                <li>تقارير لمتابعة طلبيات العملاء </li>
                                <li>تقارير لمتابعة المراحل الانتاجية </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">جميع القطاعات</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">مجموعة أنظمة قطاع المقاولات</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">مجموعة أنظمة قطاع الصيانة والتشغيل </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}"> مجموعة أنظمة قطاع الاستثمار العقارى وادارة الاملاك </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}"> مجموعة أنظمة قطاع الصيدليات ومستودعات الادوية </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}">مجموعة أنظمة قطاع تجارة التجزئة ونقاط البيع </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/6') }}">مجموعة أنظمة قطاع التجميل والتفصيل</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}"> مجموعة أنظمة قطاع المدارس والمعاهد التعليمية </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}"> مجموعة أنظمة قطاع تأجير السيارات </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}"> مجموعة أنظمة قطاع النقليات </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}"> مجموعة أنظمة قطاع مكافحة الحشرات </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}"> مجموعة أنظمة قطاع الاعاشة </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}"> مجموعة أنظمة قطاع الجمعيات الخيرية </a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @else
        <section class="about-bg">
            <h1>Manufacturing Sector Systems Group</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector8.jpg') }}" alt="Manufacturing Sector Systems Group">
                            </div>
                            <ul class="sector_block-list">
                                <li>Ease of planning for production during the period In the case of continuous production system can determine the absorptive force and productivity of the plant during a given period and is based upon the annual production plan and then the production plan semi-annual and quarterly access to weekly and daily plan.</li>
                                <li>In the case of production on the basis of orders can start from customer orders and then convert them to orders Antegmn through the system.</li>
                                <li>Ease of product identification and identification of BOM for each product as well as identification of the stages of the product during the operation and the requirements of each stage of employment, machinery and raw materials.</li>
                                <li>Easy to define the production stages of production lines in the factory.</li>
                                <li>Easy to define the machines available in the factory and the rate of depreciation per hour for each machine as well as production capacity.</li>
                                <li>Definition of the existing workers in the factory and the possibility of linking workers with certain machines.</li>
                                <li>Definition of work schedules and vortices (shift).</li>
                                <li>The possibility of splitting the warehouses associated with the factory to the warehouse of raw materials and production depots and warehouses operating under full production.</li>
                                <li>Start operating as of the preparation of the production order, whether based on the production plan or an order for a client or is a direct production.</li>
                                <li>Access to the estimated cost of production is based on the standard cost that has been defined with the products.</li>
                                <li>The possibility of converting the production order to one or more operation orders.</li>
                                <li>The possibility of establishing exchange requests for raw materials for automatically operating orders from the system.</li>
                                <li>The possibility of follow-up products within the sections and stages of production.</li>
                                <li>Follow-up orders operating in terms of the start time and the end of what has been spent from raw materials.</li>
                                <li>The possibility of products pass quality tests on the stage before converting full production.</li>
                                <li>Control the warehouses under operation.</li>
                                <li>Ease of measurement of deviations between standard cost and actual cost of production orders.</li>
                                <li>The possibility of loading indirect costs on production orders based on predetermined percentages based on previous studies.</li>
                                <li>The possibility of completing the operation orders and converting the products to the complete production warehouse through screens dedicated to the system.</li>
                                <li>Production.</li>
                                <li>Based on the production plan, the system can determine the requirements of the raw materials and compare them with the balances already in the warehouses with the possibility of establishing an automatic purchase order in case of insufficient current balances of requirements.</li>
                                <li>Reports to follow-up orders - Position of the product orders.</li>
                                <li>Reports to follow up customer orders.</li>
                                <li>Reports to follow the production stages.</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">Private Sectors</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">Construction Sector Systems Group</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">Maintenance and Operating Systems sector group </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}">Group of Real Estate Investment Sector Systems and Property Management</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}">Pharmacies and Pharmaceutical Warehouses Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}">Retail and POS Systems Sector Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/6') }}">Beauty and Customization sector Systems Group</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}">Schools and educational institutes sector Systems Group </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}">Group Of Car Rental Sector Systems</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}">Transport Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}">Pest Control Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}">Subsistence Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}">Charitable Sector Sector Regulations</a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @endif
    @include(FE.'.pages.sectors.sector_form')
@stop