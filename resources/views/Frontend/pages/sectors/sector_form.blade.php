<!--Start Sector single page Form-->

<section class="sector_form" id="ApplyForm">
    <div class="container">
        <div class="form_content">
            @if(session()->has('contact_success'))
                <div class="alert alert-success">
                    {{ session()->get('contact_success') }}
                </div>
            @endif
            <h3>{{ trans('main.ask_for_service_now') }}</h3>
            {!! Form::open(['url'=>'apply-for-sector','class'=>'row']) !!}
                <div class="col-sm-12 col-lg-4">
                    {!! Form::hidden('sector_id',$id) !!}
                    {!! Form::hidden('lang',session()->get('lang')) !!}
                    {!! Form::text('name',old('name'),['placeholder'=>trans('main.name'),'required']) !!}
                    @if($errors->has('name'))
                        <p class="text-danger">{{ $errors->first('name') }}</p>
                    @endif
                </div>

                <div class="col-sm-12 col-lg-4">
                    {!! Form::text('email',old('email'),['placeholder'=>trans('main.email'),'required']) !!}
                    @if($errors->has('email'))
                        <p class="text-danger">{{ $errors->first('email') }}</p>
                    @endif
                </div>

                <div class="col-sm-12 col-lg-4">
                    {!! Form::text('phone',old('phone'),['placeholder'=>trans('main.phone'),'required']) !!}
                    @if($errors->has('phone'))
                        <p class="text-danger">{{ $errors->first('phone') }}</p>
                    @endif
                </div>
                <div class="col-sm-12">
                    {!! Form::textarea('message',old('message'),['placeholder'=>trans('main.message'),'required']) !!}
                    @if($errors->has('message'))
                        <p class="text-danger">{{ $errors->first('message') }}</p>
                    @endif
                </div>

                <div class="col-sm-12">
                    <button type="submit">{{ trans('main.send') }}</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</section>
<!--end Sector single page Form-->
@section('js')
    @if(session()->has('contact_success'))
        <script>
            location.href = "#ApplyForm";
        </script>
    @endif
@stop