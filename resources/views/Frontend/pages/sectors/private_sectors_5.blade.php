@extends(FEL.'.master')

@section('content')
    @if(session()->get('lang') == 'ar')
        <section class="about-bg">
            <h1>
                مجموعة أنظمة قطاع تجارة التجزئة ونقاط البيع
                (عطور - ساعات – ملابس – مواد غذائية – مطاعم – مواد بناء)
            </h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector5.jpg') }}" alt="مجموعة أنظمة قطاع تجارة التجزئة ونقاط البيع">
                            </div>
                            <ul class="sector_block-list">
                                <li>مرونة كاملة فى تصنيف مجموعات الاصناف بشكل شجرى</li>
                                <li>تعريف الاصناف وفقاً لمجموعاتها</li>
                                <li>وحدات قياس متعددة لكل صنف وامكانية التعامل داخل النظام باى منها</li>
                                <li>امكانية ادراج الاصناف البديلة للصنف وارفاق صورة واقعية له</li>
                                <li>تحديد حد الطلب والحد الادنى والاقصى لكل صنف على مستوى الصنف او المستودع او نقطة البيع</li>
                                <li>سهولة التعامل مع الشحنات المختلفة لنفس الصنف وتواريخ الصلاحية المختلفة ايضا لنفس الصنف</li>
                                <li>سهولة التعامل مع الالوان والمقاسات والمواسم والموديلات للاصناف </li>
                                <li>سهولة انشاء رقم الصنف وفقاً للطريقة التى يرغبها المستخدم</li>
                                <li>سهولة انشاء الباركود لكل صنف</li>
                                <li>سهولة التعامل مع اصناف المطاعم وتكوين الوجبات وكذلك التعامل مع الطاولات والحجوزات</li>
                                <li>ادارة متكاملة للمخزون بدءاً من دورة المشتريات واستلام البضائع وصرفها وتحويلها للمستودعات ونقاط البيع</li>
                                <li>سياسات تسعير للاصناف وخصومات وعروض ترويجية</li>
                                <li>نظام الولاء للعملاء واحتساب النقاط والمكافآت وطباعة بطاقات للعملاء </li>
                                <li>إمكانية تعليق الفواتير</li>
                                <li> اوقات دوام خاصة لكل نقطة بيع </li>
                                <li>سهولة السداد وفقاً لطرق السداد المختلفة لدى العملاء</li>
                                <li> سهولة التعامل مع الاجهزة الملحقة (طابعات- قارىء الباركود – الدرج – شاشة العرض للعملاء – الموازين)</li>
                                <li>الجرد باستخدام جهاز data collector</li>
                                <li>سهولة التعامل مع مبيعات السيارات والمندوبين واستخدام جهاز Handheld لفوترة العملاء بمبيعات السيارات والمندوبين </li>
                                <li> تقارير احصائية لحركة مبيعات الاصناف والاكثر مبيعاً وربحية الاصناف</li>
                                <li>تقارير احصائية للمخزون والاصناف التى وصلت لحد الطلب ومعدل دوران المخزون</li>
                                <li>تقارير لمتابعة طلبات المواد وعروض الاسعار واوامر الشراء والاستلامات والتحويلات والمرتجعات والتوالف</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">جميع القطاعات</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">مجموعة أنظمة قطاع المقاولات</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">مجموعة أنظمة قطاع الصيانة والتشغيل </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}"> مجموعة أنظمة قطاع الاستثمار العقارى وادارة الاملاك </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}"> مجموعة أنظمة قطاع الصيدليات ومستودعات الادوية </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/6') }}"> مجموعة أنظمة قطاع التجميل والتفصيل </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}"> مجموعة أنظمة قطاع المدارس والمعاهد التعليمية </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/8') }}"> مجموعة أنظمة قطاع التصنيع </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}"> مجموعة أنظمة قطاع تأجير السيارات </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}"> مجموعة أنظمة قطاع النقليات </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}"> مجموعة أنظمة قطاع مكافحة الحشرات </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}"> مجموعة أنظمة قطاع الاعاشة </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}"> مجموعة أنظمة قطاع الجمعيات الخيرية </a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @else
        <section class="about-bg">
            <h1>Retail and POS Systems Sector Group</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector5.jpg') }}" alt="Retail and POS Systems Sector Group">
                            </div>
                            <ul class="sector_block-list">
                                <li>Complete flexibility in categorizing groups of varieties.</li>
                                <li>Define the items according to their groups.</li>
                                <li>Multiple measurement units for each class and the possibility of dealing within the system in any of them.</li>
                                <li>The possibility of alternative items included for the class and attach a realistic picture of him.</li>
                                <li>Determining the demand limit and the minimum and maximum for each class on the level of product or warehouse or point of sale.</li>
                                <li>Easy handling with different shipments of the same class and the dates of the different validity also the same class.</li>
                                <li>Easy to deal with colors, sizes, seasons and varieties of models.</li>
                                <li>Easily create product number in the manner desired by the user.</li>
                                <li>Easily create barcodes for each class.</li>
                                <li>Easy to deal with varieties of restaurants and composition of meals as well as dealing with tables and bookings.</li>
                                <li>Integrated inventory management, starting from the procurement cycle, receiving goods, transferring them and transferring them to warehouses and points of sale.</li>
                                <li>Pricing policies for categories, discounts and promotions.</li>
                                <li>Loyalty system for customers, counting points, rewards and printing cards for customers.</li>
                                <li>Ability to suspend invoices.</li>
                                <li>Special times for each point of sale.</li>
                                <li>Easy payment according to different payment methods of customers.</li>
                                <li>Ease of dealing with the attached devices (printers - barcode reader - stairs - customer display screen - scales).</li>
                                <li>Inventory using data Collister device.</li>
                                <li>Ease of dealing with sales of cars and delegates and the use of handheld device for billing customers with sales of cars and delegates.</li>
                                <li>Statistical reports of sales of items, best sellers and profitability of varieties.</li>
                                <li>Statistical reports of stocks and items that reached demand and inventory turnover.</li>
                                <li>Reports to follow up material orders, quotations, purchase orders, receipts, transfers, returns and transfers.</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">Private Sectors</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">Construction Sector Systems Group</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">Maintenance and Operating Systems sector group </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}">Group of Real Estate Investment Sector Systems and Property Management</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}">Pharmacies and Pharmaceutical Warehouses Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/6') }}">Beauty and Customization sector Systems Group</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}">Schools and educational institutes sector Systems Group </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/8') }}">Manufacturing Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}">Group Of Car Rental Sector Systems</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}">Transport Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}">Pest Control Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}">Subsistence Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}">Charitable Sector Sector Regulations</a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @endif
    @include(FE.'.pages.sectors.sector_form')
@stop