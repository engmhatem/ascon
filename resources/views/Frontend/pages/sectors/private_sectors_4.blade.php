@extends(FEL.'.master')

@section('content')
    @if(session()->get('lang') == 'ar')
        <section class="about-bg">
            <h1>مجموعة أنظمة قطاع الصيدليات ومستودعات الادوية</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector4.jpg') }}" alt="مجموعة أنظمة قطاع الصيدليات ومستودعات الادوية">
                            </div>
                            <ul class="sector_block-list">
                                <li> مرونة كاملة فى تصنيف مجموعات الاصناف بشكل شجرى</li>
                                <li> تعريف الاصناف وفقاً لمجموعاتها </li>
                                <li> وحدات قياس متعددة لكل صنف وامكانية التعامل داخل النظام باى منها</li>
                                <li> امكانية ادراج الاصناف البديلة للصنف وارفاق صورة واقعية له </li>
                                <li> تحديد حد الطلب والحد الادنى والاقصى لكل صنف على مستوى الصنف او المستودع او نقطة البيع</li>
                                <li> سهولة التعامل مع الشحنات المختلفة لنفس الصنف وتواريخ الصلاحية المختلفة ايضا لنفس الصنف</li>
                                <li> التعامل مع الموردين والوكلاء والشركات المصنعه </li>
                                <li>امكانية تحديد عمولات الصيادلة نسبة او مبلغ مع كل صنف </li>
                                <li>امكانية تحديد مبيعات اللستة لكل صيدلى واحتساب العمولات المستحقة </li>
                                <li> امكانية انشاء عروض ترويجية لكل صنف خلال فترة زمنية محددة</li>
                                <li> تحديد فترات دوام الصيدليات والصيادلة </li>
                                <li> دورة مشتريات كاملة بالاضافة الى امكانية الصرف باستخدام طريقة الوارد اولا صادر اولا</li>
                                <li> استخدام رقم الباتش وفقاً لتعليمات وزارة الصحة</li>
                                <li>متابعة الاتفاقيات مع الموردين ونسب الخصومات والبونص </li>
                                <li> مستويات متعددة لنسب الخصم فى المشتريات واتفاقيات الموردين </li>
                                <li> الجرد وفقاً لتواريخ الصلاحية والموردين</li>
                                <li> تحديد حد ركود الاصناف (فترات ايام)</li>
                                <li> تحديد فترة تحديد الاصناف المقاربة على انتهاء الصلاحية </li>
                                <li> مبيعات الجملة وفئات ومستويات الخصم لعملاء الجملة</li>
                                <li>امكانية عمل العروض الترويجية باشكال وافكار متعددة من داخل النظام </li>
                                <li>سهولة احتساب النقاط للعملاء والاطلاع على مكافآتهم وكذلك سهولة استبدال النقاط وذلك من خلال الربط مع نظام الولاء واحتساب النقاط </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">جميع القطاعات</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">مجموعة أنظمة قطاع المقاولات</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">مجموعة أنظمة قطاع الصيانة والتشغيل </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}"> مجموعة أنظمة قطاع الاستثمار العقارى وادارة الاملاك </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}"> مجموعة أنظمة قطاع تجارة التجزئة ونقاط البيع </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/6') }}"> مجموعة أنظمة قطاع التجميل والتفصيل </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}"> مجموعة أنظمة قطاع المدارس والمعاهد التعليمية </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/8') }}"> مجموعة أنظمة قطاع التصنيع </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}"> مجموعة أنظمة قطاع تأجير السيارات </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}"> مجموعة أنظمة قطاع النقليات </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}"> مجموعة أنظمة قطاع مكافحة الحشرات </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}"> مجموعة أنظمة قطاع الاعاشة </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}"> مجموعة أنظمة قطاع الجمعيات الخيرية </a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @else
        <section class="about-bg">
            <h1>Pharmacies and Pharmaceutical warehouses sector Systems Group</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector4.jpg') }}" alt="Pharmacies and Pharmaceutical warehouses sector Systems Group">
                            </div>
                            <ul class="sector_block-list">
                                <li>Complete flexibility in categorizing groups of varieties.</li>
                                <li>Define the items according to their groups.</li>
                                <li>Multiple units of measurement for each item and the possibility of dealing within the system.</li>
                                <li>The possibility of including alternative varieties of the item and attach a realistic picture of it.</li>
                                <li>Determine the demand limit and the minimum and maximum limit for each item at the level of the class, warehouse or point of sale.</li>
                                <li>Easy handling with different shipments of the same class and the dates of the different validity also the same class.</li>
                                <li>Dealing with suppliers, agents and manufacturers.</li>
                                <li>The possibility of determining the proportion of pharmacists with commissions or the amount of each item.</li>
                                <li>The possibility of determining the sales of the net for each pharmacist and the calculation of the commissions due.</li>
                                <li>The possibility of creating promotional offers for each class within a specified period of time.</li>
                                <li>Determining the time periods of pharmacies and pharmacists.</li>
                                <li>Complete purchase cycle in addition to the possibility of exchange using the method first received first.</li>
                                <li>Use the Bach number according to the instructions of the Ministry of Health.</li>
                                <li>Follow-up agreements with suppliers and ratios discounts and bonus.</li>
                                <li>Multiple levels of discount in purchases and supplier agreements.</li>
                                <li>Inventory according to the validity dates and suppliers.</li>
                                <li>Determine the extent recession varieties (periods of days).</li>
                                <li>Determine the period of identifying items on the approach expiration.</li>
                                <li>Wholesale sales categories and discount levels for wholesale customers.</li>
                                <li>The possibility of making promotional offers in multiple forms and ideas from within the system.</li>
                                <li>Ease of calculating the points for customers and see the bonuses as well as easy to redeem points through a linkage with the loyalty system and calculate the points.</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">Private Sectors</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">Construction Sector Systems Group</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">Maintenance and Operating Systems sector group </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}">Group of Real Estate Investment Sector Systems and Property Management</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}">Retail and POS Systems Sector Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/6') }}">Beauty and Customization sector Systems Group</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}">Schools and educational institutes sector Systems Group </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/8') }}">Manufacturing Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}">Group Of Car Rental Sector Systems</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}">Transport Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}">Pest Control Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}">Subsistence Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}">Charitable Sector Sector Regulations</a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @endif
    @include(FE.'.pages.sectors.sector_form')
@stop