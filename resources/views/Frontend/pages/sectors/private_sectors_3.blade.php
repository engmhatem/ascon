@extends(FEL.'.master')

@section('content')
    @if(session()->get('lang') == 'ar')
        <section class="about-bg">
            <h1> مجموعة أنظمة قطاع الاستثمار العقارى وادارة الاملاك</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector3.jpg') }}" alt="مجموعة أنظمة قطاع الاستثمار العقارى وادارة الاملاك">
                            </div>
                            <h3> قطاع الاستثمار العقارى</h3>
                            <ul class="sector_block-list">
                                <li>تسجيل بيانات الاراضى </li>
                                <li>انشاء مساهمات جديدة على الاراضى </li>
                                <li> طرح المساهمات للاكتتاب</li>
                                <li>اغلاق الاكتتاب </li>
                                <li>متابعة عروض البيع والشراء وتداول الاسهم </li>
                                <li> تقسيم وبيع اراضى المساهمات</li>
                                <li>تصفية المساهمات </li>
                                <li> توزيع وصرف الارباح ورأس المال</li>
                            </ul>

                            <h3> قطاع ادارة الاملاك</h3>
                            <ul class="sector_block-list">
                                <li> اعداد شجرة العقارات والمواقع والمناطق</li>
                                <li> بيانات العقارات والوحدات داخل العقارات </li>
                                <li> سهولة التعامل مع البنود المتغيرة مع الوحدات (المياه – الكهرباء – التامين - .........)</li>
                                <li> بيانات المستاجرين والمالكين</li>
                                <li> عقود ايجار الوحدات (قيمة الايجار / السعى / التامين / المياة / الدفعة المقدمة)</li>
                                <li> امكانية توسعة وفكك وضم الوحدات </li>
                                <li>امكانية ابرام عقود مؤقتة (الفعاليات والاحتفالات ........) </li>
                                <li> امكانية تاجير مساحات اعلانية </li>
                                <li>امكانية تاجير مستودعات ملحقة للوحدات</li>
                                <li> تقارير الايجارات المستحقة </li>
                                <li>تقارير بالعقود التى ستنتهى خلال فترة </li>
                                <li> صيانة العقارات</li>
                                <li> انهاء عقود الايجار وامكانية بيع العقار او وحدات من العقار ومتابعة عقود البيع والاقساط</li>

                            </ul>




                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">جميع القطاعات</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">مجموعة أنظمة قطاع المقاولات</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">مجموعة أنظمة قطاع الصيانة والتشغيل </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}"> مجموعة أنظمة قطاع الصيدليات ومستودعات الادوية </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}"> مجموعة أنظمة قطاع تجارة التجزئة ونقاط البيع </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/6') }}"> مجموعة أنظمة قطاع التجميل والتفصيل </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}"> مجموعة أنظمة قطاع المدارس والمعاهد التعليمية </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/8') }}"> مجموعة أنظمة قطاع التصنيع </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}"> مجموعة أنظمة قطاع تأجير السيارات </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}"> مجموعة أنظمة قطاع النقليات </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}"> مجموعة أنظمة قطاع مكافحة الحشرات </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}"> مجموعة أنظمة قطاع الاعاشة </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}"> مجموعة أنظمة قطاع الجمعيات الخيرية </a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @else
        <section class="about-bg">
            <h1>Group of Real Estate Investment Sector Systems and Property Management</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector3.jpg') }}" alt="Group of Real Estate Investment Sector Systems and Property Management">
                            </div>
                            <h3>Real estate investment sector</h3>
                            <ul class="sector_block-list">
                                <li>Land registration data.</li>
                                <li>The creation of new contributions to the land.</li>
                                <li>Put forward contributions to the IPO(initial public offering).</li>
                                <li>Closure of the IPO(initial public offering).</li>
                                <li>Follow-up buying and selling and trading stock offerings.</li>
                                <li>Division and sale of land contributions.</li>
                                <li>Filter contributions.</li>
                                <li>Distribution and disbursement of profits and capital.</li>
                            </ul>

                            <h3>Property Management Sector</h3>
                            <ul class="sector_block-list">
                                <li>Preparation of real estate sites, areas and places.</li>
                                <li>Preparation of real estate sites and tree areas.</li>
                                <li>Real estate data and units within the real estate.</li>
                                <li>Easy to deal with the changing items with units (water - electricity - insurance - .........).</li>
                                <li>Data of renters and owners.</li>
                                <li>Rent Units contracts (rent / seek / insurance / water / value payment).</li>
                                <li>The possibility of expansion and annexation cannibalize units.Possibility of concluding temporary contracts (events and celebrations ........).</li>
                                <li>The possibility of renting advertising space.</li>
                                <li>Possibility of renting warehouses attached to units.</li>
                                <li>Rents outstanding reports.</li>
                                <li>Reports of contracts that will expire during the period.</li>
                                <li>Maintenance of real estate.</li>
                                <li>End lease contracts and the possibility of selling the property or units of the property and follow-up sales and premium contracts.</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">Private Sectors</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">Construction Sector Systems Group</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">Maintenance and Operating Systems sector group </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}">Pharmacies and Pharmaceutical Warehouses Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}">Retail and POS Systems Sector Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/6') }}">Beauty and Customization sector Systems Group</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/7') }}">Schools and educational institutes sector Systems Group </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/8') }}">Manufacturing Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}">Group Of Car Rental Sector Systems</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}">Transport Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}">Pest Control Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}">Subsistence Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}">Charitable Sector Sector Regulations</a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @endif
    @include(FE.'.pages.sectors.sector_form')
@stop