@extends(FEL.'.master')

@section('content')
    @if(session()->get('lang') == 'ar')
        <section class="about-bg">
            <h1>مجموعة أنظمة قطاع المدارس والمعاهد التعليمية</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector7.jpg') }}" alt="مجموعة أنظمة قطاع المدارس والمعاهد التعليمية">
                            </div>
                            <ul class="sector_block-list">
                                <li> امكانية العمل كمدرسة او معهد منفصلين اومجموعة قابضة ويتبعها مجموعة مدارس </li>
                                <li> تحديد الاقسام والسنوات الدراسية وتقسيم الفصول التابعة لكل سنة دراسية</li>
                                <li> تحديد انواع المناهج الدراسية</li>
                                <li> تحديد مصروفات السنوات الدراسية ونسب الخصومات على الابناء الاشقاء</li>
                                <li> تحديد انواع الحافلات وقيمة مصروفات الحافلات وخصم الاخوة </li>
                                <li> سهولات اجراءات التسجيل والقبول للدارسين الجدد والمنتقلين بين الصفوف</li>
                                <li>تسجيل مواعيد الاختبارات ونتائج الاختبارات للدارسين بجميع المراحل الدراسية </li>
                                <li>امكانية ارفاق المستندات الخاصة بالدارسين مع ملفاتهم داخل النظام </li>
                                <li> طباعة التقارير الشهرية للدارسين والسنوية</li>
                                <li> امكانية استخراج كشف حساب على مستوى كل ولى امر مفصل على مستوى الابناء واخر مجمع</li>
                                <li> تقارير عن ايرادات كل مرحلة من المراحل الدراسية وتقارير ايرادات مجمعة على مستوى كافة المراحل الدراسية</li>
                                <li>سهولة التواصل مع اولياء الامور من خلال النظام عن طريق الرسائل القصيرة التى ترسل مباشرة من النظام </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">جميع القطاعات</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">مجموعة أنظمة قطاع المقاولات</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">مجموعة أنظمة قطاع الصيانة والتشغيل </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}"> مجموعة أنظمة قطاع الاستثمار العقارى وادارة الاملاك </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}"> مجموعة أنظمة قطاع الصيدليات ومستودعات الادوية </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}">مجموعة أنظمة قطاع تجارة التجزئة ونقاط البيع </a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/6') }}">مجموعة أنظمة قطاع التجميل والتفصيل</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/8') }}"> مجموعة أنظمة قطاع التصنيع </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}"> مجموعة أنظمة قطاع تأجير السيارات </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}"> مجموعة أنظمة قطاع النقليات </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}"> مجموعة أنظمة قطاع مكافحة الحشرات </a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}"> مجموعة أنظمة قطاع الاعاشة </a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}"> مجموعة أنظمة قطاع الجمعيات الخيرية </a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @else
        <section class="about-bg">
            <h1>Schools and educational institutes sector Systems Group</h1>
        </section>
        <!-- Start Section of Single Sector Page-->
        <div class="sector_singlepage">
            <div class="container">
                <div class="row">
                    <!-- Start Page Description-->
                    <div class="col-xs-12 col-md-6 col-lg-8">
                        <div class="sector_singlepage-desc">
                            <div class="sector_singlepage-img">
                                <img src="{{ asset('public/Frontend/images/sector7.jpg') }}" alt="مجموعة أنظمة قطاع المدارس والمعاهد التعليمية">
                            </div>
                            <ul class="sector_block-list">
                                <li>Possibility to work as a separate school or institute and a holding group followed by a group of schools.</li>
                                <li>Determination of classes and academic years and the division of classes for each academic year.</li>
                                <li>Determining the types of curriculum.</li>
                                <li>Determine the school years expenses and discounts on rates of brothers, sons.</li>
                                <li>Determining the types of buses, the value of bus expenses and the discount of brothers.</li>
                                <li>Procedures of registration and acceptance procedures for new students and students between the classes.</li>
                                <li>Recording test dates and results of tests for students in all grades.</li>
                                <li>The possibility of attaching documents with their files within the learners system.</li>
                                <li>Print monthly reports for students and annual.</li>
                                <li>The possibility of extracting a statement of account at the level of each order is detailed at the level of children and the last complex.</li>
                                <li>Reports on the income of each stage of study and consolidated income reports at all levels of study.</li>
                                <li>Easy to communicate with parents through the system by SMS messages sent directly from the system.</li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Page Description-->

                    <!-- Start Siderbar-->
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <div class="sidebar_sectors">
                            <h3 class="sidebar_sectors-title">Private Sectors</h3>
                            <ul class="sidebar_sectors-list">
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/1') }}">Construction Sector Systems Group</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/2') }}">Maintenance and Operating Systems sector group </a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/3') }}">Group of Real Estate Investment Sector Systems and Property Management</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/4') }}">Pharmacies and Pharmaceutical Warehouses Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/5') }}">Retail and POS Systems Sector Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/6') }}">Beauty and Customization sector Systems Group</a> </li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/8') }}">Manufacturing Sector Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/9') }}">Group Of Car Rental Sector Systems</a></li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/10') }}">Transport Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/11') }}">Pest Control Systems Group</a> </li>
                                <li> <a href="{{ url(session()->get('lang').'/private-sectors/12') }}">Subsistence Sector Systems Group</a></li>
                                <li><a href="{{ url(session()->get('lang').'/private-sectors/13') }}">Charitable Sector Sector Regulations</a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Siderbar-->
                </div>
            </div>
        </div>
        <!-- End Section of Single Sector Page-->
    @endif
    @include(FE.'.pages.sectors.sector_form')
@stop