@extends(FEL.'.master')

@section('content')
    <section class="about-bg">
        <h1>{{ trans('main.site_map') }}</h1>
    </section>

    <div class="single_page sitemap_page">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <!-- Start Block 1-->
                    <aside class="sitemap_page-section">
                        <h4 class="title">{{ trans('main.home') }}</h4>
                        <ul>
                            <li><a href="{{ url(session()->get('lang')) }}">{{ trans('main.home') }}</a></li>
                            <li><a href="{{ url(session()->get('lang').'/about-us') }}">{{ trans('main.about_us') }}</a></li>
                            <li><a href="{{ url(session()->get('lang').'/news') }}">{{ trans('main.news') }}</a></li>
                            <li><a href="{{ url(session()->get('lang').'/careers') }}">{{ trans('main.careers') }}</a></li>
                            <li><a href="{{ url(session()->get('lang').'/contact-us') }}">{{ trans('main.contact_us') }}</a></li>
                        </ul>
                    </aside>
                    <!-- End Block 1-->
                </div>

                <div class="col-sm-12 col-md-6">
                    <!-- Start Block 1-->
                    <aside class="sitemap_page-section">
                        <h4 class="title">{{ trans('main.products') }}</h4>
                        <ul>
                            <li><a href="{{ url(session()->get('lang').'/products/financial') }}">{{ trans('main.financial') }}</a></li>
                            <li><a href="{{ url(session()->get('lang').'/products/distribution') }}">{{ trans('main.distribution') }}</a></li>
                            <li><a href="{{ url(session()->get('lang').'/products/human-resources') }}">{{ trans('main.human_resources') }}</a></li>
                            <li><a href="{{ url(session()->get('lang').'/products/others') }}">{{ trans('main.others') }}</a></li>
                        </ul>
                    </aside>
                    <!-- End Block 1-->
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <!-- Start Block 1-->
                    <aside class="sitemap_page-section">
                        <h4 class="title">{{ trans('main.sectors') }}</h4>
                        <ul>
                            <li><a href="{{ url(session()->get('lang').'/private-sectors') }}">{{ trans('main.private_sectors') }}</a></li>
                            <li><a href="{{ url(session()->get('lang').'/governmental-sectors') }}">{{ trans('main.governmental_sectors') }}</a></li>
                        </ul>
                    </aside>
                    <!-- End Block 1-->
                </div>
            </div>
        </div>
    </div>

@stop