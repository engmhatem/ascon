@extends(FEL.'.master')

@section('content')
    <section class="about-bg">
        <h1>{{ trans('main.careers') }}</h1>
    </section>

    @if(count($careers))
        <!-- Start  Career Page -->
        <section class="career_page">
            <div class="container">
                <h2 class="career_page-title">{{ trans('main.careers') }}</h2>
                <div class="row">
                    <div class="col-xm-12 col-md-6  col-lg-3">
                        <ul class="career_page-tabs">
                            <h3>{{ trans('main.career') }}</h3>
                            @foreach($careers as $career)
                                <li data-tab="tab-{{ $career->id }}">{{ $career->{session()->get('lang').'_title'} }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-xm-12 col-md-6 col-lg-9 ">
                        <div class="all_tabs">
                            @foreach($careers as $career)
                                <div class="all_tabs-description tab-{{ $career->id }}">
                                    <h4>{{ $career->{session()->get('lang').'_title'} }}</h4>
                                    <p>{{ $career->{session()->get('lang').'_description'} }}</p>

                                    <h4>{{ trans('main.special_requirements') }}</h4>
                                    <p>{{ $career->{session()->get('lang').'_special_requirements'} }}</p>
                                    <a href="{{ url()->current().'/'.$career->id.'/'.str_replace(' ','-',$career->{session()->get('lang').'_title'}) }}">{{ trans('main.read_more') }}</a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End  Career Page -->
    @endif
@stop

@section('js')
    <script>
        /*Career Page**/
        $('.career_page-tabs li:first').addClass('active');
        $('.all_tabs  div:first').show();
        // Add and Remove Active Class
        $(".career_page-tabs li").click(function() {
            $(this).addClass('active').siblings().removeClass('active');
        });

        //
        $(".career_page-tabs li").click(function() {
            $('.' + $(this).data('tab')).show();

            //Hide All Content
            $(".all_tabs > div").hide();

            //Show Dive With This Link
            $('.' + $(this).data('tab')).fadeIn();
        });
    </script>
@stop