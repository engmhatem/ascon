@extends(FEL.'.master')

@section('content')
    @if(!empty($career))

        <section class="about-bg">
            <h1>{{ $career->{session()->get('lang').'_title'} }}</h1>
        </section>

        <!-- Start  Career Apply Page -->
        <section class="career_apply">
            <div class="container">
                <div class="career_apply-content">
                    @if(session()->has('contact_success'))
                        <div class="alert alert-success">
                            {{ session()->get('contact_success') }}
                        </div>
                    @endif

                    {!! Form::open(['files'=>true,'url'=>'apply-career','class'=>'row']) !!}
                        <div class="col-sm-12 col-lg-6">
                            {!! Form::text('name',old('name'),['placeholder'=>trans('main.name'),'required'=>'required']) !!}
                            {!! Form::hidden('lang',session()->get('lang')) !!}
                        @if($errors->has('name'))
                                <p class="text-danger">{{ $errors->first('name') }}</p>
                            @endif
                        </div>

                        <div class="col-sm-12 col-lg-6">
                            {!! Form::text('email',old('email'),['placeholder'=>trans('main.email'),'required'=>'required']) !!}
                            @if($errors->has('email'))
                                <p class="text-danger">{{ $errors->first('email') }}</p>
                            @endif
                        </div>

                        <div class="col-sm-12 col-lg-6">
                            {!! Form::text('current_title',old('current_title'),['placeholder'=>trans('main.current_title'),'required'=>'required']) !!}
                            @if($errors->has('current_title'))
                                <p class="text-danger">{{ $errors->first('current_title') }}</p>
                            @endif
                        </div>

                        <div class="col-sm-12 col-lg-6">
                            {!! Form::number('current_salary',old('current_salary'),['min'=>0,'placeholder'=>trans('main.current_salary'),'required'=>'required']) !!}
                            @if($errors->has('current_salary'))
                                <p class="text-danger">{{ $errors->first('current_salary') }}</p>
                            @endif
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            {!! Form::date('expected_join_date',old('expected_join_date'),['placeholder'=>trans('main.expected_join_date'),'required'=>'required']) !!}
                            @if($errors->has('expected_join_date'))
                                <p class="text-danger">{{ $errors->first('expected_join_date') }}</p>
                            @endif
                        </div>


                        <div class="col-sm-12 col-lg-6">
                            {!! Form::file('CV_file',old('CV_file'),['placeholder'=>trans('main.CV_file'),'required'=>'required']) !!}
                            {!! Form::hidden('career_id',$career->id) !!}
                            @if($errors->has('CV_file'))
                                <p class="text-danger">{{ $errors->first('CV_file') }}</p>
                            @endif
                        </div>

                        <div class="col-sm-12 col-lg-12">
                            {!! Form::text('current_location_city',old('current_location_city'),['placeholder'=>trans('main.current_location_city'),'required'=>'required']) !!}
                            @if($errors->has('current_location_city'))
                                <p class="text-danger">{{ $errors->first('current_location_city') }}</p>
                            @endif
                        </div>

                        <div class="col-sm-12">
                            <button type="submit">{{ trans('main.send') }}</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </section>
        <!-- End  Career Apply Page -->
    @endif
@stop