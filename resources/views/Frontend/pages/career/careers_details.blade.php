@extends(FEL.'.master')

@section('content')
    @if(!empty($career))

        <section class="about-bg">
            <h1>{{ $career->{session()->get('lang').'_title'} }}</h1>
        </section>

        <!-- Start  Career Details Page -->
        <section class="career_details">
            <div class="container">
                <div class="career_details-content">

                    <h2 class="career_details-title">{{ $career->{session()->get('lang').'_title'} }}</h2>
                    <div class="career_details-img">
                        <img alt="{{ $career->{session()->get('lang').'_title'} }}" src="{{ $career->image }}">
                    </div>

                    <div class="career_details-date">
                        <p>{{ trans('main.publish_date')}}: <span>{{ $career->publish_date->toDateString() }}</span></p>
                        <p>{{ trans('main.close_date') }}: <span>{{ $career->close_date->toDateString() }} </span></p>
                        <p>{{ trans('main.position_code') }}: <span> {{ $career->position_code }}</span></p>

                    </div>
                    <div class="career_details-requirment requirment">
                        <h3>{{ trans('main.description') }}</h3>
                        <p>{!! $career->{session()->get('lang').'_description'} !!}</p>
                    </div>

                    <div class="career_details-description requirment">
                        <h3>{{ trans('main.special_requirements') }}</h3>
                        <p>{!! $career->{session()->get('lang').'_special_requirements'} !!}</p>
                    </div>
                    <div class="career_details-location">
                        <i><img alt="Location" src="{{ asset('public/Frontend/images/location.png') }}"></i>
                        <span>{{ $career->{session()->get('lang').'_location'} }}</span>
                    </div>
                    @if($career->close_date >= \Carbon\Carbon::now())
                        <a href="{{ url(session()->get('lang').'/careers/apply/'.$career->id.'/'.str_replace(' ','-',$career->{session()->get('lang').'_title'})) }}" class="career_details-button"> {{ trans('main.apply_now') }}</a>
                    @endif

                </div>

            </div>
        </section>
        <!-- End  Career Details Page -->
    @endif
@stop