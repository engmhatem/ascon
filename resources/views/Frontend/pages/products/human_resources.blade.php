@extends(FEL.'.master')

@section('content')

    <!--=01=Start Page Head-->
    <section class="about-bg">
        <h1>{{ trans('main.Ascon_Group_for_Human_Resources') }}</h1>
    </section>
    <!--=01=End Page Head-->

    @if(session()->get('lang') == 'ar')
        <!--Start Product Page -->
        <div class="product_page">
            <div class="container">
                <!--Start Page Content 1-->
                <div class="product_page-section">
                    <h3> مميزات نظام الموارد البشرية</h3>

                    <ul class="product_page-list">

                        <li> يتميز النظام بامكانية اضافة جميع بيانات الموظف الشخصية مثل كود الموظف – رقم الاقامة – المؤهل الدراسى الحالة الاجتماعية – تاريخ الدخول للملكة – الكفيل ....الخ .
                        </li>

                        <li> يتميز النظام بادخال العقود للموظفين وكافة بيانات العقود .</li>

                        <li> يتميز النظام بامكانية تفعيل الرتب والدرجات الوظيفية .</li>

                        <li> يتميز النظام بمرونة تصميم مستويات الهياكل الادارية وكذلك مستويات هيكل الوظائف </li>

                        <li> يتميز النظام بتسجيل ومتابعة تواريخ تجديد المستندات مثل الاقامات – رخص العمل – جواز السفر ....الخ .</li>

                        <li>يتميز النظام تحديد عناصرالتقييم السنوى للموظف ومن ثم اجراء عملية التقييم من المختصين بذلك وطباعة نتيجة التقييم لكل موظف.</li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">

                        <li> يتميز النظام بتسجيل كافة الوثائق المسلمة للشركة من طرف الموظف مثل شهادات التخرج – جواز السفر – الاقامة ..... الخ .
                        </li>

                        <li> يتميز النظام بامكانية تنقل الموظفين بين المشاريع والهياكل الادارية .</li>

                        <li> يتميز النظام بامكانية اضافة ساعات العمل الاضافى وتحديد المتغير لكل ساعة.</li>

                        <li> يتميز النظام بامكانية اضافة الجزاءات والمكافات على مستوى كل موظف .</li>

                        <li> يتميز النظام بامكانية اضافة تصريح خروج للموظف وامر اصدار اركاب.</li>

                        <li> يتميز النظام بامكانية اضافة الاعتمادات والموافقات الالكترونية لكافة الحركات على النظام .</li>
                        <li> يتميز النظام بتقارير لكافة الادخالات المرتبطة بتواريخ الانتهاء مثل اصدار الاقامة – رخص العمل – جواز السفر ...... </li>


                        <li> يتميز النظام بتعريف كافة الاستحقاقات والاستقطاعات المختلفة للموظف واضافة كافة البدلات مثل ( بدل الانتقال – بدل المعيشة – بدل الطعام –بدل الاشراف .....الخ ).</li>


                        <li> يتميز النظام بامكانية ادخال كافة السلف واثبات السدادات والتسويات على كافة السلف بشكل شهرى واستقطاعها اليا .</li>
                        <li> يتميز النظام بامكانية احتساب التامينات على مستوى الموظف واحتساب نسبة الموظف والشركة وخصمها اليا من الراتب فى نهاية كل شهر .</li>
                        <li> يتميز النظام بامكانية ادخال الزيادات الدورية على النظام مع الاحتفاظ بكافة الزيادات السابقة وامكانية الاستعلام عنها .</li>

                        <li> يتميز النظام بامكانية اثبات كافة الاجازات على الموظف حسب نوعية الاجازة مثال </li>
                        <li> ( اجازة مرضية – غياب – اجازة سنوية – اجازة مقاول ) مع امكانية خصمها اليا من الراتب .</li>

                        <li> يتميز النظام بتسجل كافة حركات التسويات النهائية مثل ( الاجازة السنوية – الاقالة – الاستقالة – نهاية الخدمة .....الخ ) مع امكانية احتساب الشق المالى الخاص بكل نوع حسب قانون مكتب العمل .</li>


                        <li> يتميز النظام بامكانية صرف بدلات السكن بكافة انواعها ( شهرى او سنوى او ربع سنوى ...الخ ) مع امكانية عمل اقفالات على مصروف بدل السكن المقدم . </li>

                        <li> يتميز النظام بامكانية ادخال المخصصات بكافة انواعها (بدل اجازة – بدل تذاكر سفر – بدل نهاية الخدمة ...الخ ) مع امكانية عمل اقفالات لكل مخصص .
                        </li>


                        <li> يتميز النظام بامكانية استخراج ملف البنك (حماية الاجور) .</li>


                        <li> يتميز النظام بوجود تنبيهات للمستخدمين عن المستندات التى ستنتهى خلال فترة .</li>


                        <li> كما يتميز النظام بوجود عدد كبير جدا من التقارير تخدم كل احتياجات الادارة ومتخذى القرار فى المؤسسة.
                        </li>

                    </ul>

                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content 1-->

                <!--Start Page Content 2-->
                <div class="product_page-section">
                    <h3>مميزات نظام الحضور والانصراف </h3>
                    <ul class="product_page-list">
                        <li>يتميز النظام بامكانية ادخال لائحة عمل لكل هيكل ادارى منفصل </li>
                        <li> يتميز النظام بامكانية ادخال مواعيد الدوام ( الحضور والانصراف ) لكل هيكل ادارى وربطها مع نظام الاجور والرواتب .</li>
                        <li>يتميز النظام بامكانية الربط مع ماكينات الحضور والانصراف . </li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">

                        <li>يتميز النظام بامكانية التحكم فى مواعيد الحضور والانصراف واحتساب التاخيرات والغيابات والانصراف المبكر ووضع معالجات محاسبية مرتبطة مع مسير رواتب الموظفين. </li>
                        <li>يتميز النظام بامكانية معالجة العمل الاضافى . </li>
                        <li>يتميز النظام بامكانية معالجة ( الغيابات – الاسئذانات – الماموريات .....الخ ) . </li>
                        <li>كما يتميز النظام بوجود عدد كبير جدا من التقارير تخدم كل احتياجات الادارة ومتخذى القرار فى المؤسسة . </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content 2-->

                <!--Start Page Content 2-->
                <div class="product_page-section">
                    <h3>مميزات نظام الخدمة الذاتية </h3>
                    <ul class="product_page-list">
                        <li> يهدف النظام الى توفير وقت تنفيذ المعاملات عن طريق قيام كل صاحب معاملة بانشائها بنفسه ومن ثم متابعتها من خلال واجهة هذا النظام .</li>
                        <li>كما يتيح هذا النظام للمستخدم امكانية تقديم الطلبات ومتابعة موقفها الحالى من الموافقات </li>
                        <li>امكانية ان يقوم الموظف بعمل طلب اجازة ومتابعة الاعتمادات التى تمت على هذا الطلب </li>
                        <li>امكانية ان يقوم الموظف بعمل طلب سلفة ومتابعة الاعتمادات التى تمت على هذا الطلب </li>
                        <li>امكانية ان يقوم الموظف بعمل طلب استئذان ومتابعة الاعتمادات التى تمت على هذا الطلب </li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">

                        <li>امكانية ان يقوم الموظف بعمل طلب استقالة ومتابعة الاعتمادات التى تمت على هذا الطلب</li>
                        <li> امكانية ان يقوم الموظف بعمل طلب مباشرة عمل ومتابعة الاعتمادات التى تمت على هذا الطلب</li>
                        <li>امكانية عمل تكليف انتداب للموظف </li>
                        <li>امكانية عمل اكثر من هيكل للاعتمادات </li>
                        <li> امكانية عمل صفحة خاصة بالموظف يتابع بها رصيد الاجازات والسلف والراتب وكل اليانت الخاصة به</li>
                        <li>امكانية عمل خطاب تعريف للموظف </li>
                        <li>امكانية ان يقوم الموظف بعمل طلب صرف تذاكر ومتابعة الاعتمادات التى تمت على هذا الطلب </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content 2-->



            </div>
        </div>
        <!--End Product Page -->
    @else
        <!--Start Product Page -->
        <div class="product_page">
            <div class="container">
                <!--Start Page Content 1-->
                <div class="product_page-section">
                    <h3>Human Resources Features System</h3>
                    <ul class="product_page-list">
                        <li>The system is characterized by the possibility of adding all the employee's personal data, such as employee code - number of residence - academic qualification - marital status - date of entry to the queen - sponsor .... etc.</li>
                        <li>The system is characterized by entering contracts for employees and all contract data.</li>
                        <li>The system is characterized by the possibility of activating the functional ranks and grades.</li>
                        <li>The system is flexible design of administrative structures and levels as well as the structure of job levels.</li>
                        <li>The system is characterized by the registration and follow-up dates of renewal of documents such as residence - work permits - passport ... etc.</li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>The system is characterized by determining the elements of the employee's annual evaluation and then conducting the evaluation process from the specialists and printing the results of the evaluation for each employee.</li>
                        <li>The system is characterized by the registration of all documents delivered to the company by the employee such as graduation certificates - passport - residence ... etc.</li>
                        <li>The system is characterized by the possibility of staff mobility between the projects and administrative structures.</li>
                        <li>The system is characterized by the possibility to add extra hours and determine the variable per hour.</li>
                        <li>The system is characterized by the possibility of adding sanctions and bonuses at the level of each employee.</li>
                        <li>The system is characterized by the possibility of adding an exit permit for the employee and ordered the issuance of boarding.</li>
                        <li>The system is characterized by the possibility of adding electronic credits and approvals for all movements on the system.</li>
                        <li>The system is characterized by the reports of all the entries associated with the dates of completion, such as the issuance of residence - work permits - Passport.</li>
                        <li>The system is characterized by the definition of all the different benefits of employee deductions and add all allowances (such as transport allowance - living allowance - food allowance allowance supervision ..... etc).</li>
                        <li>The system is characterized by the possibility of inserting all advances and proof of stoppers and adjustments on all advances monthly and deducting them automatically.</li>
                        <li>The system is characterized by the possibility of calculating the level of employee loyalty and calculating the percentage of employee and company and deducted from the salary at the end of each month.</li>
                        <li>The system is characterized by the possibility of introducing periodic increases to the system while retaining all previous increases and the possibility to query.</li>
                        <li>The system is characterized by the possibility of proving all vacations on the employee by the quality of leave such as ( sick leave - absence - an annual vacation - vacation contractor) with the possibility of salary deducted automatically.</li>
                        <li>The system records all final settlement movements such as (annual leave, dismissal, resignation, end of service, etc.) with the possibility of calculating the financial share of each type according to the Labor Law.</li>
                        <li>The system is characterized by the possibility of housing allowances for the exchange of all kinds (monthly or yearly or quarterly etc ...) with the possibility of work on closings expense housing allowance provided.</li>
                        <li>The system is characterized by the possibility of the introduction of allocations of all kinds (instead of vacation - travel allowance tickets - instead of the end of service ... etc) with the possibility of work for each custom closings.</li>
                        <li>The system is characterized by the possibility of extracting the bank file (protection of wages).</li>
                        <li>System features alerts users about the documents that will expire during the period.</li>
                        <li>The system also features a very large number of reports serve all the needs of management and decision-makers in the institution.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content 1-->

                <!--Start Page Content 2-->
                <div class="product_page-section">
                    <h3>Features Attendance System</h3>
                    <ul class="product_page-list">
                        <li>The system features the possibility of introducing a work list for each separate administrative structure.</li>
                        <li>The system is characterized by the possibility of entering the working hours (attendance and departure) of each administrative structure and linking them with the system of wages and salaries.</li>
                        <li>The system is characterized by the possibility of linking with attendance machines.</li>
                        <li>The system is characterized by the possibility to control the attendance and leaving, and calculating delays and absences and leave early and the development of associated accounting treatments with Messier employees' salaries.</li>
                        <li>The system is characterized by the possibility of additional processing work.</li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>The system is characterized by the possibility of processing (absences - excuses - commissariats ..... etc).</li>
                        <li>The system also features a very large number of reports serve all the needs of management and decision-makers in the institution.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content 2-->

                <!--Start Page Content 2-->
                <div class="product_page-section">
                    <h3>Self-Service System Features</h3>
                    <ul class="product_page-list">
                        <li>The system aims to provide the time of execution of transactions by the owner of each treatment in order to establish himself and then follow them through the interface of the system.</li>
                        <li>This system also allows the user to submit applications and follow up the current position of approvals.</li>
                        <li>The possibility that the employee's request for vacation work and follow-up credits made on this request.</li>
                        <li>Possibility of the employee to make an advance request and follow up the credits made to this request.</li>
                        <li>Possibility of the employee to make a request for authorization and follow-up credits made to this request.</li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>Possibility of the employee to make a request for resignation and follow up the appropriations made on this request.</li>
                        <li>Possibility of the employee to make a request directly to work and follow up the credits made on this request.</li>
                        <li>The possibility of the work assigned to the assignment of an employee.</li>
                        <li>The possibility of more than the structure of appropriations work.</li>
                        <li>The possibility of the work of a private employee is following the balance of vacations and salary advances and all Alliant has its own page.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content 2-->
            </div>
        </div>
        <!--End Product Page -->
    @endif
    @include(FE.'.pages.products.product_form')
@stop