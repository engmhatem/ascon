@extends(FEL.'.master')

@section('content')

    <section class="about-bg">
        <h1>{{ trans('main.Other_products_Systems_Group') }}</h1>
    </section>

    @if(session()->get('lang') == 'ar')
        <!--Start Product Page -->
        <div class="product_page">
            <div class="container">
                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3> نظام متابعة المشاريع والمستخلصات</h3>
                    <ul class="product_page-list">
                        <li> يمكن نظام مراقبة المشاريع من إدخال كافة المناقصات والمشاريع وبنود الاعمال الخاصة بها وتحديد التكاليف الخاصة بنتفيذ البنود من حيث العمالة, الأصول الثابتة, المستهلكات, النثريات
                        </li>
                        <li>يمكن تحديد بنود سداد على مستوى المشاريع وتحديد تواريخ استحقاق لكل بند ومتابعة التحصيل </li>
                        <li> ويتم متابعة تنفيذ بنود الأعمال وعمل مستخلصات مطالبة للشركات المنفذ لها ( العملاء )</li>
                        <li> يتيح البرنامج إمكانية اسناد بعض بنود الأعمال لمقاولى باطن ومتابعة تنفيذهم والدفعات المستحقة لهم واستخراج تقارير على مستوى بنود الاعمال المسندة وموقف تنفيذها</li>
                        <li> هذا ويقوم النظام بترحيل البيانات بعد إجراء العمليات المختلفة أثناء التشغيل على برامج الحسابات والعملاء الموردين بسرعة فائقة للتأثير على كشوف حساب العملاء والموردين وحسابات المشروع
                        </li>
                    </ul>

                    <h4> مزايا النظام</h4>
                    <ul class="product_page-list ">
                        <li>يمكن تحديد أرباح المناقصات واسعار بنودها وتكاليفها وتحديد العرض المناسب للعميل وكذا تاريخ بدء المناقصة وصلاحية العرض </li>
                        <li> يتيح النظام إمكانية تقسيم المناقصة إلى عدة مشاريع يتضمن كل منها بنود اعمال رئيسية وفرعية وتحديد بنود التكاليف على مستوى كل مشروع وربح المشروع وبنود السداد وتواريخ الاستحقاق</li>
                        <li> إمكانية اسناد بعض بنود مشاريع إلى مقاولى باطن وتحديد بنود السداد وتواريخ الاستحقاق</li>
                        <li>متابعة تنفيذ الاعمال من قبل مقاولى الباطن واستحقاق المستخلصات والدفعات . </li>
                        <li>متابعة تنفيذ بنود الاعمال لدى العملاء وحجم التنفيذ وتكاليف التنفيذ وترحيلها للأنظمة المرتبطة </li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li> متابعة المستخلصات المستحقة من جهات المشاريع وسداداتها وترحيلها للأنظمة المرتبطة</li>
                        <li>ارتباط النظام مع نظام الحسابات العامة ونظام احتساب ضريبة القيمة المضافة . </li>
                        <li> امكانية ﻣﻘﺎرﻧﺔ ﻋﻧﺎﺻر اﻟﺗﻛﻠﻔﺔ اﻟﻔﻌﻠﯾﺔ ﺑﺎﻟﻣوازﻧﺔ وﺑﯾﺎن اﻻﻧﺣراﻓﺎت ﻋﻠﻰ ﻣﺳﺗوى ﺑﻧود اﻷﻋﻣﺎل واﻻﺟﻣﺎﻟﻰ . </li>
                        <li> مؤشرات الاداء وفقاً ﻟﻌﻧﺎﺻر اﻟﺗﻛﻠﻔﺔ اﻟﻌﻣﺎﻟﺔ – ﻣﻘﺎوﻟﻲ اﻟﺑﺎطن – اﻟﻣﻌدات – اﻟﻣواد – اﻟﺗﻛﺎﻟﯾف اﻷﺧرى </li>
                        <li> امكانية رﻓﻊ اﻟﻣﺳﺗﺧﻠﺻﺎت ﻟﻠﺟﮭﺎت اﻻﺳﺗﺷﺎرﯾﺔ ﺛم اﻟﺟﮭﺎت اﻟﻣﺎﻟﻛﺔ ﺑﻌد اﻋﺗﻣﺎدھﺎ </li>
                        <li> امكانية ﻣﺗﺎﺑﻌﺔ دورة ﺗﺣﺻﯾل اﻟﻣﺳﺗﺧﻠﺻﺎت ﺑﺎﻟﺟﮭﺎت اﻟﺣﻛوﻣﯾﺔ </li>
                        <li> ﺗﺣﺻﯾل اﻟﻘﯾﻣﺔ اﻟﺻﺎﻓﯾﺔ ﻟﻠﻣﺳﺗﺧﻠﺻﺎت ﺑﻌد اﺳﺗﻘطﺎع اﻟﻐراﻣﺎت وﻧﺳب اﻟدﻓﻌﺔ اﻟﻣﻘدﻣﺔ </li>
                        <li> ﺑﯾﺎن ﻣﻧﻔﺻل ﺑﻣوﻗف ﻛل ﻣن اﻟدﻓﻌﺔ اﻟﻣﻘدﻣﺔ / اﻟﻣﺑﺎﻟﻎ اﻟﻣﺣﺗﺟزة / اﻟﻐراﻣﺎت واﻧواﻋﮭﺎ/اﻟﻣﺳﺗﺧﻠص ﻟﻛل ﻣﺷروع على حده وﻟﻛل اﻟﻣﺷﺎرﯾﻊ اجمالى . </li>
                        <li> ﻣﺗﺎﺑﻌﺔ ﻣﺳﺗﺧﻠﺻﺎت ﻣﻘﺎوﻟﻰ اﻟﺑﺎطن . </li>
                        <li>ﻗﯾﺎس ﻣﻌدﻻت اﻻداء ﻣن اﻟﻧﺎﺣﯾﺔ اﻟﻣﺎﻟﯾﺔ ﻟﻛل ﻣﺷروع . </li>
                        <li> ﻣﺗﺎﺑﻌﺔ اﻟﺗﻐﯾرات ﻓﻰ ﻗﯾﻣﺔ وﻣدة اﻟﻌﻘود . </li>
                        <li>ﻣﺗﺎﺑﻌﺔ ﻓﺗرات اﻟﺗوﻗف ﻟﻛل ﻣﺷروع او ﻟﻛل اﻟﻣﺷﺎرﯾﻊ . </li>
                        <li>ﺗﻘﺎرﯾر ﺗﺣﻠﯾﻠﯾﺔ ﻟﻣﺻروﻓﺎت واﯾرادات ﻛل ﻣﺷروع . </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3> نظـام متابعـة ادارة الاملاك</h3>
                    <p>تم تصميم هذا النظام للمتابعة الشاملة لكافة العقارات التي تؤجرها الشركة وذلك من خلال ربط منطقه المسـتأجر بنوع العقار بالوحده المؤجرة داخل العقار مع ربط كل هذه البيانات مع بيانات المسـتأجر وذلك للحصول فى النهاية على مجموعه من التقارير الشاملة </p>
                    <h4>مزايا النظام </h4>
                    <ul class="product_page-list">
                        <li>
                            يمكن ﻣﻦ ﺧﻼل البرنامج إدﺧﺎل وإدارة اﻷراﺿﻲ واﻟﻔﻠﻞ وإﻇﻬﺎر اﻟﺘﻘﺎرﻳﺮ الخاصة ﺑﻬﻢ، ﺗﺴﺘﻄﻴﻊ ﺗﻌﺮﻳﻒ أو استعراض المعلومات الاساسية لارض معينة كمعلومات عامة مثل الاسم – نوع الارض – رقم القطعة – عدد الشوارع – الجهة </li>
                        <li> المرونة ﰲ إدﺧﺎل اﻟﺒﻨﺎء ﺑﺸﻜﻞ ﺣﺮ وﻣﻄﺎﺑﻖ ﻟﻠﻮاﻗﻊ ﻣﻦ ناحية ﻋﺪد اﻷدوار، وﻋﺪد اﻟﺸﻘﻖ والمكاتب وﻣﻮاﻗﻒ اﻟﺴﻴﺎرات ﰲ ﻛﻞ ﻃﺎﺑﻖ ﺑﺸﻜﻞ ﺷﺠﺮى .
                        </li>
                        <li>
                            ﺗﺴﺘﻄﻴﻊ ﺗﻌﺮﻳﻒ أو اﺳﺘﻌﺮاض المعلومات اﻷﺳﺎﺳﻴﺔ ﻟﺒﻨﺎء ﻣﻌﲔ، ﻛﺎﳌﻌﻠﻮﻣﺎت اﻟﻌﺎﻣﺔ ﻣﺜﻞ اﺳﻢ اﻟﻮﺣﺪة - رقم الوحدة رقم اﻟﺸﺎرع – مساحة الوحدة – سعر المتر للوحدة اﻟﺘﻘﺎرﻳﺮ ﻟﻜﻞ ﺑﻨﺎء ﻋﻠﻰ ﺣﺪة، أو لجميع اﻷﺑﻨﻴﺔ ﺳﻮﻳﺔ .
                        </li>
                        <li> البرنامج مرتبط كلياً ببرنامج الحسابات العامة بحيث يتم ترحيل قيد خاص بكل حركه فى برنامج العقـارات . </li>
                        <li> إمكانيه تسجيل ومتابعة العهد الموجودة داخل الوحدات المؤجرة . </li>
                    </ul>


                    <ul class="product_page-list product_page-list-js">
                        <li> إمكانية متابعة المصروفات الخاصة بعقار معين أو وحده داخل العقار .</li>
                        <li>إمكانية تسجيل حركات الإيجار متضمنة بيانات الوحدة والمستأجر . </li>
                        <li>إمكانية ربط كل مسـتأجر بمحصل معين مع إعطاء إمكانيه للمستخدم بتحويل المسـتأجرين بين المحصلين . </li>
                        <li>إمكانية إدخال بيانات العقود المسجلة مع المسـتأجرين . </li>
                        <li>إمكانية الحصول على تقرير ببيانات مفصله عن المسـتأجرين من الاسم والجنسيه والمهنه ورقم الهويه… ألخ وما الى ذلك </li>
                        <li> كما يتميز نظام اﺳﻜﻮن اﻟﻌﻘﺎرى ﺑﺎﻣﻜﺎﻧﻴﺔ ﺗﻔﻌﻴﻠﻪ ﻋﻠﻰ اﺳﺎس اﳚﺎرى ﻟﻠﻮﺣﺪات اﳌﺪﺧﻠﺔ او ﻋﻠﻰ اﺳﺎس ﺑﻴﻌﻰ حيث يمكن بيع الوحدات </li>
                        <li> امكانية ﺗﻔﻌﻴﻞ اﻟﺘﻨﺒﻴﻬﺎت ﻋﻠﻰ ﻣﺴﺘﻮى ﻧﻈﺎم ادارة اﻻﻣﻼك خلال فترة محددة ﻟﻠﻌﻘﻮد المنتهية ، اﻟﺪﻓﻌﺎت المستحقة ﻟﻠﺘﺤﺼﻴﻞ ، اﻟﺘﻤﺪﻳﺪ واﻟﺘﺠﺪﻳﺪ ﻟﻠﻌﻘﻮد ، ﻋﺪد ايام اﻟﺴﻤﺎح ﰱ اﻟﺘﺠﺪﻳﺪ واﻟﺘﻤﺪﻳﺪ ﻟﻠﻌﻘﻮد .
                        </li>
                        <li> اﻣﻜﺎﻧﻴﺔ ﺗﻔﻌﻴﻞ مستويات اﻻﻋﺘﻤﺎدات ﻋﻠﻰ بعض او ﻛﺎﻓﺔ ﺷﺎﺷﺎت اﻟﻨﻈﺎم على سبيل المثال ﻓﻚ وﺿﻢ اﻟﻮﺣﺪات – انهاء العقود – اﻟﺴﺪادات – عقود اﻻﳚﺎر ...... </li>
                        <li>النظام مرتبط بشكل كامل مع نظام احتساب ضريبة القيمة المضافة </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>

                </div>
                <!--End Page Content =02=-->


                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3> نظام الوارد (اسكون الاتصالات الادارية)</h3>
                    <h4>دورة الوارد </h4>
                    <ul class="product_page-list">
                        <li> يتم إدخال بيانات المستندات الواردة حيث ياخذ كل مستند مسلسل يمثل نوع المستند والسنة والرقم المسلسل ولا يتكرر كما يقوم النظام بالتفرق بين الوارد الداخلى بين اقسام وادارات المنشأة وبين الوارد من جهات خارجية ففى حالة نوع الوارد داخلى تظهر للمستخدم هياكل الشركة الإدارية للاختيار منها أما فى حالة نوع الوارد خارجى تظهر جهات الوارد الخارجية للإختيار منها.</li>
                        <li> يتم توجيه الوارد إلى مستخدم من المستخدمين المعرفين مسبقا </li>
                        <li>
                            يقوم المستخدم باستعراض صندوق الوارد الخاص به على النظام ثم اتخاذ الاجراء المناسب بخصوص المعاملة الواردة اليه مع امكانية وضع تعليق له كما يستطيع المستخدم ارفاق اية مستندات خاصة بالمعاملة وكذلك اعادة توجيهها .
                        </li>
                        <li>امكانية الربط بين المستند او المعاملة الواردة مع معاملات صادرة سابقاً . </li>
                        <li> امكانية تتبع مسار المعاملات واتخاذ مايلزم لتسهيل انهاؤها .</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li> امكانية تمييز المعاملات حسب الاولويات ودرجات الاهمية .</li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =02=-->

                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3> نظام الصادر (اسكون الاتصالات الادارية)</h3>

                    <h4>دورة الصادر: </h4>
                    <ul class="product_page-list">
                        <li> يتم إدخال بيانات المستندات الصادرة حيث ياخذ كل مستند مسلسل يمثل نوع المستند والسنة والرقم المسلسل ولا يتكرر كما يقوم النظام بالتفرقة بين الصادر الداخلى بين اقسام وادارات المنشأة وبين الصادر الى جهات خارجية ففى حالة نوع الصادر داخلى تظهر للمستخدم هياكل الشركة الإدارية للاختيار منها أما فى حالة نوع الصادر خارجى تظهر جهات الصادر الخارجية للإختيار منها. </li>
                        <li>يتم توجيه الصادر حسب الجهة الصادر لها المعاملة ويمكن التوجيه لاكثر من جهة . </li>
                        <li>امكانية الربط بين المستند او المعاملة الصادرة مع معاملات واردة سابقاً . </li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>امكانية تتبع مسار المعاملات واتخاذ مايلزم لتسهيل انهاؤها . </li>
                        <li> امكانية تمييز المعاملات حسب الاولويات ودرجات الاهمية .</li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =02=-->

                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3>نظام الاشعارات الالكترونية </h3>

                    <ul class="product_page-list">
                        <li> الربط مع موردى خدمة الرسائل القصيرة (الاتصالات السعودية / موبايلى...)</li>
                        <li>ارسال رسائل عامة من داخل النظام كرسائل التهنئة على سبيل المثال الى ( المستخدمين / مجموعات اخرى محددة .......) . </li>
                        <li>ارسال رسائل وظيفية من نظام الموارد البشرية الى الموظفين (رواتب-مستحقات – تنبيهات ....) </li>
                        <li> ارسال رسائل وظيفية من نظام العملاء موجهة الى عملاء الشركة (الرصيد – الحد الائتمانى – العروض .....)</li>
                        <li>ارسال رسائل وظيفية من نظام الموردين موجهة الى موردى الشركة (الرصيد – سداد او تحويل المستحقات ......) </li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li> رسائل من نظام ادارة الاملاك للمستاجرين </li>
                        <li>رسائل من نظام الولاء واحتساب النقاط للعملاء </li>
                        <li> الربط مع انظمة ادارة موارد المنشاة (Ascon ERP) </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =02=-->

                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3> نظام الاشعارات الالكترونية – البريد الالكترونى </h3>
                    <ul class="product_page-list">
                        <li> الربط مع الخادم الرئيسى لتبادل المراسلات (Exchange Server)</li>
                        <li>ارسال ايميلات عامة من داخل النظام كرسائل التهنئة على سبيل المثال الى ( المستخدمين / مجموعات اخرى محددة .......) . </li>
                        <li>ارسال ايميلات وظيفية من نظام الموارد البشرية الى الموظفين (رواتب-مستحقات – تنبيهات ....)</li>
                        <li>ارسال ايميلات وظيفية من نظام العملاء موجهة الى عملاء الشركة (الرصيد – الحد الائتمانى – العروض .....) </li>
                        <li>ارسال ايميلات وظيفية من نظام الموردين موجهة الى موردى الشركة (الرصيد – سداد او تحويل المستحقات ......) </li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li> ايميلات من نظام ادارة الاملاك للمستاجرين </li>
                        <li> ايميلات من نظام الولاء واحتساب النقاط للعملاء </li>
                        <li> الربط مع انظمة ادارة موارد المنشاة (Ascon ERP)</li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =02=-->

                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3>نظام متابعة المستخدمين (على مستوى قاعدة البيانات) </h3>
                    <ul class="product_page-list">
                        <li> متابعة عمليات الادخال والتعديل والحذف بقاعدة البيانات</li>
                        <li>سجل تاريخى لكل مستخدم يتضمن كافة العمليات التى قام بها على قاعدة البيانات </li>
                        <li> امكانية تحديد الجهاز والمستخدم وكذلك القيم التى قام بادخالها او تغييرها بقاعدة البيانات مع الاحتفاظ بالقيم القديمة قبل التعديل مع امكانية استخراج تقرير مقارن يوضح القيم الجديدة بعد التعديل والقيم القديمة.</li>
                        <li> امكانية تحديد المستخدم القائم بالعملية والجهاز المستخدم والتاريخ </li>
                        <li>تقييم اداء المستخدمين من حيث عمليات الادخال والتعديل والالغاء على قاعدة البيانات
                        </li>
                    </ul>
                </div>
                <!--End Page Content =02=-->

                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3>نظام مبيعات الملابس </h3>

                    <ul class="product_page-list">
                        <li>ربط نقاط البيع بالإدارة والمخازن الرئيسية </li>
                        <li>استخدام نظام الباركود في البيع </li>
                        <li> استخدام أجهزة <bdi>Handheld </bdi> في الجرد حيث لا يتطلب وقت كبير في جرد الفرع والمخزن </li>
                        <li> إمكانية تحديد الأسعار لكل فرع أو مخزن على حدة</li>
                        <li>إمكانية تقسيم الأصناف حسب الألوان أو المقاسات أو الموديلات او الجنس او الفئة العمرية او عدة طرق مختلفة حسب كل صنف </li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>سهولة استخدام شاشة البيع من الفرع البيعى وسرعة في عمل الفاتورة </li>
                        <li> امكانية الارجاع والاستبدال من اي فرع من الفروع </li>
                        <li>امكانية الاطلاع على ارصدة المستودعات او الفروع الاخرى فى حالة وجود صلاحية للمستخدم . </li>
                        <li>ربط نظام الشبكة والفيزا كارت والماستر كارت على الفاتورة </li>
                        <li>مرونة في عمل سياسات البيع والعروض البيعية والخصومات </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =02=-->

                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3>نظام ادارة المدارس </h3>
                    <ul class="product_page-list">
                        <li>تسجيل الطلاب والفصول الدراسية </li>
                        <li> تسجيل المواد الدراسية لكل صف دراسى</li>
                        <li>رسوم التسجيل والمصاريف الدراسية لكل فصل </li>
                        <li>تسجيل الطلاب بالحافلات </li>
                        <li>رسوم حافلات نقل الطلاب </li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li> الكنترول وتحديد درجات الطلاب بالاختبارات </li>
                        <li> متابعة سداد الرسوم الدراسية وكشوف حساب لاولياء الامور</li>
                        <li> الربط مع نظام ضريبة القيمة المضافة .</li>
                        <li>تحصيل الرسوم والمصاريف الدراسية وخصومات تعدد الابناء </li>
                        <li>تسجيل نجاح الطلاب ونقلهم من صف الى صف وتقرير بدرجات الطلاب بالمواد الدراسية </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =02=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3> نظام مراقبة تكاليف الانتاج</h3>

                    <ul class="product_page-list">
                        <li>طلبات الانتاج (طلبيات عملاء / طلبات انتاج للتخزين ....) </li>
                        <li> خطة الانتاج (السنوية/الشهرية/الاسبوعية/اليومية )</li>
                        <li>طلبات توفير ال مواد وفقاً لمتطلبات خطة الانتاج </li>
                        <li>اوامر الانتاج </li>
                        <li> التكلفة التقديرية لاوامر الانتاج (عمالة /مواد/معدات/اخرى)</li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>اوامر التشغيل ,توفير المواد الخام لاوامر التشغيل </li>
                        <li> مراحل الانتاج والتكلفة الفعلية لاوامر التشغيل وتحميلها بما يخصها من التكاليف غير المباشرة</li>
                        <li>الانتاج التام وتسليمه (للعميل / للمستودعات ....) </li>
                        <li> الربط مع انظمة ادارة موارد المنشاة (Ascon ERP)</li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>نظام الورش وصيانة المعدات </h3>
                    <ul class="product_page-list">
                        <li>بيانات السيارات والمعدات الداخلة للصيانة </li>
                        <li>تحديد الاعطال والتكلفة التقديرية للتصليح </li>
                        <li>الرقابة على صرف قطع الغيار </li>
                        <li>متابعة السيارات او المعدات الداخلة للصيانة من خلال كارت السيارة او المعدة</li>
                        <li>اتخاذ قرارات استراتيجية من حيث جدوى الاستمرار فى صيانة المعدة ام استبعادها</li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>صيانة سيارات ومعدات خارجية لعملاء (ايرادات)</li>
                        <li>الربط مع انظمة ادارة موارد المنشاة (Ascon ERP)</li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3> نظام التامين ومتابعة الحوادث</h3>
                    <ul class="product_page-list">
                        <li> متابعة التعاقدات مع شركات التامين</li>
                        <li>اضافة سيارات لبوالص التامين </li>
                        <li> استبعاد سيارات من بوالص التامين</li>
                        <li>متابعة تسليم وتسلم السيارات واخلاء طرف السائقين </li>
                        <li>ادخال بيانات الحوادث وتقرير المرور </li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li> تحصيل مبالغ التعويضات المستحقة سواء من شركات التامين او افراد</li>
                        <li>متابعة المخالفات المرورية على السائقين والشركة </li>
                        <li>الربط مع انظمة ادارة موارد المنشاة (Ascon ERP) </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>نظام النقليات وتشغيل الشاحنات </h3>
                    <ul class="product_page-list">
                        <li>بيانات الشاحنات والسائقين وانواع الحمولات </li>
                        <li> جدول الاتجاهات والمسافات بين المدن واسعار الشحن</li>
                        <li>مصاريف الرحلات الاساسية وفقاً لجدول الاتجاهات والمسافات </li>
                        <li>طلبات شحن السيارات ومتابعة تحركات الشاحنات واماكن التوقف </li>
                        <li>متابعة الرحلات المغادرة والقادمة لكل فرع </li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li> شحن السيارات من والى الاتجاهات المختلفة ومتابعة تاشيرات السائقين فى حالة الشحن الدولى</li>
                        <li> اصدار الفواتير للعملاء وتسجيل الاستحقاقات</li>
                        <li>مركز ربحية لكل شاحنة ولكل سائق </li>
                        <li> متابعة حالات الشاحنات ( فى الخدمة / متوقفة / معطلة / فى رحلة /فى انتظار وصولها لفرع ...) </li>
                        <li> الربط مع انظمة ادارة موارد المنشاة (Ascon ERP)</li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3> نظام متابعة عقود الصيانة </h3>
                    <ul class="product_page-list">
                        <li>بيانات العقود وانواعها (شهرى / ربع سنوى ...) </li>
                        <li>الدفعات المستحقة وفقاً لطرق الدفع بالعقود(مقدم/مؤخر) </li>
                        <li>العقود التى ستنتهى خلال فترة </li>
                        <li>تجديد العقود </li>
                        <li>اصدار فواتير العملاء وفقاً لنوع العقد </li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li> متابعة المندوبين </li>
                        <li>شكاوى العملاء ومتابعتها </li>
                        <li>الربط مع انظمة ادارة موارد المنشاة (Ascon ERP). </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3> نظام الحضور والانصراف</h3>
                    <ul class="product_page-list">
                        <li>تعريف أوقات عمل الشركات </li>
                        <li>تعريف أوقات العمل الخاصة للشركات (مواعيد شهر رمضان ......) </li>
                        <li>سياسة احتساب التاخير وخارج الدوام </li>
                        <li>تسجيل دوامات الموظفين الفعلية من واقع (جهاز البصمة / ساعة الدوام .....) </li>
                        <li>معالجة الاذونات والحالات الخاصة </li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>الربط مع الاجور والرواتب </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3> نظام القروض</h3>
                    <ul class="product_page-list">
                        <li> امكانية تكويد كافة بينات القروض من ( الجهات الطالبة للقرض – الغرض من القرض – تكويد البنوك المرتبط بالقروض – ادخال الحدود على مستوى كل بنك على حدى – المفوضين لاتوقيعات ....... ) . </li>
                        <li>امكانية عمل طلب للقرض حسب كل جهة طالبة بالشركة . </li>
                        <li> امكانية ادخال القرض مع تحديد ( البنك – فرع البنك قيمة القرض – الغرض من القرض – قيمة القرض – كيفية السداد – مدة القرض ....... ) .</li>
                        <li>امكانية عمل سداد على مستوى كل قرض على حدى ، مع معرفة كل حد على مستوى كل قرض وعى مستوى فرع البنك والبنك الرئيسى . </li>
                        <li>امكانية تسجيل القرروض طويلة الاجل . </li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>امكانية جدولة كافة القروض حسب طريقة السداد ( شهرى – ربع سنوع – نص سنوى – سنوى ) . </li>
                        <li> امكانية تسجيل القروض المرتبطة بنظام الاعتمادات المستندية .</li>
                        <li> امكانية تسجيل القروض المرتبطة بنظام خطابات الضما ن .</li>
                        <li>امكانية تسجيل قروض وربطها على نظام المستخلصات . </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3> نظام الورش والصيانة:</h3>

                    <ul class="product_page-list">
                        <li>يتميز نظام الورش بسهولة العمل عليه وادخال كافة البيانات التى تخص العمليات داخل الورش </li>
                        <li> امكانية تسجيل بيانات العملاء وربطها بانظمة الاشعارات</li>
                        <li> امكانية تسجيل بيانات السيارات التى يتم العمل عليها فى كافة الورش حتى نتمكن من معرفة تاريخ السيارة</li>
                        <li>امكانية تسجيل الخدمات التى تقدم فى الورش وتحديد سعر وفترة الضمان لكل خدمة </li>
                        <li>امكانية تسجيل وقت دخول السيارة الاى الورشة ووقت بداية العمل عليها ووقت انتهاء العمل ووقت تسليم السيارة </li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li> امكانية عمل عرض سعر مبدئى والحصول على موافقة العميل قبل العمل بالسيارة</li>
                        <li>امكانية صرف قطع غيار على امر الشغل والحصول على موافقة واستلام الفنى لقطع الغيار </li>
                        <li>امكانية ارسال السيارة لعمل اصلاحات خارجية فى ورش اخر </li>
                        <li>امكانية توجيه امر الشغل الى ورشة اخرى تابعة لنفس الشركة مع توضيح ذلك عند احتساب الايرادات </li>
                        <li> تحديد الروافع والمعدات والفنين داخل امر الشغل</li>
                        <li>امكانية عمل اصلاحات على الاصول الخاصة بالشركة وتسجيل ذلك كمصروف او اضافة على الاصل </li>
                        <li>امكانية اصلاح سيارات الموظفين وتسجيل ذلك كسلف للعاملين </li>
                        <li>امكانية عمل اعادة اصلاح بناء على فترة الضمان مع امكانية تجاوز فترة الضمان فى بعض الحالت</li>
                        <li> امكانية تسجيل كافة بيانات قطع غيار السيارات وتسجيل القطع الملحقة والقطع البديلة </li>
                        <li> امكانية تسجيل ملاحظات على قطع الغيار حتى تساعد متخذى القرار فى عمليات الشراء والبيع القادمة </li>
                        <li> تنبيهات عند عمل مشتريات قطع غيار توضح المشتريات السابقة واوامر الشراء المتوقع استلامها حتى تساعد متخذ القرار فى عملية الشراء</li>
                        <li>تنبيهات عند عملية البيع توضح القطع البديلة والقطع الملحقة والمبيعات السابقة وملاحظات على القطع وموقع القطع بالمخزن </li>
                        <li> سهولة عملية جرد قطع الغيار على مستوى المواقع بالمخزن وعدم توقف عملية البيع خلال فترة الجرد</li>
                        <li>امكانية عمل انتاج بعض الاصناف داخل الورش </li>
                        <li>تقارير توضح حالات اوامر الشغل والفنين الذيين قامو بيها وايرادات الورش على مستوى الخدمات المقدمة وقطع الغيار والخدمات الاكثر مبيعا والقطع الاكثر مبيعا </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3> نظـام متـابعة خطابات الضمان</h3>

                    <ul class="product_page-list">
                        <li>يتميز النظام بامكانية متابعة خطابات الضمان الصادرة اوالمستلمة من الجهات الاخرى </li>
                        <li>يتميز النظام بامكانية تصنيف خطابات الضمان حسب انواعها ( ابتدائى – نهائى – دفعة مقدمة ) </li>
                        <li>ربط النظام بطريقة آلية مع جميع أنظمة أسكون وخاصه نظام الحسابات العامة والبنوك وترحيل قيود العمليات آليا ( مصاريف الاصدار والتجديد وعمولة البنك والتامين المحجوز ) </li>
                        <li>ربط النظام مع البيانات اللازمة مثل البنك والمشروع والجهة المالكة والمورد مع تحديد نوع الضمان وفترة السريان مع تحديد العمولات والتأمين المحجوز . </li>
                        <li>يتميز النظام بامكانية متابعة وتسجيل كل عمليات خطابات الضمان من اصدار – تمديد – تجديد - تسييل </li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>يتميز النظام بامكانية المتابعة لتواريخ انتهاء الضمانات وذلك باكثر من طريقة عرض حسب نوع الضمان او حسب البنك </li>
                        <li>تقارير ببيان البنوك بموقف خطابات الضمان بتاء على الحدود البنكية المسوح بها لكل بنك </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>مميزات نظام الخدمة الذاتية </h3>

                    <ul class="product_page-list">
                        <li> امكانية ان يقوم الموظف بعمل طلب اجازة ومتابعة الاعتمادات التى تمت على هذا الطلب </li>
                        <li>امكانية ان يقوم الموظف بعمل طلب سلفة ومتابعة الاعتمادات التى تمت على هذا الطلب </li>
                        <li> امكانية ان يقوم الموظف بعمل طلب استئذان ومتابعة الاعتمادات التى تمت على هذا الطلب </li>
                        <li>امكانية ان يقوم الموظف بعمل طلب استقالة ومتابعة الاعتمادات التى تمت على هذا الطلب </li>
                        <li>امكانية ان يقوم الموظف بعمل طلب مباشرة عمل ومتابعة الاعتمادات التى تمت على هذا الطلب </li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>امكانية عمل تكليف انتداب للموظف </li>
                        <li> امكانية عمل اكثر من هيكل للاعتمادات</li>
                        <li> امكانية عمل صفحة خاصة بالموظف يتابع بها رصيد الاجازات والسلف والراتب وكل اليانت الخاصة به</li>
                        <li> امكانية عمل خطاب تعريف للموظف</li>
                        <li>امكانية ان يقوم الموظف بعمل طلب صرف تذاكر ومتابعة الاعتمادات التى تمت على هذا الطلب </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3> نظـام احتساب الضريبة</h3>
                    <ul class="product_page-list">
                        <li>تعريف نوع الضريبة مع تحديد مدة الاقرار وربط الحسابات الازمة لكل نوع ضريبة </li>
                        <li> امكانية تحديد مدة الاقرار شهرى / 3 شهور</li>
                        <li> تقسيم الاصناف الى خاضعة للنسبة الاساسية / خاضعة للنسبة صفر / معفاة</li>
                        <li> تقسيم العملاء والمورديين الى خاضع للضريبة او معفاة</li>
                        <li>تقسم مناطق التوريد الى خاضع للضريبة او معفاة</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li> ربط شاشة اصدار الاقرار بجميع الانظمة والعمليات التى تمت عليها</li>
                        <li>تحليل الاقرار الضريبى الى المبيعات بتوجيهاتها والمشتريات بتوجيهاتها </li>
                        <li>ترحيل جميع العمليات من مختلف الانظمة على حسابات الضرائب المرتبطة بها </li>
                        <li>اصدار الاقرار الضريبى بنفس شكل النموذج الذى يتم تعبئته من موقع الزكاة والدخل </li>
                        <li>استخراج تقارير تحليلية على المستوى الاجمالى والتفصيل توضح عمليات الضريبة سواء المدينة والدائنة مع فصل كل تصنيف على حدى </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>نظـام النقاط والولاء </h3>

                    <ul class="product_page-list">
                        <li>يعتمد نظام النقاط على عمليات البيع المباشر بين نقطة البيع والعميل </li>
                        <li>يهدف النظام الى منح العميل حوافز لكل عملية شراء بحيث يحفز العميل للقيام بتكرار عمليات الشراء او تعظيم المشتريات لزيادة اكتساب النقاط </li>
                        <li>يقوم النظام بمنح وزن نسبى لكل صنف من قيمة النقاط المكتسبة التى بالتالى تتحول الى مبالغ فى حساب العميل ( لاختلاف ربحية كل صنف ) </li>
                        <li> فى حالة قيام العميل باكتساب النقاط يتم تسجيلها كنقاط معلقة الا ان تمر عليها مدة محددة من المستخدم ( فى الغالب مدة سماح الارجاع ) الى ان تتحول تلقائيا الى نقاط فعالة قابلة للاستبدال</li>
                        <li>يتم تحديد حد نقاط معين لاستبدال النقاط على العميل بلوغها حتى يسمح النظام باستبدال النقاط </li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>يمكن عمل شرائح لترقية النقاط كمكافاة للعميل لبلوغه حد معين فيتم مضاعفة الرصيد الحالى للعميل </li>
                        <li> تتم عمليات الاستبدال من نقاط البيع حيث يمكن استبدال كلى او جزئى للرصيد المتاح للعميل نظير قيامه بعملية شراء</li>
                        <li> يتم تسجيل عملاء النقاط برقم الهاتف حيث يعتبر عو الرقم المميز لكل عميل</li>
                        <li>
                            يتم تسجيل بعض البيانات الاخرى لكل عميل مثل النوع والعمر والحالة الاجتماعية وبيانات اخرى بالاضافة الى وجود سجل بالاصناف المهتم بها العميل بشرائها يمكن استخدام هذه البيانات فى استخراج تقارير اسقسائية لتحليل طبيعة المبيعات سواء بالفئة العمرية او النوع او استخدام وسيلة اتصال العميل لارسال العروض الترويجية </li>
                        <li>ربط النظام مع الحسابات العامة حيث يقوم بترحيل التوجيهات اللازمة لعملية اكتساب النقاط او عملية الاستبدال </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->
            </div>
        </div>
        <!--End Product Page -->
    @else
        <!--Start Product Page -->
        <div class="product_page">
            <div class="container">
                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>Project Tracking System And Abstracts</h3>
                    <ul class="product_page-list">
                        <li>The project control system can enter all tenders, projects and their business items and determine the special costs for implementing the items in terms of employment, fixed assets, and consumables.</li>
                        <li>Payment can be determined at the project level and determine the eligibility of each item and follow-up dates of collection items.</li>
                        <li>The implementation of the terms of work and the work of abstracts are carried out to claim the companies executing them.</li>
                        <li>The program allows the possibility of assigning some items of work to internal contractors and follow-up their implementation and payments owed to them and the extraction of reports on the level of the assigned items and the status of implementation.</li>
                        <li>The system relay data after conducting various operations during operation on the accounts and vendor software customers very quickly to influence the statements of customers and suppliers account and project accounts.</li>
                    </ul>

                    <h4>Advantages Of The System</h4>
                    <ul class="product_page-list ">
                        <li>The profits of tenders, the prices of their items and their costs, and the identification of the appropriate offer to the customer, as well as the date of commencement of tender and the validity of the offer.</li>
                        <li>The system allows the possibility of dividing the tender into several projects, each of which includes main and subsidiary business items, specifying the cost items at the level of each project, the profit of the project, the terms of payment and the maturity dates.</li>
                        <li>The possibility of assigning some project items to subcontractors and determining the terms of payment and due dates.</li>
                        <li>Follow-up to the implementation of works by subcontractors and maturity extracts and payments.</li>
                        <li>Follow-up to the implementation of the agenda items of the customers and the size of implementation and the costs of implementation and migration of systems associated.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>Follow-up of projects due from projects, their seals and their migration to related systems.</li>
                        <li>Link system with the system of general accounts and VAT calculation system.</li>
                        <li>The possibility of comparing the actual cost of the elements budget and the deviation of the level of work items and the total.</li>
                        <li>Performance indicators according to labor cost components - sub-contractors - equipment - materials - Other costs.</li>
                        <li>The possibility of raising the abstracts for the consultants and then the owners after their adoption.</li>
                        <li>The possibility of follow-up collection of the CAS cycle at governmental organizations.</li>
                        <li>Net recoverable amount of the extracts after deducting fines and payment rates.</li>
                        <li>A separate statement of the position of each of the payment / amounts withheld / fines and its types / extract for each project separately and each project's total.</li>
                        <li>Follow-up sub-contractors extracts.</li>
                        <li>Measure the financial performance of each project.</li>
                        <li>Follow-up changes in the value and duration of contracts.</li>
                        <li>Follow-up of downtime for each project or for all projects.</li>
                        <li>Analytical reports on expenditures and revenues of each project.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->


                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3>Property Management Monitoring System</h3>
                    <p>This system is designed for comprehensive follow-up of all properties leased by the company by linking the leased area of the type of real estate leased unit within the property and linking all these data with the tenant's data in order to finally get a set of comprehensive reports.</p>
                    <h4>Advantages Of The System</h4>

                    <ul class="product_page-list">
                        <li>Through the program you can enter and manage land and villas and show their own reports. You can define or review the basic information of a specific land as general information such as name - land type - plot number - number of streets - destination.</li>
                        <li>Flexibility in introducing free construction and identical to reality in terms of the floors number, the number of apartments, offices and parking on each floor in a form.</li>
                        <li>You can define or review basic information for a particular building, such as general information as unit name - unit number Street number - unit space - price per meter for reporting unit per building, or for all buildings together.</li>
                        <li>The program is fully linked to the General Accounts program so that a special registration for each move is posted in the real estate program.</li>
                        <li>The possibility of recording and following up the covenant within the leased units.</li>
                    </ul>


                    <ul class="product_page-list product_page-list-js">
                        <li>Possibility to track the expenses of a particular property or alone within the property.</li>
                        <li>The possibility of recording movements including rental unit and tenant data.</li>
                        <li>The possibility of linking each tenant to a specific recipient, giving the user the possibility to transfer the tenants among the collectors.</li>
                        <li>The possibility of registering contracts with tenants data entry.</li>
                        <li>The possibility of obtaining a report with detailed data on the tenants of the name, nationality, profession, ID number ... etc.</li>
                        <li>Ascon real estate system is characterized by the possibility to activate it on a rental basis for the units entered or on a sale basis where the units can be sold.</li>
                        <li>The possibility of activating alerts on the level of property management system during a specific period of contracts ended, payments due for collection, extension and renewal of contracts, the number of days allowed in the renewal and extension of contracts.</li>
                        <li>The possibility of activating the levels of credits on some or all system screens, for example, included decoding units - end contracts - Stoppers - rent contracts.</li>
                        <li>The system is fully linked to the VAT calculation system.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =02=-->

                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3>Inbound Cycle</h3>
                    <h4>دورة الوارد </h4>
                    <ul class="product_page-list">
                        <li>The data of the incoming documents is entered where each document takes a series representing the document type, the year and the serial number, and it can not be repeated. The system also distinguishes between the internal input between the departments, the establishment department and the incoming from third parties. In the case of the type of internal, the user shows the administrative structure of the company to choose from. Outbound External inboxes appear to select from.</li>
                        <li>Inbox is directed to a user from predefined users.</li>
                        <li>The user browses his / her inbox on the system and then takes appropriate action regarding the incoming transaction with the possibility of commenting, and the user can attach and redirect any transaction-specific documents.</li>
                        <li>The possibility of linking the document or the transaction received with previously issued transactions.</li>
                        <li>The possibility of tracking transactions and taking the necessary to facilitate their termination.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>The ability to distinguish transactions according to priorities and degrees of importance.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =02=-->


                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3>Outgoing System (Ascon Administrative Communications)</h3>

                    <h4>Outbound Course</h4>
                    <ul class="product_page-list">
                        <li>Data is entered in the outgoing documents where each document takes a serial representing the document type and the year and serial number is not repeated as the system distinguishes between the internal output between the departments and departments of the establishment and the issuance to external parties in the case of the type of the internal statement showing the user structures of the company to choose from them, Externally, external issuers appear to choose from.</li>
                        <li>The outgoing is routed by the issuing entity and the transaction can be routed to more than one destination.</li>
                        <li>The possibility of linking document or outbound transaction with incoming earlier transactions.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>The possibility of tracking transactions and taking the necessary to facilitate their termination.</li>
                        <li>The ability to distinguish transactions according to priorities and degrees of importance.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>

                </div>
                <!--End Page Content =02=-->


                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3>Electronic Notification System</h3>

                    <ul class="product_page-list">
                        <li>Linking with SMS service providers (STC / Mobily ...).</li>
                        <li>Send public messages from within the system as greeting messages to (other users / specific groups .......).</li>
                        <li>Sending functional messages from HR system to employees (salaries - dues - alerts ....).</li>
                        <li>Send functional messages from customer-oriented system to the company's customers (credit - credit limit - offers .....).</li>
                        <li>Send functional messages from suppliers addressed to the company's suppliers system (balance - payment or transfer of receivables ......).</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>Messages from the property management system for tenants.</li>
                        <li>Messages from the loyalty system and score points for customers.</li>
                        <li>Linkage with systems facility resources management (Ascon ERP).</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =02=-->


                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3>Electronic Notification System - Email</h3>
                    <ul class="product_page-list">
                        <li>Connecting to the primary Exchange Server.</li>
                        <li>Send public emails from the system, such as greeting messages (eg, other specific users / groups).</li>
                        <li>Sending functional emails from HR system to employees (salaries, benefits, alerts, etc.)</li>
                        <li>Send functional customer-oriented emails to the company's customers (credit - credit limit - offers .....).</li>
                        <li>Sending professional emails from the supplier system addressed to the company's suppliers (balance - payment or transfer of receivables ......).</li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>Emails from the property management system for traders.</li>
                        <li>Emails from the loyalty system and calculate the points for customers.</li>
                        <li>Linkage with systems facility resources management (Ascon ERP).</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =02=-->


                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3>User Tracking System(On the database level)</h3>

                    <ul class="product_page-list">
                        <li>Follow-up input, modification and deletion database operations.</li>
                        <li>Historical record for each user that includes all operations carried out on the database.</li>
                        <li>The possibility of specifying the device and the user as well as the values entered or changed on the database while retaining the old values before the amendment with the possibility of extracting a comparative report showing the new values after the amendment and the old values.</li>
                        <li>The possibility of specifying the user using the process, the device used and the date.</li>
                        <li>Evaluate the performance of users in terms of the processes of entry, modification and cancellation on the database.</li>
                    </ul>
                </div>
                <!--End Page Content =02=-->

                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3>Clothing Sales System</h3>

                    <ul class="product_page-list">
                        <li>Linking POS management and main stores.</li>
                        <li>Use the barcode system in the sale.</li>
                        <li>Use Handheld devices in the inventory, where time does not require a large inventory branch and store.</li>
                        <li>The possibility of setting prices for each branch or store separately.</li>
                        <li>The possibility of dividing categories according to colors, sizes, models, sex, age group or several different methods according to each category.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>Easy to use screen sales of the branch and the selling speed in the work of the bill.</li>
                        <li>Possibility of replacement and Returns any branch of the branches.</li>
                        <li>Access to the assets of warehouses or other branches in case of the validity of the user.</li>
                        <li>Connect the network system and the Visa card and MasterCard on the invoice.</li>
                        <li>Flexibility in making sales policies, sales offers and discounts.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =02=-->


                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3>School Management System</h3>
                    <ul class="product_page-list">
                        <li>Students and classroom registration.</li>
                        <li>Registration of subjects for each grade.</li>
                        <li>Registration fees and tuition fees for each semester.</li>
                        <li>Registration of students in bus.</li>
                        <li>Student bus transfer fee.</li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>Control Unit and identify students testing grades.</li>
                        <li>Follow-up payment of tuition fees and account statements for the parents.</li>
                        <li>Linkage with VAT system.</li>
                        <li>Collection of fees and expenses and tuition discounts of multiple children.</li>
                        <li>Record students' success and move them from grade to grade and report student grades.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =02=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>Control System Of Production Costs</h3>
                    <ul class="product_page-list">
                        <li>Production orders (customer orders / production requests for storage ...).</li>
                        <li>Production plan (annual / monthly / weekly / daily).</li>
                        <li>Applications for the supply of materials according to the requirements of the production plan.</li>
                        <li>Production orders.</li>
                        <li>Estimated cost of production orders (Labor / Material / Equipment / Other).</li>
                        <ul class="product_page-list product_page-list-js">
                            <li>Operating orders, provide raw materials for operating orders.</li>
                            <li>Stages of production and the actual cost of operating orders and upload its own costs, including indirect.</li>
                            <li>Full production and delivery (for the client / warehouse ...).</li>
                            <li>Linkage with systems facility resources management (Ascon ERP).</li>
                        </ul>
                        <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                        <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                        <div class="clear"></div>
                    </ul>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>Workshops And Equipment Maintenance System</h3>

                    <ul class="product_page-list">
                        <li>Data of vehicles and equipment entering maintenance.</li>
                        <li>Identifying faults and the estimated cost of repair.</li>
                        <li>Control over the exchange of spare parts.</li>
                        <li>Follow up the cars or equipment entering for maintenance through the car or the card.</li>
                        <li>Making strategic decisions in terms of the feasibility of continuing maintenance of the equipment or excluding it.</li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>Maintenance of vehicles and external equipment for customers (revenues).</li>
                        <li>Linkage with systems facility resources management (Ascon ERP).</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>Insurance And Accident Tracking System</h3>
                    <ul class="product_page-list">
                        <li>Follow-up contracts with insurance companies.</li>
                        <li>Add cars to insurance policies.</li>
                        <li>Exclude cars from insurance policies.</li>
                        <li>Follow-up delivery and receipt of cars and evacuation of drivers.</li>
                        <li>The introduction of accident data and traffic report.</li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>The collection of compensation amounts due from both insurance companies or individuals.</li>
                        <li>Follow up traffic violations on drivers and company.</li>
                        <li>Linkage with systems facility resources management (Ascon ERP).</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>Transportation System And Operation Of Trucks </h3>
                    <ul class="product_page-list">
                        <li>Data of trucks, drivers and types of cargo.</li>
                        <li>Table of directions, inter-city distances and freight rates.</li>
                        <li>Basic travel expenses according to schedule of directions and distances.</li>
                        <li>Shipping orders for vehicles and follow-up movements of trucks and parking spaces.</li>
                        <li>Follow-up of departing and arriving flights in each branch.</li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>Shipping cars to and from different directions and follow-up visa drivers in the case of international shipping.</li>
                        <li>Issue invoices to customers and record benefits.</li>
                        <li>Profit center for each truck and per driver.</li>
                        <li>Follow-up cases of trucks (in service / parked / Off / on a trip / waiting for the arrival of the branch ...).</li>
                        <li>Linkage with systems facility resources management (Ascon ERP).</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->


                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>Maintenance Contracts Follow-up System</h3>
                    <ul class="product_page-list">
                        <li>Contract data and types (monthly / quarterly).</li>
                        <li>Payments due according to contract payment method (prepayment).</li>
                        <li>Contracts that will expire during the period.</li>
                        <li>Renewal of contracts.</li>
                        <li>Issuing customer invoices according to the type of contract.</li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>Follow-up of delegates.</li>
                        <li>Customer complaints and follow-up.</li>
                        <li>Linkage with systems facility resources management (Ascon ERP).</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>Attendance System</h3>
                    <ul class="product_page-list">
                        <li>Definition of business hours of companies.</li>
                        <li>Defining business hours for companies (Ramadan dates ... ...).</li>
                        <li>Calculating the delay and off-duty policy.</li>
                        <li>Record the actual staff swirls from reality (fingerprint / clock ... ...).</li>
                        <li>Processing of special permissions and cases.</li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>Linkage with wages and salaries.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>Loans System</h3>
                    <ul class="product_page-list">
                        <li>The possibility of coding all the loan evidence from (the loan authorities - the purpose of the loan - the coding of the banks associated with loans - the introduction of the limits at the level of each bank alone - the commissioners of signatures ... ...).</li>
                        <li>The possibility of making a request for the loan according to each applicant company.</li>
                        <li>The possibility of introducing the loan with the identification (bank - bank branch loan value - the purpose of the loan - the value of the loan - how to pay - the term of the loan ... ....).</li>
                        <li>The possibility of repayment of the work at the level of each loan separately, with the knowledge of each extent on the level of each loan level of awareness of the main branch of the bank and the bank.</li>
                        <li>The possibility of registering long term loans.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>The possibility of scheduling all loans according to the method of payment (monthly - quarter of the year - annual text - annual).</li>
                        <li>Associated with the possibility of recording documentary credits system loans.</li>
                        <li>The possibility of recording associated with the letters of guarantee system loans.</li>
                        <li>The possibility of registration of loans and linked to the CAS system.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>Workshop And Maintenance System</h3>

                    <ul class="product_page-list">
                        <li>Workshops system is easy to work on it and enter all the data pertaining to operations inside the workshops.</li>
                        <li>The possibility of recording customer data and linking it to the notification systems.</li>
                        <li>The possibility of recording the data of cars that are working in all workshops so that we can know the history of the car.</li>
                        <li>The possibility of recording the services provided in the workshops and determining the price and warranty period for each service.</li>
                        <li>The possibility of recording the time of entering the car and the time of starting work on it and the time of completion of work and the time of delivery of the car.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>The possibility of making an initial price offer and obtaining the customer's approval before working on the car.</li>
                        <li>The possibility of exchange parts is on the job and get the approval and receipt of technical spare parts.</li>
                        <li>The possibility of sending the car to make external repairs in other workshops.</li>
                        <li>The possibility of directing the work order to another workshop belonging to the same company, with clarification of this when calculating revenues.</li>
                        <li>Determination of cranes, equipment and technicians within the work order.</li>
                        <li>The possibility of making repairs on the assets of the company and registering it as an expense or adding it to the original.</li>
                        <li>The possibility of repairing the cars of employees and recording this as an advance from their salaries.</li>
                        <li>The possibility of re-repair work based on the warranty period with the possibility of exceeding the warranty period in some cases.</li>
                        <li>The possibility of recording all data of car parts and recording of spare parts and spare parts.</li>
                        <li>Ability to record notes on spare parts to assist decision-makers in future purchases and sales.</li>
                        <li>Alerts when making spare parts purchases showing previous purchases and expected purchase orders to assist the decision maker in the purchase process.</li>
                        <li>Alerts when sales process describes the alternative pieces and the pieces attached to sales and previous notes on the pieces and the location of the pieces warehouse.</li>
                        <li>Easy inventory of spare parts at the level of sites and not to sell the warehouse process stops during the inventory process.</li>
                        <li>The possibility of producing some of the items within the workshops.</li>
                        <li>Reports showing the status of work orders, technicians, and workshop revenues on the level of services provided, spare parts, best-selling services and best-selling parts.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>System Of Follow-up Letters Of Guarantee</h3>

                    <ul class="product_page-list">
                        <li>The system is characterized by the possibility of following letters of guarantee issued or received from other parties.</li>
                        <li>The system is characterized by the possibility of classifying letters of guarantee by type (primary, final, advance payment).</li>
                        <li>Connect the system automatically with all Ascon systems, especially the system of general accounts and banks and automatically transfer process restrictions (issuance expenses, renewal, bank commission and insurance reserved).</li>
                        <li>Link the system with the necessary data such as bank, project, the owner and the supplier, specifying the type of guarantee and the period of validity with the specified commissions and insurance reserved.</li>
                        <li>The system is characterized by the possibility of tracking and recording all letters of guarantee operations from issue - extension - renewal - liquidation.</li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>The system is characterized by the possibility of follow-up to expiration dates and guarantees that more of a view by type of security or by the Bank.</li>
                        <li>Reports of banks' statements regarding the position of the letters of guarantee on the bank boundaries surveyed for each bank.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>Self-service System Features</h3>
                    <ul class="product_page-list">
                        <li>The possibility that the employee's request for vacation work and follow-up credits made on this request.</li>
                        <li>Possibility of the employee to make an advance request and follow up the credits made to this request.</li>
                        <li>Possibility of the employee to make a request for authorization and follow-up credits made to this request.</li>
                        <li>The possibility that the employee's work and request the resignation of the follow-up credits made on this request.</li>
                        <li>Possibility of the employee to make a request directly to work and follow up the credits made on this request.</li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>The possibility of a mandate assignment to the employer.</li>
                        <li>The possibility of more than the structure of appropriations work.</li>
                        <li>The possibility of creating a special page for the employee to follow up the balance of the leaves, advances, salary and all of its entitlements.</li>
                        <li>The ability to make a speech definition of an employee.</li>
                        <li>The possibility that the employee's job request exchange tickets and follow-up credits made on this request.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>Tax Calculation System</h3>
                    <ul class="product_page-list">
                        <li>Definition of the type of tax with determining the duration of approval and accounts linking the crisis for each tax type.</li>
                        <li>The possibility of determining the duration of a monthly declaration / 3 months</li>
                        <li>The division of items subject to the basic ratio / subject to the ratio of zero / exempt.</li>
                        <li>Divide customers and suppliers into taxable or exempt.</li>
                        <li>The supply areas shall be divided into taxable or exempt.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>Linking the issuance of declaration of all systems and processes that were on display</li>
                        <li>Analysis of tax recognition to sales under its direction and procurement under its guidance.</li>
                        <li>Deport all the operations of the various systems on the associated tax accounts.</li>
                        <li>Issuing tax declaration form of the same model, which is filled from the site of Zakat and income.</li>
                        <li>Extraction of analytical reports on the overall level of detail and clarify whether the tax debit and credit operations separating each category separately.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>Points And Loyalty System</h3>

                    <ul class="product_page-list">
                        <li>Points system depends on direct sales between point of sale and customer.</li>
                        <li>The system aims to grant incentives to the customer for each purchase so that the customer to do stimulates repeat purchases or optimize purchases to increase the acquisition of points.</li>
                        <li>The system assigns a relative weight to each item of the value of the acquired points, which are then converted into amounts in the customer's account (for the different profitability of each item).</li>
                        <li>In the case of a customer gaining points, they are recorded as outstanding points but have a specified period of time (usually the return grace period) until they are automatically converted to redeemable points.</li>
                        <li>A specific score limit is set to replace the points on the client until they are allowed to replace points.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>Slices can work to upgrade reward points for reaching the client to a certain extent are doubling the current balance of the customer.</li>
                        <li>Repurchases are made from points of sale where a partial or total replacement of the balance available to the customer for a purchase can be made.</li>
                        <li>Points are registered clients where the phone number is a unique number for each Ao client.</li>
                        <li>Some other data is recorded for each client such as type, age, social status and other data. In addition to a record of the items the customer is interested in purchasing, this data can be used to extract reports to analyze the nature of the sales, whether in the age group or type.</li>
                        <li>Linking the system with public accounts where relay the necessary guidance to the process of acquiring points or replacement process.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->
            </div>
        </div>
        <!--End Product Page -->
    @endif
    @include(FE.'.pages.products.product_form')
@stop