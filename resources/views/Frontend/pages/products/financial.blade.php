@extends(FEL.'.master')

@section('content')

    <!--=01=Start Page Head-->
    <section class="about-bg">
        <h1>{{ trans('main.ascon_financial_group') }}</h1>
    </section>
    <!--=01=End Page Head-->

    @if(session()->get('lang') == 'ar')
        <!--Start Product Page -->
        <div class="product_page">
            <div class="container">
                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3> مميزات نظام أسكون للحسابات العامة</h3>
                    <ul class="product_page-list">
                        <li>يتميز النظام بامكانية التعامل بأكثر من لغة على مستوى كل من الشاشات والتقارير . </li>
                        <li>يتميز النظام بتصميم يلائم العمل على النظام متعدد الفروع ومتعدد الشركات على نفس قاعدة البيانات . </li>
                        <li>يتميز النظام بامكانية عمل دليل الحسابات ومراكز التكلفة فى شكل شجرى متعدد المستويات. </li>
                        <li>يتميز النظام بامكانية العمل بعملة واحدة او عملات متعددة مع تحقيق مرونة تغيير سعر صرف العملات الأجنبية. </li>
                        <li> يتميز النظام بدعمه للعمل باى من النظم المتعارف عليها باليوميات (نظام اليومية الامريكية ، اليومية الفرنسية ....) .</li>
                        <li>يتميز النظام بسهولة عمل قيد الإقفـال آلياً وكذلك ترحيل الارصدة الافتتاحية لحسابات الميزانية آليـا مع نهـاية كل فتـرة محاسبيـة. </li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>يتميز النظام بإمكانيـة إدخال الأرصدة التقديرية للحسابات ومقارنتـها بالأرصدة الفعليـة شهريا وسنـويا كما يمكن المقارنة بين الفعلى والتقديرى مع الادخالات اليومية للقيود المحاسبية مع بيان الانحراف بين الأرصـدة الفعليـة والتقديريـة. </li>
                        <li>يتميز النظام بامكانية التوزيع على مراكز التكلفه يدويا أثناء أدخال القيد أو آليا بنسب يتم تعريفها مسبقا بواسطه المستخدم . </li>
                        <li> يتميز النظام بخاصيـة الاستعـلام عـن رصيـد الحسـابـات قبـل وبعـد الترحيـل إلـي الأستـاذ العـام .</li>
                        <li> يتميز النظام بامكانية عمل قيود دورية فى حالة وجود مدخلات متكررة دورياً .</li>
                        <li>يتميز النظام بالربط الكامل بين الحسابات العامه وباقى الانظمه بحيث يمكن للمستخدم الاطلاع على القيود من النظام الفرعى أو من نظام الحسابات العامة. </li>
                        <li> يتميز النظام بسهولة تحليل الحسابات (مصروفات/ايرادات) على مستوى مراكز التكلفة والشهور .</li>
                        <li> يتميز النظام بسهولة البحث والاستعلام بحيث يمكن البحث عن أي مبلغ أو اسم تم إدخاله بقيود في الحساب أو اعطاء تقرير يوضح القيود التي بها المبلغ أو الاسم المراد البحث عنه .</li>
                        <li> يتميز النظام بوجود عدد كبير من التقارير التى تخدم كل احتياجات الادارة المالية ومتخذى القرار فى المؤسسة.</li>
                        <li> يتميز النظام بامكانية استخراج جميع التقارير بصيغة PDF وامكانية تصدير جميع التقارير على الاكسيل .</li>
                        <li> يتميز النظام بامكانية العمل علية من خلال الشبكة الداخلية او الدخول عن طريق الانترنت حيث ان النظام web-enabled</li>
                    </ul>
                    <span class="product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3> مميزات نظام أسكون للأصول الثابتة</h3>
                    <ul class="product_page-list">
                        <li>يتميز النظام بامكانية تصنيف الاصول الى ( اصل – عهدة – اصل عهدة ) . </li>
                        <li> يتميز النظام بامكانية تقسيم الأصول الثابتة إلى أنواع ومجموعات مختلفة بشكل شجرى .</li>
                        <li>يتميز النظام بتسجيل كافة بيانات الأصول الثابتة مثل رقم الأصل ، باركود الاصل، مسمى الاصل ، الوصف ، الهيكل الادارى للأصل المتواجد به الاصل , تاريخ الشراء , قيمة الشراء ، نسبة الاهلاك...... </li>
                        <li>يتميز النظام بامكانية تحديد تاريخ بداية اهلاك الاصل الثابت, العمر الافتراضى , ترحيل اهلاكات الأصول الى الحسابات العامة.</li>
                        <li>يتميز النظام بسهولة معالجة العمليات التى يتم اجراؤها على الأصل من شراء ، اضافة جزئية ، إهلاك ، استبعاد كلى ، استبعاد جزئى .</li>
                        <li>يتميز النظام بامكانية نقل الأصل من عهدة موظف الى موظف أخر. </li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>يتميز النظام بامكانية نقل الأصل من هيكل ادارى الى هيكل ادارى اخر وتحميل كل هيكل بما يخصة من الاهلاكات آلياً وفقاً للفترة الزمنية المتواجد فيها الاصل بكل هيكل . </li>
                        <li>كما يتميز النظام بامكانية عمل اضافات جزئية على الأصول . </li>
                        <li>يتميز النظام بسهولة حساب اهلاكات الأصول وترحيل قيود الاهلاك الى الحسابات العامة . </li>
                        <li> يتميز النظام بالتوافق مع نظام ضريبة القيمة المضافة .</li>
                        <li> كما يتميز النظام بوجود عدد كبير من التقارير التى تخدم احتياجات الادارة ومتخذى القرار فى المؤسسة.</li>
                        <li> يتميز النظام بامكانية استخراج جميع التقارير بصيغة PDF وامكانية تصدير جميع التقارير على الاكسيل .</li>
                        <li>يتميز النظام بامكانية العمل علية من خلال الشبكة الداخلية او الدخول عن طريق الانترنت حيث ان النظام web-enabled </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>

                </div>
                <!--End Page Content =02=-->

                <!--Start Page Content =03=-->
                <div class="product_page-section">
                    <h3> مميزات نظام أسكون لمتابعة العملاء</h3>

                    <ul class="product_page-list">
                        <li>يتميز النظام بامكانية هيكلة العملاء بشكل شجرى . </li>
                        <li>يتميز النظام بتسجيل كافة بيانات العملاء الشخصية والمالية و الضريبيه والبيعيه وكذلك امكانية ربط المندوب مع العميل . </li>
                        <li>يتميز النظام بعمل ربط العميل بنوعيه و نشاط و قطاع بيعى و منطقه. </li>
                        <li>يتميز النظام بامكانية تحديد الحد الإئتماني لكل عميل وكذلك فترات السماح . </li>
                        <li>يتميز النظام بامكانية استثناءات لتجاوز الحدود الائتمانية للعملاء بصلاحيات معينة مع الاحتفاظ بالنظام بالسجل التاريخى لاستثناءات كل عميل وبيان سبب الاستثناء والقائم بمنح الاستثناء .</li>
                        <li>يتميز النظام بامكانية تقسيم عملاء الشركة الى فئات وتحديد سياسات تعامل مختلفة لكل مجموعة عملاء . </li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>يتميز النظام بامكانية عمل مستهدف لمشتريات العميل من الاصناف ومجموعات الاصناف وكذلك المستهدف للمتحصلات من العميل . </li>
                        <li>يتميز النظام بامكانية عمل مستهدف لمبيعات المندوبين من الاصناف على مستوى كل عميل . </li>
                        <li> يتميز النظام بتحقيق الرقابة على مديونية العملاء وأرصدة المندوبين .</li>
                        <li>يتميز النظام بامكانية تعامل العميل مع أكثر من مندوب وتعامل المندوب مع أكثر من عميل . </li>
                        <li> يتميز النظام بتحديد المستهدف للمندوبين وفقا للفترات التى تحددها الادارة (شهرى او ربع سنوى) .</li>
                        <li> يتميز النظام بامكانية توزيع العملاء على المناطق والفروع .</li>
                        <li> يتميز النظام بامكانية اعداد جداول متنوعة لاحتساب اعمر الديون يمكن للمستخدم استخراج تقرير اعمار الديون باى منها </li>
                        <li>يتميز النظام بامكانية اعداد خطوط السير للمندوبين وكذلك خطة الزيارات اليومية والاسبوعية .</li>
                        <li>يتميز النظام بامكانية متابعة نتائج زيارات المندوبين ومعرفة الزيارات الايجابية والناجحة مقارنة بالزيارات غير الناجحة.</li>
                        <li>يتميز النظام بامكانية متابعة ارصدة العملاء وحركات السداد من العملاء .</li>
                        <li>يتميز النظام بامكانية تسجيل شكاوى العملاء واستخراج تقارير بها لمعالجتها .</li>
                        <li>يتميز النظام بالربط مع نظام الولاء واحتساب النقاط .</li>
                        <li>يتميز النظام بالربط مع نظام ضريبة القيمة المضافة .</li>
                        <li>كما يتميز النظام بوجود عدد كبير من التقارير التى تخدم احتياجات الادارة ومتخذى القرار فى المؤسسة.</li>
                        <li>يتميز النظام بامكانية استخراج جميع التقارير بصيغة PDF وامكانية تصدير جميع التقارير على الاكسيل .</li>
                        <li>يتميز النظام بامكانية العمل علية من خلال الشبكة الداخلية او الدخول عن طريق الانترنت حيث ان النظام web-enabled .</li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =03=-->

                <!--Start Page Content =04=-->
                <div class="product_page-section">
                    <h3>مميزات نظام أسكون لمتابعة الموردين </h3>
                    <ul class="product_page-list">
                        <li>يتميز النظام بامكانية هيكلة الموردين بشكل شجرى . </li>
                        <li>يتميز النظام بتسجيل كافة بيانات الموردين الشخصية والمالية و الضريبيه ه وكذلك امكانية ربط المندوب مع المورد . </li>
                        <li> يتميز النظام بامكانية تحديد نوع ونشاط المورد وربطه مع المنطقة الرئيسية والفرعية .</li>
                        <li>يتميز النظام بامكانية تحديد الحد الإئتماني لكل مورد وكذلك فترات السماح . </li>
                        <li>امكانية ستخراج تقرير باعمار الديون لكل مورد . </li>
                        <li>امكانية التفرقة بالنظام بين الموردين ومقاولى الباطن . </li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>يتميز النظام بامكانية تصنيف للموردين الى قطاعات مختلفة. </li>
                        <li>يتميز النظام بامكانية تسجيل مندوبين المشتريات المسئولين فى التعامل مع الموردين. </li>
                        <li>يتميز النظام بامكانية التفرقة بين رصيد المورد من الدفعات المقدمة والدفعات المستحقة وكذلك المبالغ المحتجزة مع امكانية استخراج كشف حساب لكل نوع دفعة او كشف حساب مجمع للمورد . </li>
                        <li>يتميز النظام بتسجيل كافة بيانات الموردين الشخصية والمالية وطرق التعامل مع الموردين مع تسجيل المندوب الخاص بالمورد . </li>
                        <li>يتميز النظام بتحديث ارصدة الموردين الدائنة و المدينة وفقا لحركات المشتريات ومردوداتها من نظام المشتريات . </li>
                        <li>يتيمز النظام بامكانية السداد للموردين من الصندوق او من البنك او من خلال الحسابات. </li>
                        <li>يتميز النظام بالربط مع نظام ضريبة القيمة المضافة . </li>
                        <li> كما يتميز النظام بوجود عدد كبير من التقارير التى تخدم احتياجات الادارة ومتخذى القرار فى المؤسسة.</li>
                        <li> يتميز النظام بامكانية استخراج جميع التقارير بصيغة PDF وامكانية تصدير جميع التقارير على الاكسيل .</li>
                        <li>يتميز النظام بامكانية العمل علية من خلال الشبكة الداخلية او الدخول عن طريق الانترنت حيث ان النظام web-enabled . </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =04=-->

                <!--Start Page Content =05=-->
                <div class="product_page-section">
                    <h3> مميزات نظام أسكون للمقبوضات والمدفوعات</h3>
                    <ul class="product_page-list">
                        <li>يتميز النظام بامكانية اضافة الصناديق والخزائن الخاصة بالشركة وربطها بالحسابات. </li>
                        <li>يتميز النظام بتسجيل دفاتر سندات الصرف والقبض التى يتم استخدامها. </li>
                        <li> يتميز النظام بتسجيل المستفيدين الذين يتم التعامل معهم مباشرة من الصندوق دون وجود نظام فرعى .</li>
                        <li>يتميز النظام بتسجيل البنود النقدية التى يتم التعامل بها مباشرة من الصندوق دون وجود نظام فرعى . </li>
                        <li>يتميز النظام بامكانية الغاء سندات الصرف والقبض مع الاحتفاظ بها كسندات ملغاة ولا يتم حذفها لدقة عملية المراجعة. </li>
                        <li>يتميز النظام بامكانية التحويل من الصندوق الى صندوق اخر . </li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li> يتميز النظام بامكانية التحويل من الصندوق الى البنك.</li>
                        <li>يتميز النظام بالربط الكامل مع أنظمة العملاء والموردين والأصول الثابتة والموارد البشرية والمستخلصات. </li>
                        <li> يتميز النظام بامكانية اضافة مراكز التكلفة عند عمل سندات الصرف والقبض.</li>
                        <li>يتميز النظام بامكانية طباعة سندات الصرف والقبض. </li>
                        <li>يتميز النظام بتنبيه المستخدم للحركات المعلقة التى يجب قبضها او دفعها من الأنظمة الأخرى. </li>
                        <li> يتميز النظام بالربط مع نظام ضريبة القيمة المضافة .</li>
                        <li> كما يتميز النظام بوجود عدد كبير من التقارير التى تخدم احتياجات الادارة ومتخذى القرار فى المؤسسة.</li>
                        <li> يتميز النظام بامكانية استخراج جميع التقارير بصيغة PDF وامكانية تصدير جميع التقارير على الاكسيل .</li>
                        <li>يتميز النظام بامكانية العمل علية من خلال الشبكة الداخلية او الدخول عن طريق الانترنت حيث ان النظام web-enabled . </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>

                </div>
                <!--End Page Content =05=-->

                <!--Start Page Content =06=-->
                <div class="product_page-section">
                    <h3>مميزات نظام أسكون لطباعة الشيكات </h3>
                    <ul class="product_page-list">
                        <li> يتميز النظام بامكانية اضافة كل البنوك الخاصة بالشركة وفروع هذه البنوك وربطها بالحسابات.</li>
                        <li> يتميز النظام بامكانية تسجيل دفاتر الشيكات الخاصة بالشركة وكذلك طباعة الشيكات من النظام .</li>
                        <li> يتميز النظام بتسجيل المستفيدين الذين يتم التعامل معهم مباشرة من البنك دون وجود نظام فرعى.</li>
                        <li>يتميز النظام بتسجيل البنود البنكية التى يتم التعامل بها مباشرة من البنك دون وجود نظام فرعى. </li>
                        <li>يتميز النظام بامكانية تسجيل الشيكات المحصلة من العملاء وغيرهم. </li>
                        <li>يتميز النظام بالربط الكامل مع أنظمة العملاء والموردين والأصول الثابتة والموارد البشرية والمستخلصات. </li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li> يتميز النظام بامكانية عمل التحويلات البنكية الصادرة والواردة.</li>
                        <li>يتميز النظام بامكانية التحويل من بنك الى بنك اخر. </li>
                        <li> يتميز النظام بامكانية التحويل من البنك الى الصندوق. </li>
                        <li>يتميز النظام بامكانية الغاء الشيكات الصادرة كسندات ملغاة ولا يتم حذفها نهائيا. </li>
                        <li> يتميز النظام بامكانية تسجيل المصروفات البنكية.</li>
                        <li>يتميز النظام بامكانية اعداد مذكرة تسوية البنك. </li>
                        <li>يتميز النظام بالربط مع نظام ضريبة القيمة المضافة . </li>
                        <li> كما يتميز النظام بوجود عدد كبير من التقارير التى تخدم احتياجات الادارة ومتخذى القرار فى المؤسسة.</li>
                        <li>يتميز النظام بامكانية استخراج جميع التقارير بصيغة PDF وامكانية تصدير جميع التقارير على الاكسيل . </li>
                        <li>يتميز النظام بامكانية العمل علية من خلال الشبكة الداخلية او الدخول عن طريق الانترنت حيث ان النظام web-enabled . </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =06=-->

                <!--Start Page Content =07=-->
                <div class="product_page-section">
                    <h3> مميزات نظام اسكون لمتابعة العهد النقدية </h3>

                    <ul class="product_page-list">
                        <li>يتميز النظام بامكانية اضافة الصناديق والخزائن الخاصة بالشركة وربطها بالحسابات. </li>
                        <li> يتميز النظام بتسجيل دفاتر سندات الصرف والقبض التى يتم استخدامها.</li>
                        <li> يتميز النظام بتسجيل المستفيدين الذين يتم التعامل معهم مباشرة من الصندوق دون وجود نظام فرعى.</li>
                        <li>يتميز النظام بتسجيل البنود النقدية التى يتم التعامل بها مباشرة من الصندوق دون وجود نظام فرعى. </li>
                        <li>يتميز النظام بالغاء سندات الصرف والقبض مع الحتفاظ بها كسندات ملغاة ولا يتم حذفها لدقة عملية المراجعة. </li>
                        <li>يتميز النظام بامكانية تحديد الموظفين الذين يقومون باستلام عهد نقدية. </li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li> يتميز النظام بامكانية تحديد انواع العهد النقدية التى يتم منحها للموظفين .</li>
                        <li> يتميز النظام بامكانية فتح سجل لكل نوع عهدة وربطه مع الموظف وكذلك تحديد الحد الاقصى لمبلغ العهدة .</li>
                        <li> يتميز النظام بامكانية فتح اكثر من عهدة لكل موظف حسب نوعها مع الرقابة على اجمالى قيمة العهد المسموح بها للموظف .</li>
                        <li>يتميز النظام بامكانية تصفية كل عهدة على حدى بفواتيرها ومعاملاتها وربطها مع الانظمة المعنيه بهذه العهدة . </li>
                        <li> يتميز النظام بالربط الكامل مع أنظمة الحسابات والموردين العملاء والمشاريع والمستخلصات والأصول الثابتة والموارد البشرية و.....</li>
                        <li> يتميز النظام بامكانية اضافة مراكز التكلفة عند عمل تصفية للعهدة.</li>
                        <li> يتميز النظام بامكانية طباعة سندات تصفية العهد.</li>
                        <li>يتميز النظام بامكانية متابعة الفواتير التشغيلية وتصفيتها.</li>
                        <li>يتميز النظام بالربط مع نظام ضريبة القيمة المضافة . </li>
                        <li> كما يتميز النظام بوجود عدد كبير من التقارير التى تخدم احتياجات الادارة ومتخذى القرار فى المؤسسة.</li>
                        <li> يتميز النظام بامكانية استخراج جميع التقارير بصيغة PDF وامكانية تصدير جميع التقارير على الاكسيل .</li>
                        <li>يتميز النظام بامكانية العمل علية من خلال الشبكة الداخلية او الدخول عن طريق الانترنت حيث ان النظام web-enabled . </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =07=-->

                <!--Start Page Content =08=-->
                <div class="product_page-section">
                    <h3> مميزات نظام اسكون لمتابعة خطابات الضمان </h3>
                    <ul class="product_page-list">
                        <li> يتميز النظام بامكانية متابعة خطابات الضمان الصادرة اوالمستلمة من الجهات الاخرى </li>
                        <li> يتميز النظام بامكانية تصنيف خطابات الضمان حسب انواعها ( ابتدائى – نهائى – دفعة مقدمة ) .</li>
                        <li> ربط النظام بطريقة آلية مع جميع أنظمة أسكون وخاصه نظام الحسابات العامة والبنوك وترحيل قيود العمليات آليا ( مصاريف الاصدار والتجديد وعمولة البنك والتامين المحجوز )</li>
                        <li>ربط النظام مع البيانات اللازمة مثل البنك والمشروع والجهة المالكة والمورد مع تحديد نوع الضمان وفترة السريان مع تحديد العمولات والتأمين المحجوز .</li>
                        <li>يتميز النظام بامكانية متابعة وتسجيل كل عمليات خطابات الضمان من اصدار – تمديد – تجديد - تسييل </li>
                        <li>يتميز النظام بامكانية المتابعة لتواريخ انتهاء الضمانات وذلك باكثر من طريقة عرض حسب نوع الضمان او حسب البنك </li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>يتميز النظام بامكانية متابعة حدود التسهيلات الخاصة بالضمانات وفقاً لانواعها (الحد – المستخدم – المتاح ).</li>
                        <li>يتميز النظام بامكانية اعطاء تنبيهات للمستخدمين بتواريخ انتهاء الضمانات قبل مواعيد الانتهاء لاعطاء الفرصة للادارة لاتخاذ اللازم فيما يخص التمديد او التجديد ...... </li>
                        <li>يتميز النظام بالربط مع نظام ضريبة القيمة المضافة . </li>
                        <li>كما يتميز النظام بوجود عدد كبير من التقارير التى تخدم احتياجات الادارة ومتخذى القرار فى المؤسسة. </li>
                        <li>يتميز النظام بامكانية استخراج جميع التقارير بصيغة PDF وامكانية تصدير جميع التقارير على الاكسيل . </li>
                        <li>يتميز النظام بامكانية العمل علية من خلال الشبكة الداخلية او الدخول عن طريق الانترنت حيث ان النظام web-enabled . </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =08=-->

                <!--Start Page Content =09=-->
                <div class="product_page-section">
                    <h3> مميزات نظام اسكون لمتابعة الاعتمادات المستندية </h3>
                    <ul class="product_page-list">
                        <li> يتميز النظام بامكانية ادخال كافة بيانات الاعتمادات المستندية من ( انواع الاعتمادات - المشاريع – البنك ( المستفيد ) – شركات التامين – شروط التسليم – طرق الدفع – شروط الدفع – طرق الشحن – شروط الشحن – المبالغ المحجوزة .....)</li>
                        <li>يتميز النظام بامكانية ربط كل اعتماد مستندى على مشروع وحساب البنك الخاص بالشركة . </li>
                        <li>يتميز النظام بامكانية متابعة حدود الخاصة بالاعتمادات المستندية على مستوى كل بنك على حدى من حيث (الحد – المستخدم – المتاح ). </li>
                        <li>يتميز النظام بامكانية تسجيل كافة انواع المصروفات الخاصة بالاعتمادات المستندية مع ترحيل القيود الخاصة بها للحسابات العامة. </li>
                        <li> امكانية فتح الاعتماد المستندى وربطه مع كافة الانظمة ( الحسابات - الموردين – المشتريات – المخزون - القروض ) .</li>
                        <li>امكانية فتح الاعتماد المستندى وربطه مع امر شراء او بدون امر شراء . </li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>امكانية عمل تمديد للاعتماد المستندى . </li>
                        <li>امكانية عمل سداد على الاعتماد المستندى سواء على امر شراء مربوط على نظام المشتريات او امكانية السداد المباشر بدون امر شراء . </li>
                        <li> يتميز النظام بامكانية قيام البنك بسداد قيمة الاعتماد للمستفيد على ان يقوم بجدولة قيمتها بكمبيالات على الشركة تسدد للبنك وفقاً لتواريخ استخقاقها .</li>
                        <li>يتميز النظام بامكانية اعطاء تنبيهات للمستخدمين بتواريخ انتهاء الاعتمادات قبل مواعيد الانتهاء لاعطاء الفرصة للادارة لاتخاذ اللازم فيما يخص التمديد او التجديد ...... </li>
                        <li> يتميز النظام بانه فى حالة الربط مع نظام القروض يتوفر اختيار اعادة تمويل الاعتماد عن طريق انشاء قرض .</li>
                        <li>امكانية عمل تخفيض او اضافة على الاعتماد . </li>
                        <li> يتميز النظام بالربط مع نظام ضريبة القيمة المضافة .</li>
                        <li>كما يتميز النظام بوجود عدد كبير من التقارير التى تخدم احتياجات الادارة ومتخذى القرار فى المؤسسة. </li>
                        <li> يتميز النظام بامكانية استخراج جميع التقارير بصيغة PDF وامكانية تصدير جميع التقارير على الاكسيل .</li>
                        <li> يتميز النظام بامكانية العمل علية من خلال الشبكة الداخلية او الدخول عن طريق الانترنت حيث ان النظام web-enabled .</li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =09=-->

                <!--Start Page Content =10=-->
                <div class="product_page-section">
                    <h3> مميزات نظام اسكون لمتابعة القروض الاسلامية </h3>
                    <ul class="product_page-list">
                        <li>يتميز النظام بامكانية ادخال كافة بيانات القروض الاسلامية من ( الجهات الطالبة للقرض – الغرض من القرض – تكويد البنوك المرتبط بالقروض – ادخال الحدود على مستوى كل بنك على حدى – المفوضين للتوقيعات ....... ) . </li>
                        <li>يتميز النظام بامكانية عمل طلب للقرض حسب كل جهة طالبة بالشركة . </li>
                        <li>يتميز النظام بامكانية ادخال القرض مع تحديد ( البنك – فرع البنك قيمة القرض – الغرض من القرض – قيمة القرض – كيفية السداد – مدة القرض ....... ) . </li>
                        <li> يتميز النظام بامكانية عمل سداد على مستوى كل قرض على حدى ، مع معرفة كل حد على مستوى كل قرض وعى مستوى فرع البنك والبنك الرئيسى .</li>
                        <li>يتميز النظام بامكانية تسجيل القرروض طويلة الاجل . </li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li> يتميز النظام بامكانية جدولة كافة القروض حسب طريقة السداد ( شهرى – ربع سنوع – نص سنوى – سنوى ) .</li>
                        <li> يتميز النظام بامكانية تسجيل القروض المرتبطة بنظام الاعتمادات المستندية .</li>
                        <li>يتميز النظام بامكانية تسجيل القروض المرتبطة بنظام خطابات الضما ن . </li>
                        <li> يتميز النظام بامكانية تسجيل قروض وربطها على نظام المستخلصات .</li>
                        <li>يتميز النظام بامكانية التنبية والمتابعة فيما يخص الالتزامات المالية الناتجة عن القرض كل حسب تاريخ استحقاقه . </li>
                        <li> يتميز النظام بالربط مع نظام ضريبة القيمة المضافة .</li>
                        <li> كما يتميز النظام بوجود عدد كبير من التقارير التى تخدم احتياجات الادارة ومتخذى القرار فى المؤسسة.</li>
                        <li>يتميز النظام بامكانية استخراج جميع التقارير بصيغة PDF وامكانية تصدير جميع التقارير على الاكسيل . </li>
                        <li>يتميز النظام بامكانية العمل علية من خلال الشبكة الداخلية او الدخول عن طريق الانترنت حيث ان النظام web-enabled </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =10=-->

                <!--Start Page Content =011=-->
                <div class="product_page-section">
                    <h3> مميزات نظام اسكون لاحتساب الضريبة </h3>
                    <ul class="product_page-list">
                        <li>يتميز النظام بامكانية تعريف نوع الضريبة مع تحديد مدة الاقرار الضريبى وربط الحسابات اللازمة لكل نوع ضريبة </li>
                        <li> يتميز النظام بامكانية تحديد مدة الاقرار شهرى / 3 شهور </li>
                        <li> يتميز النظام بامكانية تقسيم الاصناف الى خاضعة للنسبة الاساسية / خاضعة للنسبة صفر / معفاة </li>
                        <li> يتميز النظام بامكانية تقسيم العملاء والمورديين الى خاضع للضريبة او معفى </li>
                        <li>يتميز النظام بامكانية تقسيم مناطق التوريد الى خاضع للضريبة او معفاة </li>
                        <li>يتميز النظام بامكانية بربط شاشة اصدار الاقرار الضريبى بجميع الانظمة والعمليات التى تمت عليها </li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li> يتميز النظام بامكانية تحليل الاقرار الضريبى الى المبيعات بتوجيهاتها والمشتريات بتوجيهاتها</li>
                        <li>يتميز النظام بامكانية ترحيل جميع العمليات من مختلف الانظمة على حسابات الضرائب المرتبطة بها </li>
                        <li>يتميز النظام بامكانية اصدار الاقرار الضريبى بنفس شكل النموذج الذى يتم تعبئته من موقع الزكاة والدخل </li>
                        <li>يتميز النظام بامكانية استخراج تقارير تحليلية على المستوى الاجمالى والتفصيل توضح عمليات الضريبة سواء المدينة والدائنة مع فصل كل تصنيف على حدى</li>
                        <li>كما يتميز النظام بوجود عدد كبير من التقارير التى تخدم احتياجات الادارة ومتخذى القرار فى المؤسسة. </li>
                        <li> يتميز النظام بامكانية استخراج جميع التقارير بصيغة PDF وامكانية تصدير جميع التقارير على الاكسيل .</li>
                        <li> يتميز النظام بامكانية العمل علية من خلال الشبكة الداخلية او الدخول عن طريق الانترنت حيث ان النظام web-enabled .</li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span> <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>

                </div>
                <!--End Page Content =011=-->
            </div>
        </div>
        <!--End Product Page -->
    @else
        <!--Start Product Page -->
        <div class="product_page">
            <div class="container">
                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>Features of Ascon General Accounts System</h3>
                    <ul class="product_page-list">
                        <li>The system is capable of handling more than one language in both screens and reports.</li>
                        <li>The system is characterized by a design suitable for working on the same database for  multi-branch system and multiple companies.</li>
                        <li>The system is characterized by the possibility of doing a  manual to the accounts and cost centers in the form of a multi-level tree.</li>
                        <li>The system is characterized by the possibility of working in one currency or multiple currencies with the flexibility to change the exchange rate of foreign currency.</li>
                        <li>The system is characterized by its support to work on any of the systems commonly known as a daily system (American daily system, the French daily system).</li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>The system is characterized by ease of closing work automatically, the transfer of balances of opening accounts to the budget automatically at the end of each accounting period.</li>
                        <li>The system is characterized by the possibility to enter the estimated balances of accounts and compare them with the actual balances monthly and annually, as well as the comparisons of the actual and estimated of the daily entries and report the deviation between them.</li>
                        <li>The system has the ability to distribute to the cost centers manually during the entry process or rates are defined by the user automatically.</li>
                        <li>The system has the ability to query the balance of accounts before and after the transfer to the General Secretary.</li>
                        <li>The system is characterized by the possibility of making periodic restrictions in the case of frequent periodic inputs.</li>
                        <li>The system is fully linked between public accounts and the rest of the systems so that the user can see the restrictions from the subsystem or from the system of  the general accounts.</li>
                        <li>The system is characterized by easily analyzing  account’s (expenses / income) either by cost centers or monthly.</li>
                        <li>The system is easy to search in so you can search for any amount of money or any name entered by the restrictions in the account, it can also report the limitations of the amount or name to be searched for.</li>
                        <li>The system is characterized by a large number of reports that serve all the needs of financial managers and decision-makers in the institution.</li>
                        <li>The system is characterized by the possibility of extracting all reports in PDF format and exporting all reports on the excel.</li>
                        <li>The system is characterized by the possibility of working through the internal network or access through the Internet as the system is web-enabled.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3>Features of Ascon Fixed Assets System</h3>
                    <ul class="product_page-list">
                        <li>The system is characterized by the possibility of classifying the assets into (assets - custody - asset custody).</li>
                        <li>The system is characterized by the possibility of dividing fixed assets into different types and groups in a branched tree form.</li>
                        <li>The system is characterized by recording all fixed asset data such as the original number, the original barcode, the original name, the description, the administrative structure of the original asset, the date of purchase, the purchase value, the depreciation ratio, etc.</li>
                        <li>The system is characterized by the possibility of determining the start date depreciation of the fixed asset, the default age, the transfer of depreciation of assets to the general accounts.</li>
                        <li>The system is characterized by easy processing of operations performed on the original purchase, partial addition, depreciation, total exclusion, and partial exclusion.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>The system is characterized by the possibility of transferring the asset from the custody of an employee to another employee.</li>
                        <li>The system is characterized by the possibility of transferring the asset from an administrative structure to another administrative structure and load each structure with its depreciation automatically according to the time period in which the original existed in each structure.</li>
                        <li>The system also features the possibility of making partial additions to assets.</li>
                        <li>The system is easy to calculate depreciation of assets and transfer of depreciation restrictions to the general accounts.</li>
                        <li>The system is compatible with the VAT system.</li>
                        <li>The system is characterized by a large number of reports that serve the needs of managers and decision-makers in the institution.</li>
                        <li>The system is characterized by the possibility of extracting all reports in PDF format and exporting all reports on the excel.</li>
                        <li>The system is characterized by the possibility of working through the internal network or access through the Internet as the system is web-enabled.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>

                </div>
                <!--End Page Content =02=-->

                <!--Start Page Content =03=-->
                <div class="product_page-section">
                    <h3>Ascon Customer Tracking System Advantages</h3>

                    <ul class="product_page-list">
                        <li>The system is characterized by the possibility of structuring customers in a branched tree form.</li>
                        <li>The system is characterized by the registration of all personal data, personal, financial, tax and sales, as well as the possibility of linking the delegate with the client.</li>
                        <li>The system is characterized by the work of linking the client to the types and activity,   sale sector and the area.</li>
                        <li>The system has the ability to determine the credit limit for each customer as well as the grace periods.</li>
                        <li>The system has the possibility of exceptions on exceeding the limits of creditof customers with certain reference, while maintaining the system by the historical record of each customer's exceptions and indicating the reason for the exception and the exception grantor.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>The system is characterized by the ability to divide the company's customers into categories and define different policies for each group of customers.</li>
                        <li>The system is characterized by the possibility of targeted work for the customer's purchases of items and groups of items as well as the target of the proceeds from the client.</li>
                        <li>The system has the potential to target sales of delegates of items at each customer level.</li>
                        <li>The system is characterized by the control of customer in depts. and the delegate balances.</li>
                        <li>The system is allows the client to deal with more than one delegate and  delegates to deal with more than one customer.</li>
                        <li>The system defines the target of the delegates according to the periods determined by the administration (monthly or quarterly).</li>
                        <li>The system is characterized by the possibility of distributing customers to areas and branches.</li>
                        <li>The system is characterized by the possibility of preparing various tables to calculate the debt of the users which allows them  to extract the report of the reconstruction of any debt.</li>
                        <li>The system is characterized by the possibility of preparing routes for  the delegates and visiting daily and weekly plan.</li>
                        <li>The system is characterized by the possibility of assessing the results of visits of delegates and acknowledging positive and successful visits compared to unsuccessful visits.</li>
                        <li>The system is characterized by the ability to follow customer balances and payment movements from customers.</li>
                        <li>The system is characterized by the ability to register customer complaints and report to deal with them.</li>
                        <li>The system is characterized by linking with loyalty system and calculating points.</li>
                        <li>The system is linked to the VAT system.</li>
                        <li>The system is characterized by a large number of reports that serve the needs of managers and decision-makers in the institution.</li>
                        <li>The system is characterized by the possibility of extracting all reports in PDF format and exporting all reports on the excel.</li>
                        <li>The system is characterized by the possibility of working through the internal network or access through the Internet as the system is web-enabled.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =03=-->

                <!--Start Page Content =04=-->
                <div class="product_page-section">
                    <h3>Ascon Suppliers Tracking System Advantages</h3>
                    <ul class="product_page-list">
                        <li>The system is characterized by the possibility of structuring suppliers in a branched tree form.</li>
                        <li>The system is characterized by the registration of all the supplier’s personal data, personal and financial and tax, as well as the possibility of linking the delegate with the supplier.</li>
                        <li>The system is characterized by the possibility of identifying the type and activity of the supplier and linking it with the main and subsidiary area.</li>
                        <li>The system has the ability to determine the credit limit for each supplier as well as the grace periods.</li>
                        <li>The possibility of producing a debt reconstruction report for each supplier.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>The possibility of differentiation between suppliers and subcontractors.</li>
                        <li>The system is characterized by classification of suppliers into different sectors.</li>
                        <li>The system is characterized by the possibility of registration of  purchase’s delegates responsible for dealing with the suppliers.</li>
                        <li>The system is distinguished by the possibility of distinguishing between the supplier's balance of the prepayments, outstanding payments,  and retained money with the possibility of extracting a statement of account for each payment type or a combined  account statement of the supplier.</li>
                        <li>The system is characterized by recording all personal and financial supplier data and methods of dealing with each supplier and  the registration of the supplier's delegate.</li>
                        <li>The system is characterized by updating the balances of suppliers depts. and credits according to the purchasing movements.</li>
                        <li>The system  has the possibility of payment to the suppliers from the Fund or from the bank or through the accounts.</li>
                        <li>The system is linked to the VAT system.</li>
                        <li>The system is characterized by a large number of reports that serve the needs of managers and decision-makers in the institution.</li>
                        <li>The system is characterized by the possibility of extracting all reports in PDF format and exporting all reports on the excel.</li>
                        <li>The system is characterized by the possibility of working through the internal network or access through the Internet as the system is web-enabled.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =04=-->

                <!--Start Page Content =05=-->
                <div class="product_page-section">
                    <h3>Ascon For Receipts And Payments System Advantages</h3>
                    <ul class="product_page-list">
                        <li>The system is characterized by the possibility of adding funds and saving of the company and linked to accounts.</li>
                        <li>The system is characterized by recording the outputs and inputs that are used.</li>
                        <li>The system is characterized by the registration of beneficiaries who are dealt with directly from the Fund without a sub-system.</li>
                        <li>The system is characterized by the registration of the cash that is dealt directly by the Fund without a sub-system.</li>
                        <li>The system is characterized by the possibility of canceling the outputs and inputs debentures ,while retaining them as canceled  debentures and not being deleted, for the accuracy of the revision process.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>The system is characterized by the possibility of transferring from the fund to another fund.</li>
                        <li>The system is characterized by the possibility of transferring from the Fund to the bank.</li>
                        <li>The system is fully linked to customer systems, suppliers, fixed assets, human resources and abstracts.</li>
                        <li>The system is characterized by the possibility of adding cost centers making output and input debentures.</li>
                        <li>The system is characterized by the possibility of printing output and input debentures.</li>
                        <li>The system is characterized by alerting the user to the outstanding movements that must be captured or paid from other systems.</li>
                        <li>The system is linked to the VAT system.</li>
                        <li>The system is characterized by a large number of reports that serve the needs of managers and decision-makers in the institution.</li>
                        <li>The system is characterized by the possibility of extracting all reports in PDF format and exporting all reports on the excel.</li>
                        <li>The system is characterized by the possibility of working through the internal network or access through the Internet as the system is web-enabled.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>

                </div>
                <!--End Page Content =05=-->

                <!--Start Page Content =06=-->
                <div class="product_page-section">
                    <h3>Ascon Checks Printing System Advantages</h3>
                    <ul class="product_page-list">
                        <li>The system is characterized by the possibility of adding all the banks of the company and branches of these banks and linking them to accounts.</li>
                        <li>The system is characterized by the possibility of recording the company's check books and printing checks from the system.</li>
                        <li>The system is characterized by the registration of beneficiaries dealing directly from the bank without a sub-system.</li>
                        <li>The system is characterized by the registration of bank items with dealing directly by the bank without the existence of a subsidiary system.</li>
                        <li>The system has the ability to record checks received from customers and others.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>The system is fully linked to customer systems, suppliers, fixed assets, human resources and abstracts.</li>
                        <li>The system is characterized by the possibility of making  inbound and outbound  bank transactions.</li>
                        <li>The system has the ability to do transactions from one bank to another bank.</li>
                        <li>The system is has the ability of doing transactions from the bank to the Fund.</li>
                        <li>The system is has the ability of canceling the checks issued as canceled debentures and not deleted permanently.</li>
                        <li>The system has the ability to record bank expenses.</li>
                        <li>The system has the ability of preparing the settlement memorandum of the bank.</li>
                        <li>The system is linked to the VAT system.</li>
                        <li>The system is characterized by a large number of reports that serve the needs of managers and decision-makers in the institution.</li>
                        <li>The system is characterized by the possibility of extracting all reports in PDF format and exporting all reports on the excel.</li>
                        <li>The system is characterized by the possibility of working through the internal network or access through the Internet as the system is web-enabled.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =06=-->

                <!--Start Page Content =07=-->
                <div class="product_page-section">
                    <h3>Ascon Monetary Covenant Tracking System Advantage</h3>

                    <ul class="product_page-list">
                        <li>The system is characterized by the ability of adding funds and savings of the company and linking it to accounts.</li>
                        <li>The system is characterized by recording the input and output debentures used.</li>
                        <li>The system is characterized by the registration of beneficiaries who are dealt with directly from the Fund without a sub-system.</li>
                        <li>The system is characterized by the registration of monetary items that are dealt directly by the Fund without a sub-system.</li>
                        <li>The system is characterized by the cancellation of exchange and arrest bonds with retention as canceled bonds and are not deleted for the accuracy of the audit process.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>The system is characterized by the ability of identifying employees who receive a cash covenant.</li>
                        <li>The system is characterized by the ability of canceling the outputs and inputs debentures ,while retaining them as canceled  debentures and not being deleted, for the accuracy of the revision process.</li>
                        <li>The system is characterized by the ability of identifying employees who receive a cash covenant.</li>
                        <li>The system is characterized by the ability of determining the types of monetary covenant granted to employees.</li>
                        <li>The system is characterized by the ability of opening a registry for each type of custody and linking it  with the employee as well as determining the maximum amount of custody.</li>
                        <li>The system is characterized by the ability of opening more than one custody to each employee with the control on the total value of the Covenant allowed to the employees.</li>
                        <li>The system is characterized by the ability of filtering each custody on its own with its own bills, and transactions and linking them with the regulations concerned with this custody.</li>
                        <li>The system is fully linked with the systems of accounts and suppliers, customers, projects, abstracts, fixed assets, and human resources etc.</li>
                        <li>The system is characterized by the ability of adding cost centers when the liquidation of the custody.</li>
                        <li>The system is characterized by the ability of printing the covenant debentures.</li>
                        <li>The system is characterized by the ability of tracking and liquidation of operational bills.</li>
                        <li>The system is linked to the VAT system.</li>
                        <li>The system is characterized by a large number of reports that serve the needs of managers and decision-makers in the institution.</li>
                        <li>The system is characterized by the possibility of extracting all reports in PDF format and exporting all reports on the excel.</li>
                        <li>The system is characterized by the possibility of working through the internal network or access through the Internet as the system is web-enabled.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =07=-->

                <!--Start Page Content =08=-->
                <div class="product_page-section">
                    <h3>Ascon Grantee Letter Tracking System Advantages</h3>
                    <ul class="product_page-list">
                        <li>The system is characterized by the ability of following letters of guarantee issued or received from other parties.</li>
                        <li>The system is characterized by the ability of classifying the letters of guarantee by type (initial, final, advanced payment).</li>
                        <li>The automatic connection of the system  with all Ascon systems, especially the system of the general accounts and banks and the transfer of process restrictions automatically (issuance costs and renewal and the bank's commission and insurance is reserved).</li>
                        <li>Linking the system with the necessary data such as bank, project owner, and the supplier, specifying the type of guarantee and the period of validity with the specified commissions and insurance reserved.</li>
                        <li>The system is characterized by the ability of tracking and recording all letters of guarantee operations from issuance - extension - renewal - liquidation.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>The system is characterized by the ability of tracking the expiry dates of the grantees by more than way according to the type of guarantee or to the bank.</li>
                        <li>The system is characterized by the ability of monitoring the limits of facilities for guarantees according to their types (limit - user – available).</li>
                        <li>The system is characterized by the ability to give alerts to users with the expiration dates of guarantees before  ahead to allow the management the opportunity to take the necessary actions ; extension or renewal etc.</li>
                        <li>The system is linked to the VAT system.</li>
                        <li>The system is characterized by a large number of reports that serve the needs of managers and decision-makers in the institution.</li>
                        <li>The system is characterized by the possibility of extracting all reports in PDF format and exporting all reports on the excel.</li>
                        <li>The system is characterized by the possibility of working through the internal network or access through the Internet as the system is web-enabled.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =08=-->

                <!--Start Page Content =09=-->
                <div class="product_page-section">
                    <h3>Ascon Letter Of Credit Tracking System Advantage</h3>
                    <ul class="product_page-list">
                        <li>The system is characterized by the ability of entering all kinds of Letter of credit (types of  accreditations- projects - bank (beneficiary) - insurance companies - delivery terms - payment methods - payment terms - shipping methods - shipping terms – advance reserved payments.</li>
                        <li>The system is characterized by the ability of linking each credited letter to the project and bank account.</li>
                        <li>The system has the ability to follow the L.C at each bank level in terms of (limit - user - available).</li>
                        <li>The system is able to record all types of expenses related to L.C and to transfer their restrictions to the general accounts.</li>
                        <li>The ability of opening the credit document and linking it with all systems (accounts - suppliers - purchases - inventory – loans).</li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>The ability of extending the L.C.</li>
                        <li>The  ability of payment of the L.C either on the purchase order linked to the procurement system or the direct payment without a purchase order.</li>
                        <li>The system is characterized by the ability of the bank to pay the value of the credit to the employer and to schedule the value of bills on the company to the bank according to the dates of extraction.</li>
                        <li>The system is characterized by the ability to give alerts to users with the expiration dates L.C before ahead to allow the management the opportunity to take the necessary actions; extension or renewal etc.</li>
                        <li>The system is characterized by, when linking with the loan system,  one can choose the refinancing of the credit through the establishment of a loan.</li>
                        <li>The possibility of reducing or adding to the credit.</li>
                        <li>The system is linked to the VAT system.</li>
                        <li>The system is characterized by a large number of reports that serve the needs of managers and decision-makers in the institution.</li>
                        <li>The system is characterized by the possibility of extracting all reports in PDF format and exporting all reports on the excel.</li>
                        <li>The system is characterized by the possibility of working through the internal network or access through the Internet as the system is web-enabled.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =09=-->

                <!--Start Page Content =10=-->
                <div class="product_page-section">
                    <h3>Ascon Islamic Loan Tracking System Advantages </h3>
                    <ul class="product_page-list">
                        <li>The system is characterized by the ability to insert all Islamic loan data from (loan applicants - the purpose of the loan - the coding of banks linked to loans – insertion of  the restrictions of each bank individually -  and the authorized signatories).</li>
                        <li>The system is characterized by its ability of making a request for the loan by each applicant company.</li>
                        <li>The system is characterized by its ability of  insertion of the loan with the identification of (bank - bank branch loan value - the purpose of the loan - the value of the loan - how to pay - the period of the loan).</li>
                        <li>The system is characterized by the ability to make payment for each loan separately, putting into consideration the branch and the main bank.</li>
                        <li>The system is characterized by the ability of allowing long-term loans.</li>
                    </ul>
                    <ul class="product_page-list product_page-list-js">
                        <li>The system is characterized by scheduling all loans according to the method of payment (monthly - quarter of the year -  semi-annual – annual).</li>
                        <li>The system is characterized by the ability to record loans associated with the L.C system.</li>
                        <li>The system has the ability to record loans associated with the letter of guarantee system.</li>
                        <li>The system is characterized by the ability to record loans and link them to the abstract system.</li>
                        <li>The system is characterized by the ability of monitoring and following  the financial liabilities resulting from the loan each according to its date of entitlement.</li>
                        <li>The system is linked to the VAT system.</li>
                        <li>The system is characterized by a large number of reports that serve the needs of managers and decision-makers in the institution.</li>
                        <li>The system is characterized by the possibility of extracting all reports in PDF format and exporting all reports on the excel.</li>
                        <li>The system is characterized by the possibility of working through the internal network or access through the Internet as the system is web-enabled.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =10=-->

                <!--Start Page Content =011=-->
                <div class="product_page-section">
                    <h3>Ascon Calculating Tax System Advantage</h3>
                    <ul class="product_page-list">
                        <li>The system is characterized by its ability of defining the type of tax with  determing  the duration of tax recognition and  linking the accounts required for each type of tax.</li>
                        <li>The system is characterized by its ability of determining the period of acknowledgment, either  monthly / 3 months.</li>
                        <li>The system is characterized by its ability of dividing the items to either  base / zero / exempt ratio.</li>
                        <li>The system is characterized by its ability of dividing customers and suppliers into either taxable or exempt.</li>
                        <li>The system is characterized by its ability of dividing the supply areas into either taxable or exempt.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>The system is characterized by its ability of linking the screen of the issuance of tax documentation of all the systems and processes carried out.</li>
                        <li>The system is characterized by its abilityof analyzing the tax documentations of sales and procurement under.</li>
                        <li>The system is characterized by its ability to migrate all operations from different systems to its associated tax calculations.</li>
                        <li>The system is characterized by its ability to issue the tax documentation  in the same form as the form that is filled from the site of Zakat and income.</li>
                        <li>The system is characterized by extracting analytical reports either overall or detailed showing the operations of the tax, both the creditor and  the debtor with the separation of each classification on its own.</li>
                        <li>The system is characterized by a large number of reports that serve the needs of managers and decision-makers in the institution.</li>
                        <li>The system is characterized by the possibility of extracting all reports in PDF format and exporting all reports on the excel.</li>
                        <li>The system is characterized by the possibility of working through the internal network or access through the Internet as the system is web-enabled.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>

                </div>
                <!--End Page Content =011=-->
            </div>
        </div>
        <!--End Product Page -->
    @endif
    @include(FE.'.pages.products.product_form')
@stop