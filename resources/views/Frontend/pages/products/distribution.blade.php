@extends(FEL.'.master')

@section('content')

    <!--=01=Start Page Head-->
    <section class="about-bg">
        <h1>{{ trans('main.Ascon_depository_group') }}</h1>
    </section>
    <!--=01=End Page Head-->

    @if(session()->get('lang') == 'ar')
        <!--Start Product Page -->
        <div class="product_page">
            <div class="container">
                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3> مميزات نظام أسكون للمشتريات </h3>
                    <ul class="product_page-list">
                        <li>
                            يتميز النظام بوجود دورة مشتريات كاملة بدءاَ من وجود موازنة تقديرية للاصناف المشتراه مروراً بطلبات النواقص ومن ثم معالجة لطلبات النواقص اما بطلب تحويل من المستودع فى حالة توافر الكميات المطلوبة او طلب شراء فى حالة عدم توافر الكميات ثم طلب عروض اسعار وارساله للموردين ثم تلقى عروض الاسعار من الموردين واختيار العرض الافضل آلياً من النظام ومن ثم تحويل العرض الافضل الى امر شراء والحصول على الاعتمادات الالكترونية اللازمة على امر الشراء ومن ثم ارساله للمورد والبدء باستلام الرسائل الواردة واعتمادها الكترونياً على النظام من الاطراف المعنية ثم يتم انشاء فاتورة داخلية على النظام ايضاً يتم اعتمادها ومن ثم تراجع وترحل للمالية وحسابات الموردين .
                        </li>

                        <li>
                            يتميز النظام بتعريف جميع انواع حركات المشتريات لدى الشركة مع التحكم فى طريقة الربط مع المخازن والحسابات العامة.

                        </li>
                        <li>
                            يتميز النظام بعمل قيود يومية الية فور تسجيل حركة الشراء.
                        </li>

                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li> يتميز النظام بعمل طلبيات للنواقص ليتم معالجتها وفقا لرصيد الصنف بالمخزن. </li>
                        <li>يتميز النظام بانه يسمح بتسجيل طلبات الشراء من الإدارات المختلفة بالشركة . </li>
                        <li>يتميز النظام بامكانية عمل طلبات عروض اسعار وتسجيلها حين وصول الرد من الموردين. </li>
                        <li>يتميز النظام بامكانية الاختيار الالى لأفضل عرض سعر من الموردين او ترك حرية الاختيار للمستخدم. </li>
                        <li> يتميز النظام بامكانية تحويل طلب الشراء الى امر شراء مباشرة.</li>
                        <li>يتميز النظام بامكانية استلام امر الشراء على رسالة واحدة أو اكثر من رسالة. </li>
                        <li>تسجيل فواتير المشتريات كاملة بكل انواع المصاريف التى تتم على المشتريات واستخراج فاتورة المشتريات الرسمية الخاصة بالشركة من النظام. </li>
                        <li>يتميز النظام بامكانية عمل مرتجع للمشتريات سواء بفاتورة او بدون فاتورة. </li>
                        <li>يتميز النظام بالتوافق مع نظام ضريبة القيمة المضافة . </li>
                        <li> كما يتميز النظام بوجود عدد كبير من التقارير التى تخدم احتياجات الادارة ومتخذى القرار فى المؤسسة.</li>
                        <li>يتميز النظام بامكانية استخراج جميع التقارير بصيغة PDF وامكانية تصدير جميع التقارير على الاكسيل . </li>
                        <li>يتميز النظام بامكانية العمل علية من خلال الشبكة الداخلية او الدخول عن طريق الانترنت حيث ان النظام web-enabled </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span>                 <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>

                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3>مميزات نظام أسكون للمخازن </h3>
                    <ul class="product_page-list">
                        <li>يتميز النظام إمكانيه تكويد الاصناف بشكل شجرى يساعد على ترتيب الاصناف وفقا لمجموعات الاصناف الخاصة بالشركة. </li>
                        <li> يتميز النظام بتسجيل كل الأصناف المخزنية والغير مخزنية.</li>
                        <li>يتميز النظام بامكانية تحديد الصنف البديل بحيث يمكن استخدام الصنف البديل فى حالة عدم وجود الصنف الأساسى. </li>
                        <li>يتميز النظام بامكانية عمل باركود لكل صنف.</li>
                        <li>يتميز النظام بالربط الكامل مع أنظمة المبيعات والمشتريات والحسابات العامة.</li>
                        <li>يتميز النظام بتعدد وحدات قياس الصنف للتسهيل فى عملية الشراء والصرف للبيع او التصنيع.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>يتميز النظام بظهور الرصيد المخزنى بعد كل حركة لها تأثير على المخزن. </li>
                        <li> يتميز النظام بمتابعة تاريخ الصلاحية للأصناف .</li>
                        <li> يتميز النظام بامكانية عمل جرد للمخزون فى اى وقت وبطريقة سهلة.</li>
                        <li>يتميز النظام بعمل تسوية جرد اليا او من خلال عمل حركات صادر ووارد . </li>
                        <li> يتميز النظام بتعريف مورد رئيسى لكل صنف من اصناف الشركة.</li>
                        <li>يتميز النظام بعمل تحويلات بين المخازن من خلال تسليم واستلام البضاعةوامكانية معرفة البضاعة بالطريق. </li>
                        <li> يتميز النظام بامكانية تحديد كميات الحد الأدنى والأقصى للأصناف وتحديد حد الطلب.</li>
                        <li> يتميز النظام بالتوافق مع نظام ضريبة القيمة المضافة .</li>
                        <li>كما يتميز النظام بوجود عدد كبير من التقارير التى تخدم مسئول المخازن وتساعد فى اتخاذ القرار. </li>
                        <li> يتميز النظام بامكانية استخراج جميع التقارير بصيغة PDF وامكانية تصدير جميع التقارير على الاكسيل .</li>
                        <li>يتميز النظام بامكانية العمل علية من خلال الشبكة الداخلية او الدخول عن طريق الانترنت حيث ان النظام web-enabled </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span>                 <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>

                </div>
                <!--End Page Content =02=-->

                <!--Start Page Content =03=-->
                <div class="product_page-section">
                    <h3> مميزات نظام أسكون للمبيعات ونقاط البيع</h3>

                    <ul class="product_page-list">
                        <li>يتميز النظام بعمل دورة المبيعات كاملة. </li>
                        <li> يتميز النظام بوجود خط سير مندوبين المبيعات واستخراج تقارير لمعرفة مدى كفائة المندوب.</li>
                        <li> يتميز النظام بعمل عروض اسعار شاملة الشروط وطرق الدفع الى العملاء.</li>
                        <li>يتميز النظام بامكانية التحول الالى من عرض السعر الى أمر بيع فى حالة اعتماد العميل.</li>
                        <li>يتميز النظام بعمل مذكرات تسليم البضائع التى تمت فى امر البيع وتحويلها اليا الى فاتورة مبيعات.</li>
                        <li> يتميز النظام بامكانية عمل مرتجع للمبيعات سواء بفاتورة او بدون فاتورة.</li>
                        <li> يتميز النظام باستخدام اكثر من عملة.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>يتميز النظام بعدم امكانية اصدار فاتورة جديدة لعميل تعدى الحد الائتمانى. </li>
                        <li> يتميز النظام بعمل الاصناف الخدمية وربطها مع فاتورة الأصناف.</li>
                        <li>يتميز النظام بامكانية عمل حجز مجموعة من الأصناف للعميل. </li>
                        <li> يتميز النظام بالارتباط الكامل مع أنظمة العملاء والمخازن والحسابات العامة.</li>
                        <li> يتميز النظام بعمل للعملاء على مجموعة معينة من الأصناف.</li>
                        <li> يتميز النظام بالعمل من خلال الشحنات للتقليل من كميات الأصناف المكودة فى النظام.</li>
                        <li>يتميز النظام بالتوافق مع نظام ضريبة القيمة المضافة . </li>
                        <li>كما يتميز النظام بوجود عدد كبير من التقارير التى تخدم احتياجات الادارة ومتخذى القرار فى المؤسسة. </li>
                        <li> يتميز النظام بامكانية استخراج جميع التقارير بصيغة PDF وامكانية تصدير جميع التقارير على الاكسيل .</li>
                        <li> يتميز النظام بامكانية العمل علية من خلال الشبكة الداخلية او الدخول عن طريق الانترنت حيث ان النظام web-enabled</li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span>                 <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =03=-->




                <!--Start Page Content =04=-->
                <div class="product_page-section">
                    <h3> مميزات نظام أسكون لنظام مبيعات السيارات </h3>

                    <ul class="product_page-list">
                        <li>يهدف النظام التى سهولة الوصول للعميل وسرعة التسليم والفوترة وكذلك تحقيق الرقابة على المندوبين وزيادة انتاجيتهم .</li>
                        <li>امكانية الرقابة على مخزون السيارة .</li>
                        <li>امكانية تسجيل الوصول عند العميل على النظام على ان يقوم النظام بالتاكد من تواجد المندوب بالموقع الفعلى للعميل ومن ثم يمكن البدء باجراء المعاملات الخاصة بالعميل.</li>
                        <li>امكانية تحميل خطة زيارات المندوب وخط سيره .</li>
                        <li>امكانية عمل فاتورة مبيعات فورية وطباعة الفاتورة وتسليمها للعميل اثناء الزيارة .</li>
                        <li>امكانية عمل مرتجع مبيعات .</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <Li>امكانية عمل جرد للبضاعة الموجودة لدى العميل</Li>
                        <Li>امكانية عمل طلبية للعميل ومن ثم فاتورة بعد اعتماد الطلبية من المشرف او مدير المبيعات</Li>
                        <Li>امكانية عمل تحصيلات من العميل وطباعة سند القبض بالموقع</Li>
                        <Li>امكانية فتح عميل جديد على ان يكون معلق بالنظام لحين مراجعة مستنداته والموافقة عليه من الادارة.</Li>
                        <Li>امكانية الاطلاع علي عروض المبيعات من الجهاز المتنقل</Li>
                        <Li>امكانية الاطلاع على الاصناف ومجموعات الاصناف مع صورها المرفقة .</Li>
                        <Li> امكانية الاطلاع على


                            <bdi> dashboards</bdi> للمستهدف والمحقق والمبيعات والتحصيلات والمخزون .</Li>
                        <Li>النظام مرتبط بشكل كامل مع نظام المبيعات والمخازن والعملاء والحسابات العامة .</Li>
                        <Li> يمكن تحميل النظام على اجهزة الجوال او وحدات ال

                            <bdi>handheld </bdi> المجهزة لهذا النوع من المبيعات.</Li>
                        <Li> امكانية الاطلاع على الموقع الحالى لتواجد المندوب حيث يتصل الجهاز مع GPS</Li>
                        <Li> يمكن للجهاز ان يعمل ONLINE OR OFFLINF</Li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span>                 <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>

                </div>
                <!--End Page Content =04=-->


                <!--Start Page Content =05=-->
                <div class="product_page-section">
                    <h3> مميزات نظام أسكون لاحتساب النقاط </h3>
                    <ul class="product_page-list">
                        <li> يعتمد نظام النقاط على عمليات البيع المباشر بين نقطة البيع والعميل</li>
                        <li>يهدف النظام الى منح العميل حوافز لكل عملية شراء بحيث يحفز العميل للقيام بتكرار عمليات الشراء او تعظيم المشتريات لزيادة اكتساب النقاط </li>
                        <li> يقوم النظام بمنح وزن نسبى لكل صنف من قيمة النقاط المكتسبة التى بالتالى تتحول الى مبالغ فى حساب العميل ( لاختلاف ربحية كل صنف )</li>
                        <li>فى حالة قيام العميل باكتساب النقاط يتم تسجيلها كنقاط معلقة الا ان تمر عليها مدة محددة من المستخدم ( فى الغالب مدة سماح الارجاع ) الى ان تتحول تلقائيا الى نقاط فعالة قابلة للاستبدال </li>
                        <li>يتم تحديد حد نقاط معين لاستبدال النقاط على العميل بلوغها حتى يسمح النظام باستبدال النقاط </li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>يمكن عمل شرائح لترقية النقاط كمكافاة للعميل لبلوغه حد معين فيتم مضاعفة الرصيد الحالى للعميل </li>
                        <li> تتم عمليات الاستبدال من نقاط البيع حيث يمكن استبدال كلى او جزئى للرصيد المتاح للعميل نظير قيامه بعملية شراء</li>
                        <li> يتم تسجيل عملاء النقاط برقم الهاتف حيث يعتبر عو الرقم المميز لكل عميل</li>
                        <li>

                            يتم تسجيل بعض البيانات الاخرى لكل عميل مثل النوع والعمر والحالة الاجتماعية وبيانات اخرى بالاضافة الى وجود سجل بالاصناف المهتم بها العميل بشرائها يمكن استخدام هذه البيانات فى استخراج تقارير اسقسائية لتحليل طبيعة المبيعات سواء بالفئة العمرية او النوع او استخدام وسيلة اتصال العميل لارسال العروض الترويجية

                        </li>
                        <li>ربط النظام مع الحسابات العامة حيث يقوم بترحيل التوجيهات اللازمة لعملية اكتساب النقاط او عملية الاستبدال </li>
                    </ul>
                    <span class=" product_page-button button_hide-js ">   اخفاء  <i  class="fa fa-sort-asc"></i></span>                 <span class=" product_page-button button_show-js">   اظهار المزيد   <i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>

                </div>
                <!--End Page Content =05=-->


            </div>
        </div>
        <!--End Product Page -->
    @else
        <!--Start Product Page -->
        <div class="product_page">
            <div class="container">
                <!--Start Page Content =01=-->
                <div class="product_page-section">
                    <h3>Advantages Of Ascon Procurement System</h3>
                    <ul class="product_page-list">
                        <li>The system is characterized by its full procurement cycle starting from an estimated budget for the items purchased through the requests of deficiencies and then address the requests of deficiencies which could either be a request for transfer from the warehouse in the case of the availability of quantities required or a request for a purchase in the absence of quantities and then request a price quotes and send to suppliers and then receive the quotations of the suppliers and choose the best offer automatically from the system and then convert the best offer to the purchase order and obtain the necessary electronic credits on the purchase order and then send to the supplier and start receiving incoming messages electronically approved on the system of the parties concerned and finally the establishment of an internal invoice on the system also adopted and then retreat and migrate to the financial accounts suppliers.</li>
                        <li>The system is characterized by defining all types of procurement movements at the company with the control the way to connect with stores and general accounts.</li>
                        <li>The system is characterized by the creation of daily restrictions immediately after recording the movement of the purchase.</li>
                        <li>The system is characterized by making orders for deficiencies to be processed according to the amount of each type in the store.</li>
                        <li>The system is characterized by allowing registration of purchase orders from different departments of the company.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>The system is characterized by its ability of making requests for quotations and registering it the moment the supplier’s response.</li>
                        <li>The system is characterized by its ability of  either the automatic selection of the best offer from suppliers or leaving the freedom of choice for the user.</li>
                        <li>The system has the ability to convert the purchase request into a purchase order directly.</li>
                        <li>The system has the ability to receive a purchase order on one or more messages.</li>
                        <li>Recording the receipts of purchases complete with all types of expenses incurred on purchases and extracts the official purchase receipts of the company from the system.</li>
                        <li>The system is characterized by the possibility of returning purchases, either with a receipt or without a receipt.</li>
                        <li>The system is linked to the VAT system.</li>
                        <li>The system is characterized by a large number of reports that serve the needs of managers and decision-makers in the institution.</li>
                        <li>The system is characterized by the possibility of extracting all reports in PDF format and exporting all reports on the excel.</li>
                        <li>The system is characterized by the possibility of working through the internal network or access through the Internet as the system is web-enabled.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>

                </div>
                <!--End Page Content =01=-->

                <!--Start Page Content =02=-->
                <div class="product_page-section">
                    <h3>Features Of Ascon Storage System</h3>
                    <ul class="product_page-list">
                        <li>The system is characterized by the possibility of coding items in a series that helps to arrange the items according to the groups of items of the company.</li>
                        <li>The system is characterized by the registration of all stock and non-stock items.</li>
                        <li>The system is characterized by the possibility of determining the alternative class can be used so that the alternative class in the absence of the basic class.</li>
                        <li>The system is characterized by the possibility to work with the barcode system for each item.</li>
                        <li>The system is fully integrated with sales, procurement and general accounts systems.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>The system is characterized by multiple units of measure class to facilitate the purchase and sale of exchange for the process or manufacturing.</li>
                        <li>The system is characterized by the appearance of the stock balance after each movement affecting the store.</li>
                        <li>The system is characterized by the validity of the follow-up varieties date.</li>
                        <li>The system is characterized by the possibility of an inventory of the stock at any time and in a manner easy.</li>
                        <li>The system is characterized by doing settlement of automated inventory or through the movement of the issued and incomed.</li>
                        <li>The system is characterized by identifying a major supplier for each type of company.</li>
                        <li>The system is characterized by transfers between warehouses through the delivery and receipt of goods and the possibility of knowing the goods on its way.</li>
                        <li>The system is characterized by the ability of determining the minimum and maximum quantities of items and determining the demand limit.</li>
                        <li>The system is compatible with the VAT system.</li>
                        <li>The system also features a large number of reports that serve the warehouse manager and help in decision making.</li>
                        <li>The system features the possibility of extracting all reports in PDF format and the possibility of exporting all reports on the Excel.</li>
                        <li>The system is characterized by the possibility of working it through the internal network or access via the Internet as the web-enabled system.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>

                </div>
                <!--End Page Content =02=-->

                <!--Start Page Content =03=-->
                <div class="product_page-section">
                    <h3>Features Of Ascon Sales And POS</h3>

                    <ul class="product_page-list">
                        <li>The system features a complete sales cycle.</li>
                        <li>The system is characterized by the presence of a line of sales representatives and the extraction of reports to determine the competence of the delegate.</li>
                        <li>The system is characterized by the offer of prices including comprehensive terms and methods of payment to customers.</li>
                        <li>The system is characterized by the possibility of switching from the automated display price-to-sell order in the case of customer adoption.</li>
                        <li>The system is characterized by the work of the delivery of goods, which has ordered the sale and transferred automatically to the sales invoice notes.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>The system is characterized by the possibility of returning sales, either with a receipt or without a receipt.</li>
                        <li>The system is characterized by using more than one currency.</li>
                        <li>The system is characterized by the inability to issue a new bill to a customer exceeding the credit limit.</li>
                        <li>The system is characterized by doing service items and linking them with the bill of items.</li>
                        <li>The system is characterized by the possibility of booking a group of items for the customer.</li>
                        <li>System features a full connection with client systems, warehouses and public accounts.</li>
                        <li>The system is characterized by working with  of customers on a particular group of items.</li>
                        <li>The system is characterized by working through shipments to reduce the quantities of items encoded in the system.</li>
                        <li>The system is compatible with the VAT system.</li>
                        <li>The system is characterized by a large number of reports that serve the needs of managers and decision-makers in the institution.</li>
                        <li>The system is characterized by the possibility of extracting all reports in PDF format and exporting all reports on the excel.</li>
                        <li>The system is characterized by the possibility of working through the internal network or access through the Internet as the system is web-enabled.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =03=-->

                <!--Start Page Content =04=-->
                <div class="product_page-section">
                    <h3>Features Of Ascon System For Car Sales System</h3>

                    <ul class="product_page-list">
                        <li>The System easily accesses the customer and speed of delivery and billing, as well as to achieve control over the delegates and increase their productivity.</li>
                        <li>The ability of control over the car stock.</li>
                        <li>The ability to check when the client on the system that the system will ensure the presence of the delegate to the customer's actual location and then you can start to conduct customer transactions.</li>
                        <li>Possibility of downloading the delegate's visit plan and the itinerary.</li>
                        <li>The ability to make instant sales receipts and print the receipts and delivered to the customer during the visit.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>Possibility of making a sales return.</li>
                        <li>The possibility of an inventory of the goods to the client.</li>
                        <li>The possibility of making an order for the customer and then receipts after the approval of the order from the supervisor or sales manager.</li>
                        <li>The possibility of collecting receipts from the customer and printing the bill of arrest on the site.</li>
                        <li>The possibility of opening an a account as a new customer, however it will be suspended, pending on the review of documents and approval by the administration.</li>
                        <li>Access to sales offers from the mobile device.</li>
                        <li>Access to items and groups of items with attached pictures.</li>
                        <li>Access to the dashboards of a target investigator, sales, receipts and inventory.</li>
                        <li>The system is fully linked with the sales, stores, customers and general accounting system.</li>
                        <li>System can be downloaded on mobile devices or units of the handheld equipped for this type of sales.</li>
                        <li>Access to the current site of the presence of the delegate where the device connects with the cast.</li>
                        <li>The device can operate ONLINE or OFFLINE.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =04=-->


                <!--Start Page Content =05=-->
                <div class="product_page-section">
                    <h3>Features Of Ascon System To Calculate Points</h3>
                    <ul class="product_page-list">
                        <li>Points system is based on direct sales between the sales and customer operations point.</li>
                        <li>The system aims to grant incentives to the customer for each purchase so that the customer stimulates repeat purchases or optimize purchases to increase the acquisition of points.</li>
                        <li>The system assigns a relative weight to each item of the value of the acquired points, which are then converted into amounts in the customer's account (for the different profitability of each item).</li>
                        <li>In the case of the customer acquisition points are recorded as points on hold, but pass by a specified period of user (usually the grace period for the return) to automatically switch to effective points replaceable.</li>
                        <li>A specific score limit is set to replace the points on the client until they are allowed to replace points.</li>
                    </ul>

                    <ul class="product_page-list product_page-list-js">
                        <li>Slices can be made to upgrade the points as a reward to the customer to reach a certain limit is doubled the current balance of the customer.</li>
                        <li>Replacements are selling points where they can replace the total or partial balance available to the client peer carrying out a purchase.</li>
                        <li>Points are registered clients where the phone number is a unique number for each Ao client.</li>
                        <li>Some other data is recorded for each client such as type, age, social status and other data, in addition to a record of the items the customer is interested in purchasing. These data can be used to extract reports to analyze the nature of sales in the age group or type.</li>
                        <li>Connect the system with the general accounts where it will relay the necessary guidance for the process of acquiring the points or the replacement process.</li>
                    </ul>
                    <span class="product_page-button button_hide-js">Hide<i class="fa fa-sort-asc"></i></span>
                    <span class=" product_page-button button_show-js">View More<i class="fa fa-sort-desc "></i></span>
                    <div class="clear"></div>
                </div>
                <!--End Page Content =05=-->


            </div>
        </div>
        <!--End Product Page -->
    @endif
    @include(FE.'.pages.products.product_form')
@stop