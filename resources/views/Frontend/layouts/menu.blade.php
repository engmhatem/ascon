<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url(session()->get('lang')) }}">
                <img src="{{ asset('public/Frontend/images/ascon-logo.png') }}" alt="{{ trans('main.site_name') }}"/>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url(session()->get('lang')) }}" @if(empty(Request::segment(2))) class="active" @endif>{{ trans('main.home') }}</a></li>
                <li><a href="{{ url(session()->get('lang').'?#Products') }}" @if(Request::segment(2)=='products') class="active" @endif> {{ trans('main.products') }}</a></li>
                <li class="dropdown active">
                    <a href="#" class="dropdown-toggle @if(Request::segment(2)=='private-sectors' || Request::segment(2)=='governmental-sectors') active @endif" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        {{ trans('main.sectors') }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url(session()->get('lang').'/private-sectors') }}">{{ trans('main.private') }}</a></li>
{{--                        <li><a href="{{ url(session()->get('lang').'/governmental-sectors') }}">{{ trans('main.governmental') }}</a></li>--}}
                        <li><a href="{{ url(session()->get('lang')) }}">{{ trans('main.governmental') }}</a></li>
                    </ul>
                </li>
                <li><a href="{{ url(session()->get('lang').'/about-us') }}" @if(Request::segment(2)=='about-us') class="active" @endif>{{ trans('main.about_ascon') }} </a></li>
                <li><a href="{{ url(session()->get('lang').'/news') }}" @if(Request::segment(2)=='news') class="active" @endif>{{ trans('main.news') }}</a></li>
                <li><a href="{{ url(session()->get('lang').'/careers') }}" @if(Request::segment(2)=='careers') class="active" @endif>{{ trans('main.careers') }}</a></li>
                <li><a href="{{ url(session()->get('lang').'/contact-us') }}" @if(Request::segment(2)=='contact-us') class="active" @endif>{{ trans('main.contact_us') }}</a></li>

                @if(in_array(session()->get('lang'),config('app.locals')) && session()->get('lang') =='ar')
                    <li><a href="javascript:void(0)" class="changeLanguage" data-lang="en">ENGLISH</a></li>
                @else
                    <li><a href="javascript:void(0)" class="changeLanguage" data-lang="ar">العربية</a></li>
                @endif

            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>