<!DOCTYPE html>
<html @if(session()->get('lang')=='ar') dir="rtl" @endif>
<head>
    <title>{{ trans('main.'.Route::currentRouteName()) }} | {{ trans('main.site_name') }}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/png" href="{{ asset('public/Frontend/images/logo.png') }}"/>
    <link href="{{ asset('public/Frontend/css/slick.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('public/Frontend/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>

    @if(session()->get('lang') =='ar')
        <link href="{{ asset('public/Frontend/css/bootstrap-rtl.min.css') }}" rel="stylesheet" type="text/css"/>
    @endif

    <link href="{{ asset('public/Frontend/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('public/Frontend/css/stylesheet.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('public/Frontend/css/style.css') }}" rel="stylesheet" type="text/css"/>
    @if(session()->get('lang') =='ar')
        <link href="{{ asset('public/Frontend/css/ar-style.css') }}" rel="stylesheet" type="text/css"/>
    @endif

    @yield('style')

    <!--Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

    @if(!empty($analytics))
        @foreach($analytics as $analytic)
            @if($analytic->location ==0 || $analytic->location==1)
                {!! $analytic->script !!}
            @endif
        @endforeach
    @endif
</head>
<body>