<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 footer-det">
                <h4>{{ trans('main.about_us') }}</h4>
                <p><bdi>{{ trans('main.about_txt_1') }}</bdi></p>
                <div class="clear"></div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3 footer-det">
                <h4>{{ trans('main.important_links') }}</h4>
                <ul class="hot_links">
                    <li><a href="{{ url(session()->get('lang').'/careers') }}">{{ trans('main.careers') }}</a></li>
                    <li><a href="{{ url(session()->get('lang').'/sitemap') }}">{{ trans('main.site_map') }}</a></li>
                    <li><a href="{{ url(session()->get('lang').'/privacy-statement') }}">{{ trans('main.privacy_statement') }}</a></li>
                    <li><a href="{{ url(session()->get('lang').'/contact-us') }}">{{ trans('main.contact_us') }}</a></li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-5 footer-det">
                <h4>{{ trans('main.get_in_touch') }}</h4>
                <div>
                    <div class="get-title"><img src="{{ asset('public/Frontend/images/location-white.png') }}"> </div>
                    <div class="get-detali">
                        <p><bdi>{{ trans('main.address_riyadh') }}</bdi></p>
                    </div>
                    <div class="get-title"><img src="{{ asset('public/Frontend/images/call-answer.png') }}"> </div>
                    <div class="get-detali">
                        <ul class="phone_number">
                            <li><a href="tel:+96614620694"><bdi>+ 966-1-4620694</bdi></a></li>
                            <li><a href="tel:+96614612677"><bdi>+ 966-1-4612677</bdi></a></li>
                            <li><a href="tel:+96614664643"><bdi>+ 966-1-4664643</bdi></a></li>
                        </ul>
                    </div>
                    <div class="get-title"><img src="{{ asset('public/Frontend/images/location-white.png') }}"> </div>
                    <div class="get-detali">
                         <p><bdi>{{ trans('main.address_alex') }}</bdi></p>
                    </div>
                    <div class="get-title"><img src="{{ asset('public/Frontend/images/call-answer.png') }}"> </div>
                    <div class="get-detali">
                        <ul class="phone_number">
                            <li><a href="tel:+2035749982"><bdi>+203 574 9982</bdi></a></li>
                            <li><a href="tel:+2035772210"><bdi>+203 577 2210</bdi></a></li>
                        </ul>
                    </div>
                    <br/>
                    <div class="get-title"><img src="{{ asset('public/Frontend/images/envelope.png') }}"> </div>
                    <div class="get-detali"><a href="mailto:ascon@ascon-me.com" target="_blank">ascon@ascon-me.com</a></div>
                </div>
            </div>
        </div>

        <!-- Start Copyright Content-->
        <div class="copyright_content">
            {{--<span><a href="http://hudsystems.com" target="_blank">{{ Carbon\Carbon::now()->year }} <bdi>&copy;</bdi> HUD Systems</a></span>--}}
            <span><a href="{{ url('/') }}" target="_blank">{{ Carbon\Carbon::now()->year }} <bdi>&copy;</bdi> {{ trans('main.site_name') }} </a></span>
        </div>
        <!-- End Copyright Content-->
    </div>
</footer>
<span class="goup_button" id="button_go_up-js"><img src="{{ asset('public/Frontend/images/up-arrow.png') }}"></span>

<!--Start Loading-->
{{--<div class="loading-bg">--}}
    {{--<div class="lds-hourglass"></div>--}}
{{--</div>--}}
<!--End Loading-->

<script src="{{ asset('public/Frontend/js/jquery-2.1.3.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/Frontend/js/bootstrap.min.js') }}" type="text/javascript"></script>

@yield('js')

<script>
    $(document).ready(function () {
        $('.changeLanguage').on('click', function () {
            var lang = $(this).data('lang');
            var full_url  = "{{ url()->current() }}";
            var new_url;
            var segment_one = "{{ Request::segment(1) }}";
            if (!segment_one || segment_one == undefined || segment_one == "" || segment_one.length == 0){
                new_url = "{{ url()->current() }}"+"/"+lang;
            }else {
                new_url = full_url.replace("{{session()->get('lang')}}", lang);
            }
            window.location=new_url;
        });
    });
</script>
<script src="{{ asset('public/Frontend/js/slick.js') }}"></script>
<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
@if(session()->get('lang')=='ar')
    <script src="{{ asset('public/Frontend/js/script-rtl.js') }}"></script>
    @else
    <script src="{{ asset('public/Frontend/js/script.js') }}"></script>
@endif

@if(!empty($analytics))
    @foreach($analytics as $analytic)
        @if($analytic->location ==0 || $analytic->location==2)
            {!! $analytic->script !!}
        @endif
    @endforeach
@endif
</body>
</html>