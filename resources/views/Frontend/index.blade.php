@extends(FEL.'.master')

@section('content')

    {{--Slider--}}
    @if(count($sliders))
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                @foreach($sliders as $slider)
                    <li data-target="#carousel-example-generic" data-slide-to="{{ $loop->index }}" class="{{ ($loop->first) ? "active" : '' }}"></li>
                @endforeach
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                @foreach($sliders as $slider)
                    <div class="item {{ ($loop->first) ? "active" : '' }}">
                        <img src="{{ $slider->image }}" alt="{{ $slider->{session()->get('lang').'_title'} }}"/>
                        <div class="carousel-caption">
                            <h2 class="p-color">{{ $slider->{session()->get('lang').'_title'} }}</h2>
                            <p>{!! $slider->{session()->get('lang').'_description'} !!}</p>
                            @if(!empty($slider->link))
                                <a href="{{ $slider->link }}" class="button" target="_blank">{{ trans('main.read_more') }}</a>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
            <!-- Left and right controls -->
            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    @endif
    {{--End Slider Section--}}


    <section class="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 about-cont">
                    <h2>{{ trans('main.about_ASCON') }}</h2>
                    <p>{{ trans('main.about_txt_1') }}</p>
                    <p>{{ trans('main.about_txt_2') }}</p>
                    <a href="{{ url(session()->get('lang').'/about-us') }}" class="about_button"> {{ trans('main.read_more') }} </a>
                </div>
                <div class="col-lg-5 ">
                    <div class="about-img"></div>
                </div>
            </div>
        </div>
    </section>

    <section id="Products" class="product text-center">
        <div class="container">
            <div class="product_content">
                <h2>{{ trans('main.products') }}</h2>
                <p>{{ trans('main.products_txt') }}</p>
                <div class="row">
                    <!-- Start Block-->
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <a href="{{ url(session()->get('lang').'/products/financial') }}" class="product_block">
                            <aside class="product_icon-1">
                                <img src="{{ asset('public/Frontend/images/money.svg') }}" alt="{{ trans('main.financial') }}"/>
                            </aside>
                            <aside class="product_icon-2">
                                <img src="{{ asset('public/Frontend/images/money-w.svg') }}" alt="{{ trans('main.financial') }}"/>
                            </aside>
                            <h3 class="product_title">{{ trans('main.financial') }}</h3>
                        </a>
                    </div>
                    <!-- End Block-->
                    <!-- Start Block-->
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <a href="{{ url(session()->get('lang').'/products/distribution') }}" class="product_block">
                            <aside class="product_icon-1">
                                <img alt="{{ trans('main.distribution') }}" src="{{ asset('public/Frontend/images/package.svg') }}">
                            </aside>

                            <aside class="product_icon-2">
                                <img alt="{{ trans('main.distribution') }}" src="{{ asset('public/Frontend/images/package-w.svg') }}">
                            </aside>
                            <h3 class="product_title">{{ trans('main.distribution') }}</h3>
                        </a>
                    </div>
                    <!-- End Block-->


                    <!-- Start Block-->
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <a href="{{ url(session()->get('lang').'/products/human-resources') }}" class="product_block">
                            <aside class="product_icon-1">
                                <img alt="{{ trans('main.human_resources') }}" src="{{ asset('public/Frontend/images/target.svg') }}">
                            </aside>
                            <aside class="product_icon-2">
                                <img alt="{{ trans('main.human_resources') }}" src="{{ asset('public/Frontend/images/target-w.svg') }}">
                            </aside>
                            <h3 class="product_title">{{ trans('main.human_resources') }}</h3>
                        </a>
                    </div>
                    <!-- End Block-->

                    <!-- Start Block-->
                    <div class="col-sm-6 col-md-6 col-lg-3">
                        <a href="{{ url(session()->get('lang').'/products/others') }}" class="product_block">
                            <aside class="product_icon-1">
                                <img alt="{{ trans('main.others') }}" src="{{ asset('public/Frontend/images/diamond.svg') }}">
                            </aside>
                            <aside class="product_icon-2">
                                <img alt="{{ trans('main.others') }}" src="{{ asset('public/Frontend/images/diamond-w.svg') }}">
                            </aside>
                            <h3 class="product_title">{{ trans('main.others') }}</h3>
                        </a>
                    </div>
                    <!-- End Block-->


                </div>
            </div>
        </div>
    </section>

    @if(count($partners))
        <section class="our_company">
            <div class="container">
                <div class="row our_company-js our-team-slider">
                    @foreach($partners as $partner)
                        <div class="col-sm">
                            <a href="{{ $partner->link }}" class="our_company-block" target="_blank">
                                <aside class="img">
                                    <img src="{{ $partner->image }}" alt="{{ $partner->{session()->get('lang').'_title'} }}">
                                </aside>
                                <h4>{{ $partner->{session()->get('lang').'_title'} }}</h4>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif
@stop