$(function() {


  'use strict';


  $(window).scroll(function() {
    var scrollTop = 150;
    if ($(window).scrollTop() >= scrollTop) {
      $('.navbar').css({
        position: 'fixed',
        top: '0',
        zIndex: '99',
        backgroundColor: '#fff',

        boxShadow: '0 0 20px rgba(0,0,0,0.5)',
        marginTop: '0'
      });
      $('.navbar-brand>img').css({
        height: '64px',
        marginTop: '-1px'

      });
      $('.navbar-right').css({
        padding: '0',

      });
    }
    if ($(window).scrollTop() < scrollTop) {
      $('.navbar').removeAttr('style');
      $('.navbar-right').css({
        padding: '20px',

      });
    }
    if ($(window).scrollTop() < scrollTop) {
      $('nav a').removeAttr('style');
    }
    if ($(window).scrollTop() < scrollTop) {
      $('.navbar-brand>img').removeAttr('style');
    }
  });

  $("#carousel-example-generic").carousel({
    interval: 7000,
    pause: false
  });
  //    // Active Nav Link
  //    $('.navbar ul li a').click(function () {
  //        $('.navbar ul li a').removeClass('active');
  //        $(this).addClass('active');
  //    });
  //
  //    $(".navbar-default li:first-child a").addClass("active");
  //    $(".navbar-default li a").click(function () {
  //        $(this).addClass("active").siblings().removeClass("active");
  //    });
  //Button Go to Top Hidden and Show
  $(window).scroll(function() {

    var buttonUp = $("#button_go_up-js");
    if ($(window).scrollTop() >= 500) {
      buttonUp.fadeIn(1000);
    } else {
      buttonUp.fadeOut(1000);
    }
  });

  //Button Click To Scroll to top
  $("#button_go_up-js").on('click', function() {
    $('html,body').animate({
      scrollTop: 0
    }, 1000)
  });

  /*======= Backgrounds ======*/
  $("[data-src]").each(function() {
    var backgroundImage = $(this).attr("data-src");
    $(this).css("background-image", "url(" + backgroundImage + ")");
  });

  /**Start Webiner Slider **/
  $('.our_company-js').slick({
    //  dots: true,
    //        rtl: true,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          //        dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });
  /**End Webiner Slider **/



  //Start Product Page Js Style
  //Product Section Show


  //Start Product Page Js Style
  //Product Section Show
  //  Button Show More
  $(".button_show-js").click(function() {
    $(this).siblings(".product_page-list-js").fadeIn();

    $(this).siblings(".button_hide-js").show();
    $(this).hide();

  });

  //  Button Hide More
  $(".button_hide-js").click(function() {
    $(this).siblings(".product_page-list-js").hide();

    $(this).siblings(".button_show-js").show();
    $(this).hide();

  });

  //End Product Page Js Style


  // Start Product  Togglle Icon
  $(".product_block").mouseenter(function() {
    $(this).children(".product_icon-1").hide();
    $(this).children(".product_icon-2").show();
  });
  $(".product_block").mouseleave(function() {
    $(this).children(".product_icon-1").show();
    $(this).children(".product_icon-2").hide();
  });


  // $(window).on('load', function () {
  //     $(".loading-bg").fadeOut(2000, function () {
  //         $("body").css('overflow', 'auto')
  //     });
  // });
  //Start Popup on News Details
  // Click To Show Popup
  $(".show_popup-js").click(function() {
    $('.popup_services').fadeIn();
  });


  // Close popup
  $(".popup_services , .popup_services-close").click(function() {
    $(".popup_services").fadeOut(1000);
  });
  //Close Popup By using Esk Keypord Button
  $(document).keydown(function(e) {
    if (e.keyCode == 27) {
      $(".popup_services").fadeOut(1000);
    }
  });
  //Stop Propagation
  $(".popup_services-content").click(function(e) {
    e.stopPropagation();
  });
  //End Popup on News Details



    // Start Float Form 
    if (screen.width > 960) {
      $(".form_content").attr("id", "float_form");

    } else {
      $(".form_content").attr("id", "soliman");

    }


    $(window).scroll(function () {

      var floatForm = $("#float_form");
      if ($(window).scrollTop() >= 500) {
        floatForm.fadeIn(1000);
      } else {
        floatForm.fadeOut(1000);
      }
    });
    // End Float Form

});