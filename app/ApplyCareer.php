<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplyCareer extends Model
{
    use SoftDeletes;

    protected $table = 'apply_careers';

    protected $primaryKey = 'id';

    protected $guarded = [];

    protected $dates =[
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function career()
    {
        return $this->belongsTo(Career::class,'career_id','id');
    }
}
