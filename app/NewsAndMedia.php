<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsAndMedia extends Model
{
    protected $table = 'news_and_media';

    protected $primaryKey = 'id';

    protected $guarded =[];

    protected $dates=[
        'start_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
