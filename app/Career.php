<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Career extends Model
{
    use SoftDeletes;

    protected $table = 'careers';

    protected $primaryKey = 'id';

    protected $guarded =[];

    protected $dates = [
        'publish_date',
        'close_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
