<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Settings extends Model
{
    use SoftDeletes;

    protected $table = 'settings';

    protected $guarded =[];

    protected $primaryKey = 'id';

    protected $dates =[
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * @return mixed
     */
    public function scopeActiveSettings()
    {
        return $this->where('status',1);
    }
}
