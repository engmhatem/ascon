<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PrivateSectorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sector_id'     => 'required|integer|min:1|max:13',
            'name'          => 'required|min:3|max:100',
            'email'         => 'required|email',
            'phone'         => 'required|min:3|max:20',
            'message'       => 'required|min:3|max:300',
        ];
    }
}
