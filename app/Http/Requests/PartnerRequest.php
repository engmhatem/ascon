<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PartnerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ar_title'           => 'required|max:150',
            'en_title'           => 'required|max:150',
            'image'              => 'sometimes|nullable|required|image|mimes:png,jpeg,jpg,PNG|dimensions:width=160,height=80',
        ];
    }
}
