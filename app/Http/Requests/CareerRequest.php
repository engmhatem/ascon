<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CareerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ar_title'          => 'required|min:3',
            'en_title'          => 'required|min:3',
            'image'             => 'sometimes|nullable|required|image|mimes:png,PNG,jpg,jpeg|dimensions:min_width=1200,max_width=1500,min_height=300,max_height=500',
            'ar_description'    => 'required',
            'en_description'    => 'required',
            'publish_date'      => 'required|date',
            'close_date'        => 'required|date|after:publish_date',
            'ar_special_requirements'        => 'required',
            'en_special_requirements'        => 'required',
            'position_code'     => 'required',
            'ar_location'       => 'required',
            'en_location'       => 'required',
        ];
    }
}
