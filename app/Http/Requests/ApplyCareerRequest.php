<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApplyCareerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'career_id'             => 'required|integer|min:1',
            'name'                  => 'required|min:3|max:100',
            'email'                 => 'required|email',
            'current_title'         => 'required|min:3|max:100',
            'current_salary'        => 'required|integer|min:0',
            'expected_join_date'    => 'required',
            'current_location_city' => 'required|min:3|max:100',
            'CV_file'               => 'required|file|mimes:pdf,docx,doc|max:6000',
        ];
    }
}
