<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ar_title'          => 'required|max:150',
            'en_title'          => 'required|max:150',
            'ar_description'    => 'required|max:150',
            'en_description'    => 'required|max:150',
            'link'              => 'sometimes|nullable|required|url',
            'image'             => 'sometimes|nullable|required|image|mimes:png,PNG,jpg,jpeg|dimensions:min_width=1600,max_width=1800,min_height=800,max_height=1000',
            'status'            => 'required|integer',
        ];
    }
}
