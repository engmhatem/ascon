<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                      => 'required',
            'email'                     => 'required|email|unique:users',
            'password'                  => 'required|min:8|confirmed',
            'password_confirmation'     => 'required|min:8',
            'status'                    => 'required|integer',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name'                      => trans('admin.name'),
            'email'                     => trans('admin.email'),
            'password'                  => trans('admin.password'),
            'password_confirmation'     => trans('admin.password_confirmation'),
            'status'                    => trans('admin.status')
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($validator->fails())
        {
            session()->flash('error_msg', trans('admin.error_message'));
        }
    }
}
