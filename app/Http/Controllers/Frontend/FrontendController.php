<?php
namespace App\Http\Controllers\Frontend;

use App\ApplyCareer;
use App\ApplyProduct;
use App\Career;
use App\ContactUs;
use App\Http\Controllers\Administrator\AdminHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\ApplyCareerRequest;
use App\Http\Requests\ApplyProductRequest;
use App\Http\Requests\ContactFormRequest;
use App\Http\Requests\PrivateSectorRequest;
use App\NewsAndMedia;
use App\Partner;
use App\PrivateSector;
use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class FrontendController extends Controller
{
    /**
     * FrontendController constructor.
     */
    public function __construct()
    {
        App::setLocale(session()->get('lang'));
        $this->middleware('Language',['excepts'=>['SaveContactCustomers','ApplyPrivateSector','ApplyCareer']]);
    }

    /**
     * @return $this
     */
    public function index()
    {
        $data = [
            'sliders'=>Slider::where('status',1)->get(),
            'partners'=>Partner::ActivePartners()->get(),
        ];
        return view(FE.'.index')->with($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ShowNewsAndMedia()
    {
        $data = [
            'news'=>NewsAndMedia::latest()->paginate(12)
        ];
        return view(FE.'.pages.news.news_and_media')->with($data);
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function ShowNewsAndMediaDetails(Request $request)
    {
        if ($request->id && $request->id>0){
            $data = [
                'news_and_media'=>NewsAndMedia::findOrFail($request->id),
                'news'=>NewsAndMedia::where('id','!=',$request->id)->latest()->take(8)->get(),
            ];
            return view(FE.'.pages.news.news_details')->with($data);
        }
        return redirect(session()->get('lang').'/list-news');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ShowCareers()
    {
        $data = [
            'careers'=>Career::latest('publish_date')->get(),
        ];
        return view(FE.'.pages.career.index')->with($data);
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function ShowCareerDetails(Request $request)
    {
        if ($request->id && $request->id>0){
            $data = [
                'career'=>Career::findOrFail($request->id),
            ];
            return view(FE.'.pages.career.careers_details')->with($data);
        }
        return redirect(session()->get('lang').'/careers');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ShowCareerApplyPage(Request $request)
    {
        if ($request->id && $request->id>0){
            $career = Career::findOrFail($request->id);
            if ($career->close_date >= \Carbon\Carbon::now())
            {
                $data = [
                    'career'=>$career,
                ];
                return view(FE.'.pages.career.careers_apply')->with($data);
            }
        }
        return redirect(session()->get('lang').'/careers');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function AboutUs()
    {
        return view(FE.'.about_us');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ShowContactUs()
    {
        return view(FE.'.contact_us');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ShowSectors()
    {
        return view(FE.'.pages.sectors.private_sectors');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ShowFinancialProduct()
    {
        return view(FE.'.pages.products.financial')->with(['id'=>1]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ShowDistributionProduct()
    {
        return view(FE.'.pages.products.distribution')->with(['id'=>2]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ShowHumanResourcesProduct()
    {
        return view(FE.'.pages.products.human_resources')->with(['id'=>3]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ShowOthersProduct()
    {
        return view(FE.'.pages.products.others')->with(['id'=>4]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function ShowSectorsDetails(Request $request)
    {
        if ((int)$request->id && (int)$request->id>0 && (int)$request->id<14){
            return view(FE.'.pages.sectors.private_sectors_'.(int)$request->id)->with(['id'=>(int)$request->id]);
        }
        return redirect(session()->get('lang').'/private-sectors');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ShowSitemap()
    {
        return view(FE.'.pages.sitemap');
    }

    /**
     * @param ContactFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function SaveContactCustomers(ContactFormRequest $request)
    {
        $lang = 'ar';
        if ($request->has('lang')){
            $lang = $request->input('lang');
        }
        App::setLocale($lang);
        ContactUs::create($request->except('lang'));
        session()->flash('contact_success',trans('main.contact_success'));
        return back();
    }

    /**
     * @param ApplyCareerRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function ApplyCareer(ApplyCareerRequest $request)
    {
        $lang = 'ar';
        if ($request->has('lang')){
            $lang = $request->input('lang');
        }
        App::setLocale($lang);

        if ($request->hasFile('CV_file') && $request->file('CV_file')->isValid())
        {
            $file_path = (new AdminHelper())->UploadImage($request,'files/career','CV_file');
        }

        $add = (new ApplyCareer())->create($request->except(['CV_file','lang']));
        if(!empty($file_path)){
            $add->CV_file = url('public').'/storage/'.$file_path;
        }
        $add->save();

        try{
            //        info@ascon-me.com

            $title      = $add->name.' apply for '.$add->career->en_title;
            $msg        = $add;
            $from       = $add->email;
            $from_name  = $add->name;
            $to         = env('MAIL_FROM_ADDRESS');
            $to_name    = env('MAIL_FROM_NAME');
            (new AdminHelper())->SendMail('emails.apply_career', $title, $msg, $from, $from_name, $to, $to_name,$add->CV_file);
        }catch (\Exception $e){
            return redirect(session()->get('lang').'/careers');
            return $e->getMessage();
        }
        session()->flash('contact_success',trans('main.contact_success'));
        return back();
    }

    /**
     * @param PrivateSectorRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function ApplyPrivateSector(PrivateSectorRequest $request)
    {
        $lang = 'ar';
        if ($request->has('lang')){
            $lang = $request->input('lang');
        }
        App::setLocale($lang);
        PrivateSector::create($request->except('lang'));
        session()->flash('contact_success',trans('main.contact_success'));
        return back();
    }

    /**
     * @param ApplyProductRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function ApplyProduct(ApplyProductRequest $request)
    {
        $lang = 'ar';
        if ($request->has('lang')){
            $lang = $request->input('lang');
        }
        App::setLocale($lang);
        ApplyProduct::create($request->except('lang'));
        session()->flash('contact_success',trans('main.contact_success'));
        return back();
    }
}
