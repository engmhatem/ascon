<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Http\Requests\SettingsRequest;
use App\Settings;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * SettingsController constructor.
     */
    public function __construct()
    {
        $this->middleware('AdminAuth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'all'=>Settings::all()
        ];
        return view(AD.'.settings.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'submit_button'=>trans('admin.save'),
        ];
        return view(AD.'.settings.form')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SettingsRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(SettingsRequest $request)
    {
        (new Settings())->create($request->all());
        session()->flash('success_msg',trans('admin.success_message'));
        return redirect(AD.'/settings');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'form_data'=>Settings::findOrFail($id),
            'submit_button'=>trans('admin.save'),
        ];
        return view(AD.'.settings.form')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SettingsRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(SettingsRequest $request, $id)
    {
        Settings::findOrFail($id)->update($request->except(['_method','_token']));

        session()->flash('success_msg',trans('admin.success_message'));
        return redirect(AD.'/settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Settings::FindOrFail($id)->delete();
        session()->flash('success_msg', trans('admin.success_message'));
        return back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete_selected_settings(Request $request)
    {
        $ids = explode(",",$request->input('select_ids'));
        foreach ($ids as $id){
            Settings::FindOrFail($id)->delete();
        }
        session()->flash('success_msg', trans('admin.success_message'));
        return back();
    }
}
