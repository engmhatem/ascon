<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Administrator\AdminHelper as AdminHelper;
use App\Http\Requests\PartnerRequest;
use App\Partner;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    /**
     * PartnerController constructor.
     */
    public function __construct()
    {
        $this->middleware('AdminAuth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'all'=>Partner::all(),
        ];
        return view(AD.'.partner.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'submit_button'=> trans('admin.save'),
            'required'=>'required'
        ];
        return view(AD.'.partner.form')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PartnerRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PartnerRequest $request)
    {
        if ($request->hasFile('image') && $request->file('image')->isValid())
        {
            $image_path = (new AdminHelper())->UploadImage($request,'images/partner','image');
        }

        $add = (new Partner())->create($request->except(['image']));
        if(!empty($image_path)){
            $add->image = url('public').'/storage/'.$image_path;
        }
        $add->save();

        session()->flash('success_msg',trans('admin.success_message'));
        return redirect(AD.'/partner');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'form_data'=> Partner::FindOrFail($id),
            'submit_button'=> trans('admin.save'),
            'required'=>''
        ];
        return view(AD.'.partner.form')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PartnerRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(PartnerRequest $request, $id)
    {
        $add = Partner::findOrFail($id);

        if ($request->hasFile('image') && $request->file('image')->isValid())
        {
            $image_path = (new AdminHelper())->UploadImage($request,'images/partner','image',$add->image);
        }

        if(!empty($image_path)){
            $add->image = url('public').'/storage/'.$image_path;
        }
        $add->where('id',$id)->update($request->except(['_method','_token','image']));

        if(!empty($image_path)){
            $add->image = url('public').'/storage/'.$image_path;
        }
        $add->save();
        session()->flash('success_msg',trans('admin.success_message'));
        return redirect(AD.'/partner');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Partner::FindOrFail($id);
//        if ($delete->image)
//        {
//            (new AdminHelper())->DeleteImage($delete->image);
//        }
        $delete->delete();
        session()->flash('success_msg', trans('admin.success_message'));
        return back();
    }

    /**
     * Delete Selected ids
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete_selected_partner(Request $request)
    {
        $ids = explode(",",$request->input('select_ids'));
        foreach ($ids as $id){
            $delete = Partner::FindOrFail($id);
//            if ($delete->image)
//            {
//                (new AdminHelper())->DeleteImage($delete->image);
//            }
            $delete->delete();
        }
        session()->flash('success_msg', trans('admin.success_message'));
        return back();
    }
}
