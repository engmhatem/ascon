<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewsAndMediaRequest;
use App\NewsAndMedia;
use Illuminate\Http\Request;

class NewsAndMediaController extends Controller
{
    /**
     * NewsAndMediaController constructor.
     */
    public function __construct()
    {
        $this->middleware('AdminAuth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'all'=>NewsAndMedia::all(),
        ];
        return view(AD.'.news_and_media.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'submit_button'=> trans('admin.save'),
            'required'=>'required'
        ];
        return view(AD.'.news_and_media.form')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NewsAndMediaRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(NewsAndMediaRequest $request)
    {
        if ($request->hasFile('image') && $request->file('image')->isValid())
        {
            $image_path = (new AdminHelper())->UploadImage($request,'images/news_and_media','image');
        }

        $add = (new NewsAndMedia())->create($request->except(['image']));
        if(!empty($image_path)){
            $add->image = url('public').'/storage/'.$image_path;
        }

        $add->save();
        session()->flash('success_msg',trans('admin.success_message'));
        return redirect(AD.'/news-and-media');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'form_data'=>NewsAndMedia::findOrFail($id),
            'submit_button'=> trans('admin.save'),
            'required'=>''
        ];
        return view(AD.'.news_and_media.form')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param NewsAndMediaRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(NewsAndMediaRequest $request, $id)
    {
        $add = NewsAndMedia::findOrFail($id);

        if ($request->hasFile('image') && $request->file('image')->isValid())
        {
            $image_path = (new AdminHelper())->UploadImage($request,'images/news_and_media','image',$add->image);
        }

        $add->where('id',$id)->update($request->except(['_method','_token','image']));

        if(!empty($image_path)){
            $add->image = url('public').'/storage/'.$image_path;
        }

        $add->save();
        session()->flash('success_msg',trans('admin.success_message'));
        return redirect(AD.'/news-and-media');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = NewsAndMedia::FindOrFail($id);
//        if ($delete->image)
//        {
//            (new AdminHelper())->DeleteImage($delete->image);
//        }
        $delete->delete();
        session()->flash('success_msg', trans('admin.success_message'));
        return back();
    }

    /**
     * Delete Selected ids
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete_selected_news_and_media(Request $request)
    {
        $ids = explode(",",$request->input('select_ids'));
        foreach ($ids as $id){
            $delete = NewsAndMedia::FindOrFail($id);
//            if ($delete->image)
//            {
//                (new AdminHelper())->DeleteImage($delete->image);
//            }
            $delete->delete();
        }
        session()->flash('success_msg', trans('admin.success_message'));
        return back();
    }
}
