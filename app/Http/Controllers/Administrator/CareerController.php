<?php

namespace App\Http\Controllers\Administrator;

use App\ApplyCareer;
use App\Career;
use App\Http\Controllers\Controller;
use App\Http\Requests\CareerRequest;
use Illuminate\Http\Request;

class CareerController extends Controller
{
    /**
     * CareerController constructor.
     */
    public function __construct()
    {
        $this->middleware('AdminAuth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'all'=>Career::all(),
        ];
        return view(AD.'.career.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'submit_button'=> trans('admin.save'),
            'required'=>'required'
        ];
        return view(AD.'.career.form')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CareerRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CareerRequest $request)
    {
        if ($request->hasFile('image') && $request->file('image')->isValid())
        {
            $image_path = (new AdminHelper())->UploadImage($request,'images/career','image');
        }

        $add = (new Career())->create($request->except(['image']));
        if(!empty($image_path)){
            $add->image = url('public').'/storage/'.$image_path;
        }

        $add->save();
        session()->flash('success_msg',trans('admin.success_message'));
        return redirect(AD.'/career');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'form_data'=>Career::findOrFail($id),
            'submit_button'=> trans('admin.save'),
            'required'=>''
        ];
        return view(AD.'.career.form')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CareerRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(CareerRequest $request, $id)
    {
        $add = Career::findOrFail($id);

        if ($request->hasFile('image') && $request->file('image')->isValid())
        {
            $image_path = (new AdminHelper())->UploadImage($request,'images/career','image',$add->image);
        }

        $add->where('id',$id)->update($request->except(['_method','_token','image']));

        if(!empty($image_path)){
            $add->image = url('public').'/storage/'.$image_path;
        }

        $add->save();
        session()->flash('success_msg',trans('admin.success_message'));
        return redirect(AD.'/career');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Career::FindOrFail($id);
//        if ($delete->image)
//        {
//            (new AdminHelper())->DeleteImage($delete->image);
//        }
        $delete->delete();
        session()->flash('success_msg', trans('admin.success_message'));
        return back();
    }

    /**
     * Delete Selected ids
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete_selected_career(Request $request)
    {
        $ids = explode(",",$request->input('select_ids'));
        foreach ($ids as $id){
            $delete = Career::FindOrFail($id);
//            if ($delete->image)
//            {
//                (new AdminHelper())->DeleteImage($delete->image);
//            }
            $delete->delete();
        }
        session()->flash('success_msg', trans('admin.success_message'));
        return back();
    }

    public function ShowCareersCV()
    {
        $data = [
            'all'=>ApplyCareer::latest()->get(),
        ];
        return view(AD.'.career.show_cv')->with($data);
    }
}
