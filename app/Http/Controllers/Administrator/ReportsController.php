<?php

namespace App\Http\Controllers\Administrator;

use App\ApplyProduct;
use App\ContactUs;
use App\Http\Controllers\Controller;
use App\PrivateSector;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    /**
     * ReportsController constructor.
     */
    public function __construct()
    {
        $this->middleware('AdminAuth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegisteredCustomersSectors()
    {
        $data = [
            'all'=>PrivateSector::all(),
        ];
        return view(AD.'.reports.apply_sectors')->with($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showContactsCustomers()
    {
        $data = [
            'all'=>ContactUs::all(),
        ];
        return view(AD.'.reports.contacts')->with($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegisteredCustomersProducts()
    {
        $data = [
            'all'=>ApplyProduct::all(),
        ];
        return view(AD.'.reports.apply_products')->with($data);
    }
}
