<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Requests\SliderRequest;
use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    /**
     * AdminsController constructor.
     */
    public function __construct()
    {
        $this->middleware('AdminAuth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'all'=>Slider::all()
        ];
        return view(AD.'.sliders.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'submit_button'=>trans('admin.save'),
            'required'=>'required'
        ];
        return view(AD.'.sliders.form')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SliderRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(SliderRequest $request)
    {
        if ($request->hasFile('image') && $request->file('image')->isValid())
        {
            $image_path = (new AdminHelper())->UploadImage($request,'images/sliders','image');
        }

        $add = (new Slider())->create($request->except(['image']));
        if(!empty($image_path)){
            $add->image = url('public').'/storage/'.$image_path;
        }

        $add->save();
        session()->flash('success_msg',trans('admin.success_message'));
        return redirect(AD.'/sliders');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'form_data'=>Slider::findOrFail($id),
            'submit_button'=>trans('admin.save'),
            'required'=>''
        ];
        return view(AD.'.sliders.form')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SliderRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(SliderRequest $request, $id)
    {
        $add = Slider::findOrFail($id);

        if ($request->hasFile('image') && $request->file('image')->isValid())
        {
            $image_path = (new AdminHelper())->UploadImage($request,'images/sliders','image',$add->image);
        }

        $add->where('id',$id)->update($request->except(['_method','_token','image']));

        if(!empty($image_path)){
            $add->image = url('public').'/storage/'.$image_path;
        }

        $add->save();
        session()->flash('success_msg',trans('admin.success_message'));
        return redirect(AD.'/sliders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Slider::FindOrFail($id);
//        if ($delete->image)
//        {
//            (new AdminHelper())->DeleteImage($delete->image);
//        }
        $delete->delete();
        session()->flash('success_msg', trans('admin.success_message'));
        return back();
    }

    /**
     * Delete Selected ids
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete_selected_sliders(Request $request)
    {
        $ids = explode(",",$request->input('select_ids'));
        foreach ($ids as $id){
            $delete = Slider::FindOrFail($id);
//            if ($delete->image)
//            {
//                (new AdminHelper())->DeleteImage($delete->image);
//            }
            $delete->delete();
        }
        session()->flash('success_msg', trans('admin.success_message'));
        return back();
    }
}
