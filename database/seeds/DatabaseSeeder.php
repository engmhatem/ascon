<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // create Admin
        DB::table('users')->insert([
            'name' 			    => "Ayman",
            'email' 			=> 'admin@admin.com',
            'status'			=> 1,
            'password'			=> bcrypt('secret'),
            'created_at'        => \Carbon\Carbon::now()->toDateTimeString(),
        ]);
    }
}
