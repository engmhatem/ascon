<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplyCareersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apply_careers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('career_id')->nullable();
            $table->text('name',100)->nullable();
            $table->text('email')->nullable();
            $table->text('current_title',100)->nullable();
            $table->integer('current_salary')->nullable();
            $table->text('expected_join_date',50)->nullable();
            $table->text('current_location_city',100)->nullable();
            $table->text('CV_file')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apply_careers');
    }
}
