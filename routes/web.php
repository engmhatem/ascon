<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

define('ADL','Administrator/layouts');
define('AD','Administrator');
define('FEL','Frontend/layouts');
define('FE','Frontend');


Route::group(['middleware'=>'guest'], function (){

    Route::get('admins/login','LoginController@login')->name('Login');
    Route::post('admins/login','LoginController@checkLogin');

});

Route::group(['namespace'=>FE],function (){

    Route::group(['middleware'=>'Language','prefix'=>Request::segment(1)],function (){
        Route::group(['middleware'=>'Language','prefix'=>session()->get('lang')],function (){
            Route::get('/','FrontendController@index')->name('home');
            Route::get('news','FrontendController@ShowNewsAndMedia')->name('news');
            Route::get('news/{id}/{title?}','FrontendController@ShowNewsAndMediaDetails')->name('news');
            Route::get('careers','FrontendController@ShowCareers')->name('careers');
            Route::get('careers/{id}/{title?}','FrontendController@ShowCareerDetails')->name('careers');
            Route::get('careers/apply/{id}/{title?}','FrontendController@ShowCareerApplyPage')->name('apply_career');
            Route::get('about-us','FrontendController@AboutUs')->name('about_us');
            Route::get('contact-us','FrontendController@ShowContactUs')->name('contact_us');
            Route::get('private-sectors','FrontendController@ShowSectors')->name('sectors');
            Route::get('private-sectors/{id}','FrontendController@ShowSectorsDetails')->name('sectors');
            Route::get('products/financial','FrontendController@ShowFinancialProduct')->name('products');
            Route::get('products/distribution','FrontendController@ShowDistributionProduct')->name('products');
            Route::get('products/human-resources','FrontendController@ShowHumanResourcesProduct')->name('products');
            Route::get('products/others','FrontendController@ShowOthersProduct')->name('products');
            Route::get('sitemap','FrontendController@ShowSitemap')->name('site_map');
        });
    });

//    Post Routes
    Route::post('contact-form','FrontendController@SaveContactCustomers');
    Route::post('apply-career','FrontendController@ApplyCareer');
    Route::post('apply-for-sector','FrontendController@ApplyPrivateSector');
    Route::post('apply-for-product','FrontendController@ApplyProduct');
});

Route::group(['middleware'=>'AdminAuth','namespace'=>AD,'prefix'=>AD],function (){

    Route::get('logout','AdminsController@logout')->name('Logout');

    Route::get('home','AdminsController@index')->name('Home');

    Route::resource('admins','AdminsController',['names'=>['index'=>'Show Admins','create'=>'Create New Admin','edit'=>'Edit Admin','show'=>'Show Admin']]);
    Route::patch('change-password/{id}','AdminsController@user_update_password');

    Route::resource('news-and-media','NewsAndMediaController',['names'=>['index'=>'Show News And Media','create'=>'Create New News And Media','edit'=>'Edit News And Media','show'=>'Show News And Media']]);
    Route::post('delete_select_news-and-media','NewsAndMediaController@delete_selected_news_and_media');

    Route::resource('sliders','SliderController',['names'=>['index'=>'Slider','create'=>'Create Slider','edit'=>'Edit Slider',]]);
    Route::post('delete_select_sliders','SliderController@delete_selected_sliders');

    Route::resource('partner','PartnerController',['names'=>['index'=>'Show Partner','create'=>'Create New Partner','edit'=>'Edit Partner','show'=>'Show Partner']]);
    Route::post('delete_select_partner','PartnerController@delete_selected_partner');

    Route::resource('career','CareerController',['names'=>['index'=>'Show Career','create'=>'Create New Career','edit'=>'Edit Career','show'=>'Show Career']]);
    Route::post('delete_select_career','CareerController@delete_selected_career');
    Route::get('show-careers-cv','CareerController@ShowCareersCV');
    Route::get('customers/registered-sectors','ReportsController@showRegisteredCustomersSectors');
    Route::get('customers/contact-us','ReportsController@showContactsCustomers');
    Route::get('customers/registered-products','ReportsController@showRegisteredCustomersProducts');

    Route::resource('settings','SettingsController',['names'=>['index'=>'Show Settings','create'=>'Create New Settings','edit'=>'Edit Settings','show'=>'Show Settings']]);
    Route::post('delete_select_settings','SettingsController@delete_selected_settings');

});
